# Anagram Margana

### Pipeline Staging
[![pipeline status](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/-/commits/staging) [![coverage report](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/-/commits/staging)

### Pipeline Master

[![pipeline status](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/badges/master/pipeline.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/-/commits/master) [![coverage report](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/badges/master/coverage.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/a7-anagram-margana/-/commits/master)



### Website Deployment

[https://anagram-margana.herokuapp.com/](http://anagram-margana.herokuapp.com/)



### Dokumentasi

[DOCUMENTATION.md](docs/DOCUMENTATION.md)




## Sprint 1 


### Added
- Implementasi main-page untuk create room as host (dan tombol navigasi untuk join as guest)
- Implementasi `/join` untuk join as guest
- Implementasi `/lobby/`
- Implementasi `/game-settings/`
- Implementasi `/in-game`
- Implementasi `/in-game/host`

### Changed
### Fixed




## Sprint 2

#### Diagram UML (gambaran besar, masih tentatif)

https://drive.google.com/file/d/1sS5cS_XjcBxZEJlakF9_ZvE7djw4PXlO/view?usp=sharing 

### Added
- API untuk komunikasi dengan main-server melalui perantara microservice
- Fungsionalitas pada front-end untuk update scoreboard in-game
- Fungsionalitas pada front-end untuk update time remaining in-game
- Fungsionalitas pada front-end untuk input huruf in-game
- Scoreboard besar di in-game 
### Changed
- Client wajib mengaktifkan javascript
### Fixed



## Sprint 3

### Added
- Fungsionalitas keseluruhan untuk update scoreboard in-game
- Fungsionalitas keseluruhan untuk update time remaining in-game
- Fungsionalitas keseluruhan untuk input huruf in-game
- Fungsionalitas keseluruhan untuk vote restart
- Fungsionalitas keseluruhan untuk restart round
- Fungsionalitas keseluruhan untuk finish game
### Changed
### Fixed

