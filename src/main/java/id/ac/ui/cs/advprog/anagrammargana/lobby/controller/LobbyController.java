package id.ac.ui.cs.advprog.anagrammargana.lobby.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/lobby")
public class LobbyController {
    @Autowired
    IRoomHostValidatorService lobbyService;
    
    @GetMapping(value = "/")
    public String lobbyPage(@CookieValue(value="playerName") String playerName,
                            @CookieValue(value="playerId") String playerId,
                            @CookieValue(value="roomCode") String roomCode,
                            Model model) {
        model.addAttribute("playerName", playerName);
        model.addAttribute("roomCode", roomCode);
        model.addAttribute("isHost", lobbyService.isHost(roomCode, Long.parseLong(playerId)));
        return "lobby/game_lobby";
    }
}
