package id.ac.ui.cs.advprog.anagrammargana.common.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator.IRandomLetterGeneratorSingleton;
import id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator.RandomLetterGeneratorSingleton;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.WordRepository;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Path;


@Service
public class WordRepositoryInitializerService {
    @Autowired
    WordRepository wordRepository;
    
    @Setter
    IRandomLetterGeneratorSingleton randomLetterGeneratorSingleton = RandomLetterGeneratorSingleton.getInstance();
    
    @Setter
    Path dictionaryFilePath = Path.of("src/main/resources/static/dictionary.txt");
    
    
    @PostConstruct
    @SneakyThrows
    void wordRepositoryInitializer(){
        var fileContent = Files.readAllLines(dictionaryFilePath);
        for (var word: fileContent) {
            if (word.length() < 4 || word.length() > 9)
                continue;
            wordRepository.addWord(word.toLowerCase());
        }
    }
    
    @PostConstruct
    void initializeWordRepositoryForRandomLetterGenerator(){
        randomLetterGeneratorSingleton.setWordRepository(wordRepository);
    }
}
