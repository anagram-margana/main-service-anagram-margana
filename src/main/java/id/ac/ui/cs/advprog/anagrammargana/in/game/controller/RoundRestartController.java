package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.RoundRestartService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/communication-api/msg-from-client")
public class RoundRestartController {
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @Autowired
    IRoomHostValidatorService roomHostValidatorService;
    
    @Autowired
    RoundRestartService roundRestartService;
    
    
    
    @PostMapping(value = "/vote-restart", produces = "application/json")
    public MessageData<Object> getInput(@RequestBody MessageData<Object> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        
        var success = roundRestartService.handleSomeoneVotedForRestart(
                messageDTO.getRoomCode(), messageDTO.getPlayerId());
        
        if (success) {
            var reply = new MessageData<>();
            reply.copyDataFrom(messageDTO);
            reply.setType("vote-accepted");
    
            return reply;
        }else return null;
    }
    
    
    
    @PostMapping(value = "/restart-round", produces = "application/json")
    public void roundRestart(@RequestBody MessageData<Object> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        roomHostValidatorService.throwIfNotRoomHost(messageDTO);
    
        roundRestartService.restartRound(messageDTO.getRoomCode());
    }
}
