package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;

public interface IWebsocketPlayerAuthenticationService {
    
    /**
     * Mengecek apakah suatu roomCode valid, dan apakah playerId beserta sessionToken-nya valid. <br>
     * Setelah itu, cek juga apakah roomCode merupakan room dari playerId tersebut.
     *
     * @return boolean true apabila autentikasi berhasil, dan false jika sebaliknya
     */
    boolean authenticate(String roomCode, long playerId, String sessionToken);
    
    
    /**
     * @see IWebsocketPlayerAuthenticationService#authenticate(String, long, String)
     */
    <T> boolean authenticate(MessageData<T> messageDTO);
    
    
    
    /**
     * Mengecek apakah suatu roomCode valid, dan apakah playerId beserta sessionToken-nya valid. <br>
     * Setelah itu, cek juga apakah roomCode merupakan room dari playerId tersebut.
     *
     * Apabila roomCode tidak valid, throw {@link InvalidRoomCodeException} <br>
     * Apabila playerId tidak valid, throw {@link InvalidPlayerIdException} <br>
     * Apabila sessionToken tidak match, throw {@link InvalidSessionTokenException} <br>
     *
     * @return boolean true apabila autentikasi berhasil, dan false jika sebaliknya
     */
    void throwIfNotAuthenticated(String roomCode, long playerId, String sessionToken)
        throws InvalidRoomCodeException, InvalidPlayerIdException, InvalidSessionTokenException;
    
    
    /**
     * @see IWebsocketPlayerAuthenticationService#throwIfNotAuthenticated(String, long, String)
     */
    <T> void throwIfNotAuthenticated(MessageData<T> messageDTO)
            throws InvalidRoomCodeException, InvalidPlayerIdException, InvalidSessionTokenException;
}
