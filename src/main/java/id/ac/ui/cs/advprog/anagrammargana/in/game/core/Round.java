package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@EqualsAndHashCode
public class Round implements IRound{
    IRoom room;
    Date endTime;
    int minimumWord;
    Map<IPlayer,IWordPool> wordPoolCollection;
    ISortedChars randomChars;

    public Round(IRoom room, Date endTime, int minimumWord, ISortedChars randomChars){
        this.room = room;
        this.endTime = endTime;
        this.minimumWord = minimumWord;
        this.randomChars = randomChars;
        this.wordPoolCollection = new LinkedHashMap<>();
    }
    
    @Override
    public void stopRound() {
        var now = new Date();
        endTime = now;
    }
    
    @Override
    public Date getRoundEndTime() {
        return this.endTime;
    }

    @Override
    public ISortedChars getRandomLetters() {
        return this.randomChars;
    }

    @Override
    public int getMinimumWord() {
        return this.minimumWord;
    }

    @Override
    public IWordPool getWordPool(IPlayer player) {
        return this.wordPoolCollection.get(player);
    }
    
    @Override
    public Map<IPlayer, IWordPool> getWordPools() {
        return wordPoolCollection;
    }
    
    @Override
    public void setWordPool(IPlayer player, IWordPool wordPool) {
        var players = this.room.getMembers();
        if (players.contains(player)){
            this.wordPoolCollection.put(player, wordPool);
        }
    }


}
