package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;

import java.util.ArrayList;
import java.util.Collection;

public class RestartVote implements IRestartVote {
    int minimumRequiredNumberOfVote;
    Collection<IPlayer> affirmativePlayers = new ArrayList<>();

    public RestartVote(int minimumVote) {
        minimumRequiredNumberOfVote = minimumVote;
    }

    @Override
    public Collection<IPlayer> getAffirmativePlayer() {
        return affirmativePlayers;
    }
    
    @Override
    public int getNumberOfAffirmativePlayer() {
        return affirmativePlayers.size();
    }
    
    
    @Override
    public void addAffirmativePlayer(IPlayer player) {
        if (!affirmativePlayers.contains(player)) {
            affirmativePlayers.add(player);
        }
    }

    @Override
    public boolean isAffirmative(IPlayer player) {
        return affirmativePlayers.contains(player);
    }

    @Override
    public int getRequiredNumberOfVotes() {
        return minimumRequiredNumberOfVote;
    }

    @Override
    public boolean isRestartVoteEligible() {
        return affirmativePlayers.size() >= minimumRequiredNumberOfVote;
    }
}
