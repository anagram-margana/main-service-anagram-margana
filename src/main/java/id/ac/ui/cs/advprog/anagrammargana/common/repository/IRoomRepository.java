package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomCodeAlreadyExistException;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface IRoomRepository {
    boolean hasRoomCode(String roomCode);
    
    @NotNull
    Optional<IRoom> findByRoomCodeOrNull(String roomCode);
    
    IRoom findByRoomCodeOrException(String roomCode);
    
    
    /**
     * menambahkan objek IRoom ke repository apabila belum ada,
     * atau throw {@link RoomCodeAlreadyExistException} jika sudah ada
     * @param room objek room yang ingin ditambahkan
     */
    void add(IRoom room);
    
    
    /**
     * Menghapus IRoom room code dari repository apabila ada,
     * atau throw {@link InvalidRoomCodeException} jika belum ada
     * @param roomCode kode room yang ingin dihapus
     */
    void removeRoomByRoomCode(String roomCode);
}
