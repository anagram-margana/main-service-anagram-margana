package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.service.RoomHostValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InGamePageController {
    @Autowired
    RoomHostValidatorService roomHostValidatorService;
    public static final String IS_HOST_ATTR = "is_host";
    public static final String PLAYER_NAME_ATTR = "player_name";
    public static final String IN_GAME_MAIN_HTML = "in-game/main";

    @GetMapping(value = "/in-game")
    public String receiveGetRequestForInGamePage(
            @CookieValue(name = "playerId") long playerId,
            @CookieValue(name = "roomCode") String roomCode,
            @CookieValue(name = "playerName") String playerName,
            Model model) {
        var isHost = roomHostValidatorService.isHost(roomCode, playerId);
        model.addAttribute(IS_HOST_ATTR, isHost);
        model.addAttribute(PLAYER_NAME_ATTR, playerName);
        return IN_GAME_MAIN_HTML;
    }
}
