package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.util.DateTimeUtil;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

public class AllAloneRoundFactory implements IRoundFactory{
    @Override
    @NotNull
    public IRound initiateRound(IRoom room, int roundDurationInMinute, int minimumWords, ISortedChars randomChars) {
        var endTime = DateTimeUtil.getDateFromNow(roundDurationInMinute, TimeUnit.MINUTES);
        
        var round = new Round(room, endTime, minimumWords, randomChars);
        for(var player : room.getMembers()){
            var wordPool = new WordPool();
            round.setWordPool(player, wordPool);
        }
        return round;
    }
}
