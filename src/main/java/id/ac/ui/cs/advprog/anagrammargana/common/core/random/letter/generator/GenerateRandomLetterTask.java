package id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.ISortedChars;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.SortedChars;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.util.MathUtil;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.RandomStringUtil;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

class GenerateRandomLetterTask implements Callable<ISortedChars>, Supplier<ISortedChars> {
    private static String vowels = "aiueo";
    private static String nonVowels = "bcdfghjklmnpqrstvwxyz";
    
    IWordRepository wordRepository;
    int minimumNumberOfWords;
    int numberOfLetter;
    
    
    public GenerateRandomLetterTask(int minimumNumberOfWords, int numberOfLetter,
                                    IWordRepository wordRepository){
       this.wordRepository = wordRepository;
       this.minimumNumberOfWords = minimumNumberOfWords;
       this.numberOfLetter = numberOfLetter;
    }
    
    
    @Override
    public ISortedChars get() {
        return call();
    }
    
    @Override
    public ISortedChars call() {
        var random = new SecureRandom();
        
        
        ISortedChars sortedChars = null;
        while (true){
            int minimumNumberOfVowel = Math.floorDiv(numberOfLetter, 4);
            int maximumNumberOfVowel = MathUtil.ceilDiv(numberOfLetter, 2);
    
            var numberOfVowels = randomIntBetween(minimumNumberOfVowel, maximumNumberOfVowel, random);
            var numberOfNonVowels = numberOfLetter - numberOfVowels;
            var randomizedVowels = RandomStringUtil.randomString(numberOfVowels, vowels, random);
            var randomizedNonVowels = RandomStringUtil.randomString(numberOfNonVowels, nonVowels, random);
    
            sortedChars = new SortedChars(randomizedVowels + randomizedNonVowels);
            var numberOfWordsThatCanBeMade = wordRepository.getWordsFromGivenLetters(sortedChars).size();
            
            if (numberOfWordsThatCanBeMade >= minimumNumberOfWords)
                return sortedChars;
        }
    }
    
    private int randomIntBetween(int min, int maxInclusive, Random random){
        return min + random.nextInt(maxInclusive - min);
    }
    
    
}
