package id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StartNewRoundDto {
    long timeLimit;
    String randomLetters;
    Map<Integer, Integer> wordLengthToNumberOfWordsMapping;
}
