package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions.InvalidAnswer;

import java.util.Date;

public abstract class DefaultAnswerHandlingStrategy implements IGameModeAnswerHandlingStrategy{
    @Override
    public void throwIfAnswerNotValid(
            IWordRepository wordRepository, IRound round, IPlayer playerWhoSubmittedAnAnswer, String answer
    ){
        var currentTime = new Date();
        if (currentTime.compareTo(round.getRoundEndTime()) > 0)
            throw new InvalidAnswer("Time out!");
        
        var answerSortedChars = new SortedChars(answer);
        var currentRoundLetters = round.getRandomLetters();
        if(!wordRepository.doesWordExist(answer)){
            throw new InvalidAnswer("The answer is not recognized");
        }
        if(!answerSortedChars.isSubsequenceOf(currentRoundLetters)){
            throw new InvalidAnswer("The answer cannot be made from the given letters: "
                                            + currentRoundLetters.getSortedString());
        }

        var playerWordPool = round.getWordPool(playerWhoSubmittedAnAnswer);
        if(playerWordPool.doesWordExist(answer)){
            throw new InvalidAnswer("This word has already been taken");
        }
    }
}
