package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.in.game.model.GameMode;

public interface IAnswerHandlingFactory {
    IGameModeAnswerHandlingStrategy createAnswerHandlingStrategy(GameMode gameMode);
}
