package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service;

import java.net.URL;

public interface IPlayerWebsocketAddressUpdaterService {
    void updatePlayerWebsocketAddress(long playerId, URL websocketAddress);
}
