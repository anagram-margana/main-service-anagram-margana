package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import lombok.Setter;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnswerHandlingService {
    @Autowired
    IPlayerRepository playerRepository;
    
    @Autowired
    IMediatorRepository mediatorRepository;
    
    @Setter
    Logger logger = LoggerFactory.getLogger("AnswerHandlingService");
    
    @SneakyThrows
    public void handleAnswer(String roomCode, long playerId, String answer){
        var mediator = mediatorRepository.findByRoomCodeOrException(roomCode);
        var player = playerRepository.findByIdOrException(playerId);
        
        try{
            mediator.handleNewAnswer(player, answer);
            
        }catch (IllegalStateException e){
            var message = String.format("(illegal state) invalid answer %s from %s",
                                        answer, playerId);
            logger.info(message, e);
        }
    }
}
