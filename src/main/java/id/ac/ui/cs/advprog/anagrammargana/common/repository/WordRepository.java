package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.ISortedChars;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.SortedChars;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class WordRepository implements IWordRepository {
    Set<String> words = new HashSet<>();

    @Override
    public Set<String> getWordsFromGivenLetters(ISortedChars sortedChars) {
        Set<String> arrListWords = new HashSet<>();
        for (String word: words) {
            ISortedChars altWord = new SortedChars(word);
            if (altWord.isSubsequenceOf(sortedChars)) {
                arrListWords.add(word);
            }
        }
        return arrListWords;
    }

    @Override
    public boolean doesWordExist(String word) {
        return words.contains(word);
    }

    @Override
    public void addWord(String newWord) {
        words.add(newWord);
    }
}
