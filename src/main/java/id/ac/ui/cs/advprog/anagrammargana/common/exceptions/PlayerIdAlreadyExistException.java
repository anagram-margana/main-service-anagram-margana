package id.ac.ui.cs.advprog.anagrammargana.common.exceptions;

import lombok.Generated;

@Generated
public class PlayerIdAlreadyExistException extends RuntimeException{
    public PlayerIdAlreadyExistException() {
    }
    
    public PlayerIdAlreadyExistException(String message) {
        super(message);
    }
}
