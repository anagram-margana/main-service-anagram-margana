package id.ac.ui.cs.advprog.anagrammargana.common.core.websocket;

public interface IMessageSenderFactory {
    IMessageSender create();
}
