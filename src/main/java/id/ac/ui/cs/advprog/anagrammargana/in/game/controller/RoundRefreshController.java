package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.RoundRefreshData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.RoundRefreshService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/communication-api/msg-from-client")
public class RoundRefreshController {
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    
    @Autowired
    RoundRefreshService roundRefreshService;
    
    
    
    @PostMapping(value = "/refresh-round-information", produces = "application/json")
    public MessageData<RoundRefreshData> handleInformationRefreshRequest(@RequestBody MessageData<Object> incomingMessage)
    {
        playerAuthenticationService.throwIfNotAuthenticated(incomingMessage);
        var roundRefreshData = roundRefreshService.getRoundRefreshData(
                incomingMessage.getRoomCode(), incomingMessage.getPlayerId());
        var responseMessage = new MessageData<RoundRefreshData>();
        responseMessage.copyDataFrom(incomingMessage);
        responseMessage.setType("refresh-round-information");
        responseMessage.setBody(roundRefreshData);
        
        return responseMessage;
    }
    
    
}
