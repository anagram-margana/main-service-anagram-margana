package id.ac.ui.cs.advprog.anagrammargana.common.core.websocket;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface IMessageSender {
    <T2> List<CompletableFuture<String>> send(Iterable<IPlayer> players, String type, T2 message);
    
    <T2> CompletableFuture<String> send(IPlayer player, String type, T2 message);
    
    
    <T> List<CompletableFuture<String>> send(String type, Map<IPlayer, T> playerWithTheirData);
}
