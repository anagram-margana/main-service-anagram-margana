package id.ac.ui.cs.advprog.anagrammargana.common.util;

import lombok.SneakyThrows;

import java.net.URL;

public class CreateUrl {
    private CreateUrl(){}
    
    /**
     * Creating URL() object without having to handle its annoying checked exception
     * @param spec
     * @return URL object
     */
    @SneakyThrows
    public static URL create(String spec){
        return new URL(spec);
    }
    
    @SneakyThrows
    public static URL create(URL context, String spec){
        return new URL(context, spec);
    }
}
