package id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.ISortedChars;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RandomLetterGeneratorSingleton implements IRandomLetterGeneratorSingleton {
    private static RandomLetterGeneratorSingleton instance = new RandomLetterGeneratorSingleton();
    public static RandomLetterGeneratorSingleton getInstance(){
        return instance;
    }
    
    
    ExecutorService poolExecutor = Executors.newFixedThreadPool(4);
    IWordRepository wordRepository;
    
    @Override
    public void setWordRepository(IWordRepository wordRepository){
        this.wordRepository = wordRepository;
    }
    
    public CompletableFuture<ISortedChars> getRandomLetter(int minimumNumberOfWords, int numberOfLetters){
        var generateRandomLetterTask = new GenerateRandomLetterTask(minimumNumberOfWords,
                                                                    numberOfLetters, wordRepository);
        return CompletableFuture.supplyAsync(generateRandomLetterTask);
    }
}
