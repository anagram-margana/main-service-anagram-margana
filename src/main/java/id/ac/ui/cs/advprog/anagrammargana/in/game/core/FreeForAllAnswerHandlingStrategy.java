package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.LatestUpdateDto;
import lombok.Setter;

import java.util.ArrayList;

import static id.ac.ui.cs.advprog.anagrammargana.in.game.util.ScoreboardToListOfScoreDataUtil.getPlayerScoresAsList;

public class FreeForAllAnswerHandlingStrategy extends DefaultAnswerHandlingStrategy{
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    @Override
    public void notifyPlayersWhenAnswerValid(IRound round, IRoom room, IScoreboard scoreboard,
                                             IPlayer playerWhoSubmittedTheAnswer, String answer) {
        var playersToBeNotified = room.getMembers();
        var messageSender = this.messageSenderFactory.create();
        var playerScoreData = getPlayerScoresAsList(scoreboard);
        
        var latestUpdate = new LatestUpdateDto();
        var wordPool = round.getWordPool(playerWhoSubmittedTheAnswer);
        var wordsFound = wordPool.getWordsFound();
        
        latestUpdate.setRecentlyDiscoveredWord(answer);
        latestUpdate.setWordsFound(new ArrayList<>(wordsFound));
        latestUpdate.setPlayerIdWhoSubmittedAnswer(playerWhoSubmittedTheAnswer.getId());
        latestUpdate.setPlayerNameWhoSubmittedAnswer(playerWhoSubmittedTheAnswer.getName());
        latestUpdate.setScoreData(playerScoreData);
        
        messageSender.send(playersToBeNotified, "ffa-new-answer-submission", latestUpdate);
    }
}
