package id.ac.ui.cs.advprog.anagrammargana.create.join.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomClosedException;
import id.ac.ui.cs.advprog.anagrammargana.create.join.service.ICreateJoinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;


@Controller
public class CreateAndJoinRoomController {
    public static final String ERROR_MSG_ATTR = "error_msg";

    @Autowired
    ICreateJoinService createJoinService;

    @GetMapping(value = "/")
    public String mainPage() {
        return "create-join/create-join";
    }


    @GetMapping(value = "/join")
    public String joinAsGuestPage(@RequestParam(name="room", defaultValue="") String roomCode,
                                  Model model,
                                  HttpServletRequest request) {
        // supaya redirectAttrs.addFlashAttribute bisa berjalan dengan baik.
        // tanpa ini, ketika jsessionid belom di-set, akan terjadi 404
        request.getSession(true);
        
        if (!model.containsAttribute(ERROR_MSG_ATTR))
            model.addAttribute(ERROR_MSG_ATTR, null);
        model.addAttribute("roomCode", roomCode);
        return "create-join/join";
    }

    Logger logger = LoggerFactory.getLogger("create-join-controller");

    
    @PostMapping(value = "/join")
    public RedirectView registerGuest(@RequestParam String playerName,
                                      @RequestParam String roomCode,
                                      HttpServletResponse response,
                                      RedirectAttributes redirectAttrs) {
        try{
            var player = createJoinService.createGuest(playerName, roomCode);
            setIdentifyingCookies(response, player.getId(), playerName, roomCode,
                                  player.getSessionToken());
            logger.info("new guest {} for room {}", player, roomCode);
            return new RedirectView("lobby/");
            
        }catch (InvalidRoomCodeException e){
            redirectAttrs.addFlashAttribute(ERROR_MSG_ATTR, "Invalid room code");
            var redirectUrl = "/join/?room=" + URLEncoder.encode(roomCode, StandardCharsets.UTF_8);
            return new RedirectView(redirectUrl);
        }catch (RoomClosedException e){
            redirectAttrs.addFlashAttribute(ERROR_MSG_ATTR, "This room is closed already");
            var redirectUrl = "/join/?room=" + URLEncoder.encode(roomCode, StandardCharsets.UTF_8);
            return new RedirectView(redirectUrl);
        }
    }
    

    @PostMapping(value = "/create")
    public RedirectView registerHost(@RequestParam String playerName,
                                     HttpServletResponse response) {
        var room = createJoinService.createHost(playerName);
        var playerAsHost = room.getRoomHost();
        setIdentifyingCookies(response, playerAsHost.getId(), playerName, room.getCode(),
                              playerAsHost.getSessionToken());
    
        logger.info("new host {}", playerName);
        return new RedirectView("lobby/");
    }
    
    protected void setIdentifyingCookies(HttpServletResponse response, long playerId, String playerName, String roomCode,
                                         String playerSessionToken) {
        var roomCodeCookie = createJoinService.getRoomCodeCookie(roomCode);
        var playerIdCookie = createJoinService.getPlayerIdCookie(playerId);
        var playerNameCookie = createJoinService.getPlayerNameCookie(playerName);
        var sessionCookie = createJoinService.getSessionTokenCookie(playerSessionToken);
        
        response.addCookie(playerIdCookie);
        response.addCookie(playerNameCookie);
        response.addCookie(roomCodeCookie);
        response.addCookie(sessionCookie);
    }
}
