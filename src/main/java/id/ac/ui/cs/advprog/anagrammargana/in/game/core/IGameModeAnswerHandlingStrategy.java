package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;

public interface IGameModeAnswerHandlingStrategy {
    void throwIfAnswerNotValid(IWordRepository wordRepository, IRound round, IPlayer player, String answer);
    void notifyPlayersWhenAnswerValid(IRound round, IRoom room, IScoreboard scoreboard, IPlayer player, String answer);
}
