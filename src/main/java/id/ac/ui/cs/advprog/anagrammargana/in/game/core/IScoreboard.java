package id.ac.ui.cs.advprog.anagrammargana.in.game.core;


import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;

import java.util.Map;

public interface IScoreboard {
    long getScore(IPlayer player);
    IPlayer getPlayerByRank(int rank);
    long getScoreByRank(int rank);
    void increaseScore(IPlayer player, int i);
    void registerPlayer(IPlayer player);
    Map<IPlayer, Integer> asHashmap();
}
