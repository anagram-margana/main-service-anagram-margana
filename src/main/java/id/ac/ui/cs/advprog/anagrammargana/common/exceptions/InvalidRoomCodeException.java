package id.ac.ui.cs.advprog.anagrammargana.common.exceptions;

import lombok.Generated;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Generated
@ResponseStatus(code= HttpStatus.BAD_REQUEST, reason="Room code not found")
public class InvalidRoomCodeException extends RuntimeException {
    public InvalidRoomCodeException() {
    }
    
    public InvalidRoomCodeException(String message) {
        super(message);
    }
}
