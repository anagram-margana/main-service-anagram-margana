package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.PlayerIdAlreadyExistException;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface IPlayerRepository {
    boolean hasPlayerId(long id);
    
    @NotNull Optional<IPlayer> findByIdOrNull(long id);
    @NotNull IPlayer findByIdOrException(long id);
    
    
    /**
     * Menambahkan objek IPlayer ke repository kalau belum ada,
     * atau throw {@link PlayerIdAlreadyExistException} kalau sudah ada
     * @param player objek player yang ingin ditambahkan
     */
    void add(IPlayer player);
    
    
    /**
     * Mengecek kebenaran session token dan mengembalikan objek IPlayer yang bersangkutan jika token valid. <br>
     * Apabila autentikasi gagal, throw {@link InvalidSessionTokenException} <br>
     * Apabila player ID tidak ditemukan, throw {@link InvalidPlayerIdException} <br>
     *
     * @param id id dari objek player yang ingin di-fetch
     * @param sessionToken token session yang ingin diperiksa kebenarannya
     */
    IPlayer authenticateAndGetPlayer(long id, String sessionToken);
    
    
    /**
     * Menghapus id IPlayer dari repository apabila ada,
     * atau throw {@link InvalidPlayerIdException} jika belum ada
     * @param playerId id player yang ingin dihapus
     */
    void removePlayerWithId(long playerId);
}
