package id.ac.ui.cs.advprog.anagrammargana.common.core;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.*;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface IMediator {
    CompletableFuture<Void> initializeNewRound(int roundDurationInMinute,
                                               int minimumNumberOfWords,
                                               IRoundFactory roundFactory,
                                               IGameModeAnswerHandlingStrategy gameMode);
    
    Map<Integer, Integer> getWordLengthToNumberOfWordsMapping();
    
    void handleNewAnswer(IPlayer player, String answer);
    
    boolean handlePlayerVoteForRestartingRound(IPlayer player);
    int getNumberOfRestartVotes();
    int getRequiredNumberOfVotes();
    boolean isRestartEligible();
    void restartRound();
    
    String getRoomCode();
    IRoom getRoom();
    IScoreboard getScoreBoard();
    IRound getCurrentRound();
    IRestartVote getRestartVote();
    IWordRepository getWordRepository();
}
