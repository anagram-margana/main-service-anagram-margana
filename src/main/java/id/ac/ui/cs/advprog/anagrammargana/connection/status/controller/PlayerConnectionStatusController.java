package id.ac.ui.cs.advprog.anagrammargana.connection.status.controller;

import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.connection.status.service.PlayerConnectionStatusService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/communication-api/msg-from-client")
public class PlayerConnectionStatusController {
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @Autowired
    PlayerConnectionStatusService playerConnectionStatusService;
    
    
    @PostMapping(value = "/disconnected", produces = "application/json")
    public void someoneDisconnected(@RequestBody MessageData<Object> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        playerConnectionStatusService.handleSomeoneDisconnected(messageDTO.getRoomCode(),
                                                                messageDTO.getPlayerId());
    }
    
    @PostMapping(value = "/connected", produces = "application/json")
    public void someoneConnected(@RequestBody MessageData<Object> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        playerConnectionStatusService.handleSomeoneConnected(messageDTO.getRoomCode(),
                                                             messageDTO.getPlayerId());
    }
}
