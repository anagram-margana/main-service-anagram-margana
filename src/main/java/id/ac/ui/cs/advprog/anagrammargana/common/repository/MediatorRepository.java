package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IMediator;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomCodeAlreadyExistException;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Repository
public class MediatorRepository implements IMediatorRepository{
    Cache<String, IMediator> mediators = CacheBuilder
            .newBuilder()
            .expireAfterAccess(3, TimeUnit.HOURS)
            .build();
    
    
    @Override
    public boolean hasMediator(String roomCode) {
        return findByRoomCodeOrNull(roomCode).isPresent();
    }
    
    @Override
    public Optional<IMediator> findByRoomCodeOrNull(String roomCode) {
        var mediator = mediators.getIfPresent(roomCode);
        return Optional.ofNullable(mediator);
    }
    
    @Override
    public IMediator findByRoomCodeOrException(String roomCode){
        var nullableMediator = findByRoomCodeOrNull(roomCode);
        if (nullableMediator.isEmpty())
            throw new InvalidRoomCodeException("Invalid room code");
        return nullableMediator.get();
    }
    
    @Override
    public void add(IMediator room) {
        if (hasMediator(room.getRoomCode())) {
            var errMsg = String.format("Room code %s has been taken already (IMediatorRepository)",
                                       room.getRoomCode());
            throw new RoomCodeAlreadyExistException(errMsg);
        }
        mediators.put(room.getRoomCode(), room);
    }
    
    @Override
    public void removeMediatorByRoomCode(String roomCode) {
        if (!hasMediator(roomCode))
            throw new InvalidRoomCodeException("Not found IMediator for room code: " + roomCode);
        mediators.invalidate(roomCode);
    }
}
