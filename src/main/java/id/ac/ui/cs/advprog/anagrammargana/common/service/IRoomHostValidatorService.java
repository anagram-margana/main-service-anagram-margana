package id.ac.ui.cs.advprog.anagrammargana.common.service;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomHostException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;

public interface IRoomHostValidatorService {
    boolean isHost(String roomCode, long playerId);
    
    void throwIfNotRoomHost(String roomCode, long playerId)
            throws InvalidRoomCodeException, InvalidRoomHostException;
    
    <T> boolean isHost(MessageData<T> messageData);
    
    <T> void throwIfNotRoomHost(MessageData<T> messageData)
            throws InvalidRoomCodeException, InvalidRoomHostException;
}
