package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.AnswerHandlingFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.FactoryOfRoundFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.IAnswerHandlingFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.IFactoryOfRoundFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions.InvalidGameSettingsData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.GameSettingsData;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StartNewRoundService {
    @Autowired
    IMediatorRepository mediatorRepository;
    
    @Setter
    IFactoryOfRoundFactory factoryOfRoundFactory = new FactoryOfRoundFactory();
    
    @Setter
    IAnswerHandlingFactory answerHandlingFactory = new AnswerHandlingFactory();
    
    
    public void startNewRound(String roomCode, GameSettingsData gameSettingsData){
        var timeLimit = gameSettingsData.getTimeLimit();
        var minimumNumberOfWords = gameSettingsData.getMinWords();
        
        if (gameSettingsData.getMinWords() > 50)
            throw new InvalidGameSettingsData("minimum words cannot be more than 50");
        if (gameSettingsData.getTimeLimit() < 1 || gameSettingsData.getTimeLimit() > 5)
            throw new InvalidGameSettingsData("time limit must be between 1 and 5 (minutes)");
        
        var mediator = mediatorRepository.findByRoomCodeOrException(roomCode);
        var gameMode = gameSettingsData.getGameMode();
        
        var answerHandlingStrategy = answerHandlingFactory.createAnswerHandlingStrategy(gameMode);
        var roundFactory = factoryOfRoundFactory.createRoundFactory(gameMode);
        
        mediator.initializeNewRound(timeLimit, minimumNumberOfWords,
                                    roundFactory, answerHandlingStrategy);
    }
}
