package id.ac.ui.cs.advprog.anagrammargana.create.join.random;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;



@Component
public class RoomRandomUtil {
    @Autowired
    IRoomRepository roomRepository;
    
    
    public String getRandomRoomCode(){
        return getRandomRoomCode(new Random());
    }
    
    public String getRandomRoomCode(Random random){
        String randomRoomCode;
        
        do{
            randomRoomCode = RandomStringUtil.randomString(5, "0123456789", random);
        }while (roomRepository.hasRoomCode(randomRoomCode));
        
        return randomRoomCode;
    }
}
