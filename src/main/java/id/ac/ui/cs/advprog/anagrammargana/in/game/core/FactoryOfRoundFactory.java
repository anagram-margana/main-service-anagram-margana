package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.in.game.model.GameMode;

public class FactoryOfRoundFactory implements IFactoryOfRoundFactory {
    @Override
    public IRoundFactory createRoundFactory(GameMode gameMode) {
        if (gameMode == GameMode.ALL_ALONE) {
            return new AllAloneRoundFactory();
        }
        return new FreeForAllRoundFactory();
    }
}

