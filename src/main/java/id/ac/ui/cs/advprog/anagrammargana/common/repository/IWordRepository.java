package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.ISortedChars;

import java.util.Collection;

public interface IWordRepository {
    Collection<String> getWordsFromGivenLetters(ISortedChars sortedChars);
    boolean doesWordExist(String word);
    void addWord(String newWord);
}
