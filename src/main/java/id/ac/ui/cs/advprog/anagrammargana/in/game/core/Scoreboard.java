package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;

import java.util.*;

public class Scoreboard implements IScoreboard{

    Map<IPlayer, Integer> playersAndScores = new LinkedHashMap<>();  // supaya terurut berdasarkan join time & rank
    

    @Override
    public long getScore(IPlayer player) {
        if (playersAndScores.containsKey(player)){
            return playersAndScores.get(player);
        }
        return 0;
    }

    @Override
    public IPlayer getPlayerByRank(int rank) {
        List<Map.Entry<IPlayer, Integer>> entries =
                new ArrayList<>(playersAndScores.entrySet());
        entries.sort(Map.Entry.comparingByValue());
        Map<IPlayer, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<IPlayer, Integer> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        List<IPlayer> playerIds = new ArrayList<>( sortedMap.keySet() );
        return playerIds.get(rank-1);
    }

    @Override
    public long getScoreByRank(int rank) {
        List<Map.Entry<IPlayer, Integer>> entries =
                new ArrayList<>(playersAndScores.entrySet());
        entries.sort(Map.Entry.comparingByValue());
        Map<IPlayer, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<IPlayer, Integer> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        List<Integer> scores = new ArrayList<>(sortedMap.values());


        return scores.get(rank-1);
    }

    @Override
    public void increaseScore(IPlayer player, int increment) {
        playersAndScores.computeIfAbsent(player, player1 -> 0);
        var currentScore = playersAndScores.get(player);
        playersAndScores.put(player, currentScore + increment);
    }

    @Override
    public void registerPlayer(IPlayer player) {
        playersAndScores.computeIfAbsent(player, player1 -> 0);
    }
    
    @Override
    public Map<IPlayer, Integer> asHashmap() {
        return playersAndScores;
    }
}
