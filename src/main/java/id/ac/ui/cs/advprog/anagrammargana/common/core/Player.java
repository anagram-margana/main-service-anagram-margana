package id.ac.ui.cs.advprog.anagrammargana.common.core;

import id.ac.ui.cs.advprog.anagrammargana.common.Constants;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Setter;

import java.net.URL;

@EqualsAndHashCode
public class Player implements IPlayer{
    private final String name;
    private final long id;
    private final String sessionToken;
    
    @Setter
    @EqualsAndHashCode.Exclude
    private URL websocketUrl;
    
    
    public Player(String name, long id, String sessionToken){
        this(name, id, sessionToken,
             Constants.getWebsocketServerDefaultAddress());
    }
    
    public Player(String name, long id, String sessionToken, URL websocketUrl){
        this.name = name;
        this.id = id;
        this.sessionToken = sessionToken;
        this.websocketUrl = websocketUrl;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public long getId() {
        return id;
    }
    
    @Override
    public String getSessionToken() {
        return sessionToken;
    }
    
    @Override
    public URL getWebsocketUrl() {
        return websocketUrl;
    }
    
    
    @Generated
    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", sessionToken='" + sessionToken + '\'' +
                ", websocketUrl=" + websocketUrl +
                '}';
    }
}
