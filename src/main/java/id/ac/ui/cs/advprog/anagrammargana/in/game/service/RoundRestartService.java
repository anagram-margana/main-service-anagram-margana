package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.VoteInformationDto;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoundRestartService {
    @Setter
    Logger logger = LoggerFactory.getLogger("round-restart-vote");
    
    @Autowired
    IPlayerRepository playerRepository;
    
    @Autowired
    IMediatorRepository mediatorRepository;
    
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    public boolean handleSomeoneVotedForRestart(String roomCode, long playerId){
        var player = playerRepository.findByIdOrException(playerId);
        var mediator = mediatorRepository.findByRoomCodeOrException(roomCode);
        var room = mediator.getRoom();
        
        var isVoteSuccess = mediator.handlePlayerVoteForRestartingRound(player);
        
        if (!isVoteSuccess)
            return false;
    
        var messageSender = messageSenderFactory.create();
        var numberOfAffirmativePlayers = mediator.getNumberOfRestartVotes();
        var requiredNumberOfVotes = mediator.getRequiredNumberOfVotes();
        var roomMembers = room.getMembers();
        
        var voteInformation = new VoteInformationDto(numberOfAffirmativePlayers,
                                                     requiredNumberOfVotes);
    
        messageSender.send(roomMembers, "vote-restart", voteInformation);
    
        var isRestartEligible = mediator.isRestartEligible();
        if (isRestartEligible){
            var roomHost = room.getRoomHost();
            messageSender.send(roomHost, "vote-restart-eligible", "");
        }
        return true;
    }
    
    
    public void restartRound(String roomCode){
        var mediator = mediatorRepository.findByRoomCodeOrException(roomCode);
        try{
            mediator.restartRound();
            
            var messageSender = messageSenderFactory.create();
            var room = mediator.getRoom();
            var members = room.getMembers();
            messageSender.send(members, "round-restart", "");
            
        }catch (IllegalStateException e){
            var message = String.format(
                    "(illegal state) room %s is trying to restart a round when the required number of votes " +
                            "has not been met yet", roomCode);
            logger.info(message, e);
        }
    }
}
