/**
 * Sebagian besar komunikasi antara client-server, khususnya komunikasi real-time,
 * dilakukan dengan menggunakan websocket. Websocket ini dipisah menjadi microservice
 * tersendiri sehingga dapat membantu mengurangi latensi network client apabila
 * microservice tersebut disebar ke berbagai benua. <br><br>
 *
 * Semua pesan yang datang dari client akan dikirim ke `/communication-api/msg-from-client/{message-type}`
 *
 * Mengenai mekanisme dan alur komunikasi, bisa dilihat pada DOCUMENTATION.md di repository
 * microservice yang bersangkutan
 *
 */

package id.ac.ui.cs.advprog.anagrammargana.websocket.communication;