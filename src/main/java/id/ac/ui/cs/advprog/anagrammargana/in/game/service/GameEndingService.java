package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameEndingService {
    @Autowired
    IPlayerRepository playerRepository;
    
    @Autowired
    IMediatorRepository mediatorRepository;
    
    @Autowired
    IRoomRepository roomRepository;
    
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    public void finishTheGame(String roomCode){
        var room = roomRepository.findByRoomCodeOrException(roomCode);
        var members = room.getMembers();
    
        for (var member: members) {
            var memberId = member.getId();
            playerRepository.removePlayerWithId(memberId);
        }
        
        mediatorRepository.removeMediatorByRoomCode(roomCode);
        roomRepository.removeRoomByRoomCode(roomCode);
        
        var messageSender = messageSenderFactory.create();
        messageSender.send(members, "game-ended", "");
    }
}
