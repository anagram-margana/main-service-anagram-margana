package id.ac.ui.cs.advprog.anagrammargana.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
public class ConstantsInjector {
    @Value("${main-server-addr}")
    public void setMainServerAddress(String value) throws MalformedURLException {
        Constants.setMainServerAddress(new URL(value));
    }
    
    @Value("${websocket-server-addr}")
    public void setWebsocketServerAddress(String value) throws MalformedURLException {
        Constants.setWebsocketServerDefaultAddress(new URL(value));
    }
}
