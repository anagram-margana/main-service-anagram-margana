package id.ac.ui.cs.advprog.anagrammargana.create.join.random;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Random;


@Component
public class PlayerRandomUtil {
    
    
    @Autowired
    IPlayerRepository playerRepository;
    
    public String getRandomPlayerSessionToken(){
        return getRandomPlayerSessionToken(new SecureRandom());
    }
    
    public String getRandomPlayerSessionToken(Random random){
        return RandomStringUtil.randomString(24, random);
    }
    
    public long getRandomPlayerId(){
        return getRandomPlayerId(new Random());
    }
    
    public long getRandomPlayerId(Random random){
        long randomId;
    
        // because javascript can only store up to 53 bit integers.
        // see: https://stackoverflow.com/a/17320771/7069108
        var maximumId = 2L << 50;
        
        do{
            randomId = random.nextLong();
        }while (randomId <= 0 || randomId > maximumId || playerRepository.hasPlayerId(randomId));
        
        return randomId;
    }
}
