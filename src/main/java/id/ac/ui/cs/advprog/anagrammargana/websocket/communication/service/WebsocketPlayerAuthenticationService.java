package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebsocketPlayerAuthenticationService implements IWebsocketPlayerAuthenticationService {
    @Autowired
    IPlayerRepository playerRepository;
    
    @Autowired
    IRoomRepository roomRepository;
    
    @Override
    public boolean authenticate(String roomCode, long playerId, String sessionToken) {
        try{
            throwIfNotAuthenticated(roomCode, playerId, sessionToken);
            return true;
        }catch (InvalidSessionTokenException|InvalidPlayerIdException|InvalidRoomCodeException ignored){
            return false;
        }
    }
    
    @Override
    public <T> boolean authenticate(MessageData<T> messageDTO) {
        var roomCode = messageDTO.getRoomCode();
        var playerId = messageDTO.getPlayerId();
        var sessionToken = messageDTO.getSessionToken();
        return authenticate(roomCode, playerId, sessionToken);
    }
    
    
    @Override
    public void throwIfNotAuthenticated(String roomCode, long playerId, String sessionToken)
            throws InvalidPlayerIdException, InvalidSessionTokenException, InvalidRoomCodeException {
        
        var room = roomRepository.findByRoomCodeOrNull(roomCode);
        if (room.isEmpty())
            throw new InvalidRoomCodeException("Not found room code: " + roomCode);
        playerRepository.authenticateAndGetPlayer(playerId, sessionToken);
    }
    
    @Override
    public <T> void throwIfNotAuthenticated(MessageData<T> messageDTO)
            throws InvalidRoomCodeException, InvalidPlayerIdException, InvalidSessionTokenException {
        var roomCode = messageDTO.getRoomCode();
        var playerId = messageDTO.getPlayerId();
        var sessionToken = messageDTO.getSessionToken();
        
        throwIfNotAuthenticated(roomCode, playerId, sessionToken);
    }
}
