package id.ac.ui.cs.advprog.anagrammargana.lobby.service;

import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LobbyMessageService implements ILobbyMessageService {
    @Autowired
    IRoomRepository roomRepository;
    
    @Autowired
    IPlayerRepository playerRepository;
    
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @Override
    public MessageData<List<String>> refreshRoomMember(MessageData<Object> messageData) {
        playerAuthenticationService.throwIfNotAuthenticated(messageData);
        
        var roomCode = messageData.getRoomCode();
        var room = roomRepository.findByRoomCodeOrException(roomCode);
    
        var listOfNames = new ArrayList<>(room.getMemberNames());
    
        var ret = new MessageData<List<String>>();
        ret.copyDataFrom(messageData);
        ret.setType("update-room-member");
        ret.setBody(listOfNames);
        return ret;
    }
    
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    @Override
    public void startGame(String roomCode) {
        var room = roomRepository.findByRoomCodeOrException(roomCode);
        var messageSender = messageSenderFactory.create();
        
        room.close();
        messageSender.send(room.getMembers(), "start-first-round", "");
    }
}
