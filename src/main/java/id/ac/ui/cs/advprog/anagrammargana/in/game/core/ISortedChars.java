package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

public interface ISortedChars {
    String getSortedString();
    int countChar(char c);
    boolean isSubsequenceOf(ISortedChars sortedChars);
    String toString();
    boolean equals(Object object);
}
