package id.ac.ui.cs.advprog.anagrammargana.common.core.websocket;

public class MessageSenderFactory implements IMessageSenderFactory {
    @Override
    public IMessageSender create() {
        return new MessageSender();
    }
}
