package id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions;

import lombok.Generated;

@Generated
public class InvalidAnswer extends RuntimeException{
    public InvalidAnswer() {
    }
    
    public InvalidAnswer(String message) {
        super(message);
    }
}
