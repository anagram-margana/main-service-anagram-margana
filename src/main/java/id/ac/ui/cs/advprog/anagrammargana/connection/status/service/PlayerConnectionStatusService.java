package id.ac.ui.cs.advprog.anagrammargana.connection.status.service;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.service.RoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.PlayerConnectionStatusDto;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerConnectionStatusService {
    @Setter
    Logger logger = LoggerFactory.getLogger("player-disconnected");
    
    @Autowired
    IPlayerRepository playerRepository;
    
    @Autowired
    IRoomRepository roomRepository;
    
    @Autowired
    RoomHostValidatorService roomHostValidatorService;
    
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    public void handleSomeoneDisconnected(String roomCode, long playerId){
    
        var player = playerRepository.findByIdOrException(playerId);
        var room = roomRepository.findByRoomCodeOrException(roomCode);
        var isRoomHost = roomHostValidatorService.isHost(roomCode, playerId);
        var playerDisconnectedMessage = new PlayerConnectionStatusDto(playerId, player.getName(), isRoomHost);
        
        logger.info("A player is disconnected: id={} room={} name={}",
                    player.getId(), roomCode, player.getName());
        
        var messageSender = messageSenderFactory.create();
        messageSender.send(room.getMembers(),
                           "player-disconnected",
                           playerDisconnectedMessage);
    }
    
    public void handleSomeoneConnected(String roomCode, long playerId){
    
        var player = playerRepository.findByIdOrException(playerId);
        var room = roomRepository.findByRoomCodeOrException(roomCode);
        var isRoomHost = roomHostValidatorService.isHost(roomCode, playerId);
        var playerDisconnectedMessage = new PlayerConnectionStatusDto(playerId, player.getName(), isRoomHost);
        
        logger.info("A player is connected: id={} room={} name={}",
                    player.getId(), roomCode, player.getName());
        
        var messageSender = messageSenderFactory.create();
        messageSender.send(room.getMembers(),
                           "player-connected",
                           playerDisconnectedMessage);
    }
}
