package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URL;

@Service
public class PlayerWebsocketAddressUpdaterService implements IPlayerWebsocketAddressUpdaterService {
    @Autowired
    IPlayerRepository playerRepository;
    
    @Override
    public void updatePlayerWebsocketAddress(long playerId, URL websocketAddress) {
        var player = playerRepository.findByIdOrException(playerId);
        player.setWebsocketUrl(websocketAddress);
    }
}
