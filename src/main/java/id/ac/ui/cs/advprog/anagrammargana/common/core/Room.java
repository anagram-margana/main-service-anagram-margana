package id.ac.ui.cs.advprog.anagrammargana.common.core;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.PlayerIdAlreadyExistException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomClosedException;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@EqualsAndHashCode
public class Room implements IRoom{
    boolean closed = false;
    String roomCode;
    IPlayer roomHost;
    Map<Long, IPlayer> membersAndHost = new LinkedHashMap<>();  // supaya terurut berdasarkan join time
    
    public Room(IPlayer roomHost, String roomCode){
        this.roomCode = roomCode;
        this.roomHost = roomHost;
        membersAndHost.put(roomHost.getId(), roomHost);
    }
    
    
    @Override
    public void close() {
        closed = true;
    }
    
    @Override
    public String getCode() {
        return roomCode;
    }
    
    @Override
    public IPlayer getRoomHost() {
        return roomHost;
    }
    
    @Override
    public Collection<IPlayer> getMembers() {
        return membersAndHost.values();
    }
    
    @Override
    public int getMemberSize() {
        return membersAndHost.size();
    }
    
    @Override
    public Collection<String> getMemberNames() {
        var ret = new ArrayList<String>();
    
        for (IPlayer player: membersAndHost.values()) {
            ret.add(player.getName());
        }
        
        return ret;
    }
   
    @Override
    public boolean hasMemberWithId(long playerId) {
        return membersAndHost.containsKey(playerId);
    }
    
    @Override
    public void addMember(IPlayer player) {
        if (closed){
            throw new RoomClosedException("Cannot add more member to this room");
        }
        
        var playerId = player.getId();
        if (hasMemberWithId(playerId)) {
            var errMsg = String.format("A player with ID %s has already joined this room", playerId);
            throw new PlayerIdAlreadyExistException(errMsg);
        }
        membersAndHost.put(playerId, player);
    }
    
    @Override
    public void removeMemberWithId(long playerId) {
        if (!hasMemberWithId(playerId)) {
            var errMsg = String.format("Player with ID %s is not a member of this room", playerId);
            throw new InvalidPlayerIdException(errMsg);
        }
        membersAndHost.remove(playerId);
    }
    
}
