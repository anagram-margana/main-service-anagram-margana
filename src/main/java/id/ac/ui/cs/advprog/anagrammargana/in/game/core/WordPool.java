package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class WordPool implements IWordPool{

    Set<String> wordsFound;


    public WordPool(){
        this.wordsFound = new LinkedHashSet<>();
    }

    @Override
    public boolean doesWordExist(String word) {
        return wordsFound.contains(word);
    }

    public boolean addWord(String word) {
        if (!wordsFound.contains(word)){
            wordsFound.add(word);
            return true;
        }
        return false;
    }

    @Override
    public Collection<String> getWordsFound() {
        return wordsFound;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var wordPool = (WordPool) o;
        return Objects.equals(wordsFound, wordPool.wordsFound);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wordsFound);
    }
}
