package id.ac.ui.cs.advprog.anagrammargana.common.core;

import java.net.URL;

public interface IPlayer {
    String getName();
    long getId();
    String getSessionToken();
    URL getWebsocketUrl();
    void setWebsocketUrl(URL websocketAddress);
    
    /**
     * Check whether two player has exactly the same (name, id, session token).
     * Return true IF AND ONLY IF those three attributes are all the same
     * @param o other object to be compared
     */
    boolean equals(Object o);
    int hashCode();
}
