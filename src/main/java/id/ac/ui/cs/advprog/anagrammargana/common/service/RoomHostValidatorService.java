package id.ac.ui.cs.advprog.anagrammargana.common.service;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomHostException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomHostValidatorService implements IRoomHostValidatorService {
    @Autowired
    IRoomRepository roomRepository;
    
    
    @Override
    public boolean isHost(String roomCode, long playerId) {
        var nullableRoom = roomRepository.findByRoomCodeOrNull(roomCode);
        if (nullableRoom.isEmpty())
            throw new InvalidRoomCodeException("Invalid room code");
        
        var room = nullableRoom.get();
        return room.getRoomHost().getId() == playerId;
    }
    
    @Override
    public void throwIfNotRoomHost(String roomCode, long playerId)
            throws InvalidRoomCodeException, InvalidRoomHostException {
        if (!isHost(roomCode, playerId))
            throw new InvalidRoomHostException(String.format("%s is not a room host of %s", playerId, roomCode));
    }
    
    
    @Override
    public <T> boolean isHost(MessageData<T> messageData) {
        var roomCode = messageData.getRoomCode();
        var playerId = messageData.getPlayerId();
        return isHost(roomCode, playerId);
    }
    
    @Override
    public <T> void throwIfNotRoomHost(MessageData<T> messageData)
            throws InvalidRoomCodeException, InvalidRoomHostException {
        var roomCode = messageData.getRoomCode();
        var playerId = messageData.getPlayerId();
        if (!isHost(messageData))
            throw new InvalidRoomHostException(String.format("%s is not a room host of %s", playerId, roomCode));
    }
    
}
