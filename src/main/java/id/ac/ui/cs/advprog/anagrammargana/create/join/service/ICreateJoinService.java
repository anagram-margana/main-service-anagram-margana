package id.ac.ui.cs.advprog.anagrammargana.create.join.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;

import javax.servlet.http.Cookie;

public interface ICreateJoinService {
    IRoom createHost(String name);
    IPlayer createGuest(String name, String roomCode);
    
    Cookie getPlayerNameCookie(String playerName);
    
    Cookie getPlayerIdCookie(long playerId);
    Cookie getRoomCodeCookie(String roomCode);
    Cookie getSessionTokenCookie(String sessionToken);
}
