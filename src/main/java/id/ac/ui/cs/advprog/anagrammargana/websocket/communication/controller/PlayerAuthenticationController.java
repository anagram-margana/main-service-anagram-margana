package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.controller;


import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.PlayerWebsocketAddressUpdaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.net.URL;

@RestController
@RequestMapping("/communication-api")
public class PlayerAuthenticationController {
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @Autowired
    PlayerWebsocketAddressUpdaterService playerWebsocketAddressUpdater;
    
    
    @PostMapping("/init")
    public ResponseEntity<String> authenticate(@RequestParam String roomCode,
                                               @RequestParam long playerId,
                                               @RequestParam String sessionToken,
                                               @RequestParam String websocketServiceAddress){
        var isAuthenticated =
                playerAuthenticationService.authenticate(roomCode, playerId, sessionToken);
        
        URL websocketServiceAddressUrl;
        try{
            websocketServiceAddressUrl = new URL(websocketServiceAddress);
        } catch (MalformedURLException e) {
            return ResponseEntity.ok("-1");
        }
        
        if (isAuthenticated) {
            playerWebsocketAddressUpdater.updatePlayerWebsocketAddress(playerId, websocketServiceAddressUrl);
            return ResponseEntity.ok("1");
        }
        return ResponseEntity.ok("0");
    }
}
