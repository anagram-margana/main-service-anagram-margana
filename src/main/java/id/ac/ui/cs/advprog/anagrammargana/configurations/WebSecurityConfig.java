package id.ac.ui.cs.advprog.anagrammargana.configurations;


import lombok.Generated;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@Generated
public class WebSecurityConfig {
    @Value("${deployment-mode}")
    int deploymentMode;
    
    
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable();
        
        if (deploymentMode == 1)
            http.requiresChannel().anyRequest().requiresSecure();
        else if (deploymentMode == -1)
            http.requiresChannel().anyRequest().requiresInsecure();
        return http.build();
    }
}