package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;

import javax.validation.constraints.NotNull;

public interface IRoundFactory {
    @NotNull
    IRound initiateRound(IRoom room, int roundDurationInMinute, int minimumWords, ISortedChars randomChars);
}
