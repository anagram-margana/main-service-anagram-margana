package id.ac.ui.cs.advprog.anagrammargana.common;

import lombok.Getter;
import lombok.Setter;

import java.net.URL;


public class Constants {
    @Getter
    @Setter
    private static URL mainServerAddress;
    
    @Getter
    @Setter
    private static URL websocketServerDefaultAddress;
    
    private Constants(){}
}
