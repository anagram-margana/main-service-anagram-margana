package id.ac.ui.cs.advprog.anagrammargana.common.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator.IRandomLetterGeneratorSingleton;
import id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator.RandomLetterGeneratorSingleton;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.util.MathUtil;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.*;
import id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions.InvalidAnswer;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.StartNewRoundDto;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@EqualsAndHashCode
public class Mediator implements IMediator{
    
    IRoom room;
    IScoreboard scoreboard;
    @Nullable
    IRound currentRound;
    IRestartVote restartVote;
    IWordRepository wordRepository;
    IGameModeAnswerHandlingStrategy answerHandler;
    
    @Setter
    IRandomLetterGeneratorSingleton letterRandomizer = RandomLetterGeneratorSingleton.getInstance();
    
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    public Mediator(IRoom room, IWordRepository wordRepository){
        this(room, wordRepository, new RestartVote(0), new Scoreboard());
    }
    
    
    public Mediator(IRoom room, IWordRepository wordRepository,
                    IRestartVote restartVote, IScoreboard scoreboard,
                    IGameModeAnswerHandlingStrategy answerHandler, IRound currentRound){
        this(room, wordRepository, restartVote, scoreboard);
        this.answerHandler = answerHandler;
        this.currentRound = currentRound;
    }
    
    
    public Mediator(IRoom room, IWordRepository wordRepository, IRestartVote restartVote,
                    IScoreboard scoreboard){
        this.room = room;
        this.wordRepository = wordRepository;
        this.restartVote = restartVote;
        
        this.scoreboard = scoreboard;
        for (var member: room.getMembers()) {
            scoreboard.registerPlayer(member);
        }
    }

    
    
    @Override
    public CompletableFuture<Void> initializeNewRound(int roundDurationInMinute,
                                                      int minimumNumberOfWords,
                                                      IRoundFactory roundFactory,
                                                      IGameModeAnswerHandlingStrategy answerHandlerStrategy){
        final var NUMBER_OF_CHAR = 9;
        var future = letterRandomizer.getRandomLetter(minimumNumberOfWords, NUMBER_OF_CHAR);
        answerHandler = answerHandlerStrategy;
        
        return future.thenAccept(
                sortedChars -> initializeNewRound(sortedChars, roundDurationInMinute,
                                                  minimumNumberOfWords, roundFactory));
    }
    
    private void initializeNewRound(ISortedChars randomLetters, int roundDurationInMinute,
                                   int minimumNumberOfWords, IRoundFactory roundFactory) {
        currentRound = roundFactory.initiateRound(room, roundDurationInMinute, minimumNumberOfWords,
                                                  randomLetters);
        var requiredNumberOfVote = MathUtil.ceilDiv(room.getMemberSize(), 2);
        restartVote = new RestartVote(requiredNumberOfVote);
        
        var messageSender = messageSenderFactory.create();
        
        var startNewRoundData = new StartNewRoundDto();
        var wordLengthCount = getWordLengthToNumberOfWordsMapping();
        startNewRoundData.setTimeLimit(roundDurationInMinute * 60L);
        startNewRoundData.setRandomLetters(randomLetters.getSortedString());
        startNewRoundData.setWordLengthToNumberOfWordsMapping(wordLengthCount);
        
        messageSender.send(room.getMembers(), "start-new-round", startNewRoundData);
    }
    
    @Override
    public Map<Integer, Integer> getWordLengthToNumberOfWordsMapping(){
        var ret = new HashMap<Integer, Integer>();
        var currentRoundRandomLetters = currentRound.getRandomLetters();
        var words = wordRepository.getWordsFromGivenLetters(currentRoundRandomLetters);
    
        for (var word: words) {
            var value = ret.getOrDefault(word.length(), 0) + 1;
            ret.put(word.length(), value);
        }
        return ret;
    }
    
    
    @Override
    public void handleNewAnswer(IPlayer player, String answer){
        if (currentRound == null)
            throw new InvalidAnswer("Cannot handle new answer when no round is started");
        
        try{
            answerHandler.throwIfAnswerNotValid(wordRepository, currentRound, player, answer);
            answerIsValid(player, answer);
        }catch (InvalidAnswer e){
            var messageSender = messageSenderFactory.create();
            messageSender.send(player, "invalid-answer", e.getMessage());
        }
        
    }
    
    @Override
    public boolean handlePlayerVoteForRestartingRound(IPlayer player) {
        if (restartVote.isAffirmative(player))
            return false;
        restartVote.addAffirmativePlayer(player);
        return true;
    }
    

    @Override
    public void restartRound() {
        if (!restartVote.isRestartVoteEligible())
            throw new IllegalStateException("Required number of votes has not been satisfied yet");
        if (currentRound != null)
            currentRound.stopRound();
    }
    
    private void answerIsValid(IPlayer player, String answer){
        var playerWordPool = currentRound.getWordPool(player);
        playerWordPool.addWord(answer);
        scoreboard.increaseScore(player, answer.length());
    
        answerHandler.notifyPlayersWhenAnswerValid(
                currentRound, room, scoreboard, player, answer);
    }
    
    
    @Override
    public int getNumberOfRestartVotes() {
        return restartVote.getNumberOfAffirmativePlayer();
    }
    
    @Override
    public int getRequiredNumberOfVotes() {
        return restartVote.getRequiredNumberOfVotes();
    }
    
    @Override
    public boolean isRestartEligible() {
        return restartVote.isRestartVoteEligible();
    }
    
    @Override
    public String getRoomCode() {
        return room.getCode();
    }
    
    @Override
    public IRoom getRoom() {
        return room;
    }
    
    @Override
    public IScoreboard getScoreBoard() {
        return scoreboard;
    }
    
    @Override
    public @Nullable IRound getCurrentRound() {
        return currentRound;
    }
    
    @Override
    public IRestartVote getRestartVote() {
        return restartVote;
    }
    
    @Override
    public IWordRepository getWordRepository() {
        return wordRepository;
    }
}
