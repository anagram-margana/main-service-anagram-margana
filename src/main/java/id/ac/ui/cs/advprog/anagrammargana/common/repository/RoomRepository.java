package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomCodeAlreadyExistException;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Repository
public class RoomRepository implements IRoomRepository{
    Cache<String, IRoom> rooms = CacheBuilder
            .newBuilder()
            .expireAfterAccess(3, TimeUnit.HOURS)
            .build();
    
    @Override
    public boolean hasRoomCode(String roomCode) {
        return findByRoomCodeOrNull(roomCode).isPresent();
    }
    
    @Override
    public Optional<IRoom> findByRoomCodeOrNull(String roomCode) {
        var nullableRoom = rooms.getIfPresent(roomCode);
        return Optional.ofNullable(nullableRoom);
    }
    
    @Override
    public IRoom findByRoomCodeOrException(String roomCode){
        var nullableRoom = findByRoomCodeOrNull(roomCode);
        if (nullableRoom.isEmpty())
            throw new InvalidRoomCodeException("Invalid room code");
        return nullableRoom.get();
    }
    
    @Override
    public void add(IRoom room) {
        if (hasRoomCode(room.getCode())) {
            var errMsg = String.format("Room code %s has been taken already", room.getCode());
            throw new RoomCodeAlreadyExistException(errMsg);
        }
        rooms.put(room.getCode(), room);
    }
    
    @Override
    public void removeRoomByRoomCode(String roomCode) {
        if (!hasRoomCode(roomCode))
            throw new InvalidRoomCodeException("Not found IRoom for room code: " + roomCode);
        rooms.invalidate(roomCode);
    }
}
