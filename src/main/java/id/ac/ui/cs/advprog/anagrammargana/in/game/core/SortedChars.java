package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Objects;

public class SortedChars implements ISortedChars {
    String sortedString;

    public SortedChars(@NotNull String input) {
        sortedString = sortString(input);
    }

    public String sortString(String input) {
        char[] tempArray = input.toCharArray();
        Arrays.sort(tempArray);
        return new String(tempArray);
    }

    @Override
    public String getSortedString() {
        return sortedString;
    }

    @Override
    public int countChar(char c) {
        var counter = 0;
        var tempArray = sortedString.toCharArray();
        for (var i: tempArray) {
            if (i == c) {
                counter++;
            }
        }
        return counter;
    }

    @Override
    public boolean isSubsequenceOf(ISortedChars sortedChars) {
        var otherSortedString = sortedChars.getSortedString();
        var j = 0;
        for(var i = 0; i < otherSortedString.length(); i++){
            if (sortedString.charAt(j) == otherSortedString.charAt(i)) {
                ++j;
            }
            if (j == sortedString.length()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof String) {
            return sortedString.equals(object);
        } else if (object instanceof ISortedChars) {
            var otherSortedString = ((ISortedChars) object).getSortedString();
            return sortedString.equals(otherSortedString);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(sortedString);
    }
}
