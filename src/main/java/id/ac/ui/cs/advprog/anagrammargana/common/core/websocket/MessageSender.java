package id.ac.ui.cs.advprog.anagrammargana.common.core.websocket;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidMessageDataException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.util.CreateUrl;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class MessageSender implements IMessageSender{
    @Setter
    Logger logger = LoggerFactory.getLogger("MessageSender");
    
    private static URL getDefaultDestinationApiEndpoint(URL websocketServiceApiAddr){
        return CreateUrl.create(websocketServiceApiAddr, "/from-main-site");
    }
    
    WebClient webClient;
    
    
    public MessageSender(){
        this.webClient = WebClient.create();
    }
    
    @Override
    public <T> CompletableFuture<String> send(IPlayer player, String type, T message) {
        var playerId = player.getId();
        var sessionToken = player.getSessionToken();
        var dataToBeSent = new MessageData<>(playerId, null, sessionToken, type, message);
        
        if (dataToBeSent.getSessionToken() == null)
            throw new InvalidMessageDataException("Session token is required for this action");
        if (dataToBeSent.getType() == null)
            throw new InvalidMessageDataException("Type is required for this action");
    
        var playerWebsocketUrl = player.getWebsocketUrl();
        var destinationEndPoint = getDefaultDestinationApiEndpoint(playerWebsocketUrl);
        var ret = new CompletableFuture<String>();
    
        webClient.post()
                .uri(destinationEndPoint.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(dataToBeSent))
                .retrieve()
                .bodyToMono(String.class)
                .subscribe(response -> {
                    if (response == null || !response.equals("1"))
                        logger.error("Invalid websocket response `{}` for {}", response, dataToBeSent);
                    ret.complete(response);
                });
        return ret;
    }
    
    @Override
    public <T> List<CompletableFuture<String>> send(Iterable<IPlayer> players, String type, T message) {
        var ret = new ArrayList<CompletableFuture<String>>();
        
        for (var player: players) {
            ret.add(send(player, type, message));
        }
        return ret;
    }
    
    @Override
    public <T> List<CompletableFuture<String>> send(String type, Map<IPlayer, T> playerWithTheirData) {
        var ret = new ArrayList<CompletableFuture<String>>();
        
        for (var entry: playerWithTheirData.entrySet()) {
            var future = send(entry.getKey(), type, entry.getValue());
            ret.add(future);
        }
        
        return ret;
    }
}
