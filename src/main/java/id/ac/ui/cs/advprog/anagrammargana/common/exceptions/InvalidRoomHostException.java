package id.ac.ui.cs.advprog.anagrammargana.common.exceptions;

import lombok.Generated;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Generated
@ResponseStatus(code= HttpStatus.UNAUTHORIZED, reason="Invalid session token")
public class InvalidRoomHostException extends RuntimeException{
    public InvalidRoomHostException() {
    }
    
    public InvalidRoomHostException(String message) {
        super(message);
    }
}
