package id.ac.ui.cs.advprog.anagrammargana.in.game.model;

public enum GameMode {
    FREE_FOR_ALL, ALL_ALONE;
}
