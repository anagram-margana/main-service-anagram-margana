package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.LatestUpdateDto;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.RoundRefreshData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.StartNewRoundDto;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.VoteInformationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;

import static id.ac.ui.cs.advprog.anagrammargana.in.game.util.ScoreboardToListOfScoreDataUtil.getPlayerScoresAsList;


@Service
public class RoundRefreshService {
    @Autowired
    IPlayerRepository playerRepository;
    
    @Autowired
    IMediatorRepository mediatorRepository;
    
    public @Nullable RoundRefreshData getRoundRefreshData(String roomCode, long playerId){
        var player = playerRepository.findByIdOrException(playerId);
        var mediator = mediatorRepository.findByRoomCodeOrException(roomCode);
        if (mediator.getCurrentRound() == null)
            return null;
        
        var round = mediator.getCurrentRound();
        var endTime = round.getRoundEndTime();
        var now = new Date();
        if (isInThePast(now, endTime))
            return null;
        
        var givenRandomLetters = round.getRandomLetters();
        
        var numberOfMiliSecondsRemaining = endTime.getTime() - now.getTime();
        var numberOfSecondsRemaining = numberOfMiliSecondsRemaining / 1000;
        var wordLengthCount = mediator.getWordLengthToNumberOfWordsMapping();
        var givenRandomLettersAsString = givenRandomLetters.getSortedString();
        var roundInformation = new StartNewRoundDto(numberOfSecondsRemaining,
                                                    givenRandomLettersAsString, wordLengthCount);
        
        
        var wordpool = round.getWordPool(player);
        var scoreboard = mediator.getScoreBoard();
        var listOfScoreData = getPlayerScoresAsList(scoreboard);
        var scoreData = new LatestUpdateDto(listOfScoreData,
                                            -1,
                                            null,
                                            null,
                                            new ArrayList<>(wordpool.getWordsFound())
        );
        
        var restartVote = mediator.getRestartVote();
        var numberOfVote = restartVote.getNumberOfAffirmativePlayer();
        var requiredNumberOfVote = restartVote.getRequiredNumberOfVotes();
        var voteInformationDto = new VoteInformationDto(numberOfVote, requiredNumberOfVote);
        
        return new RoundRefreshData(scoreData, roundInformation, voteInformationDto);
    }
    
    
    private boolean isInThePast(Date present, Date dateToBeTested){
        return dateToBeTested.compareTo(present) <= 0;
    }
}
