package id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoteInformationDto {
    int numberOfVote;
    int requiredNumberOfVotes;
}
