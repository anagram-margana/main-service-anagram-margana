package id.ac.ui.cs.advprog.anagrammargana.common.util;

import org.apache.commons.lang3.tuple.Pair;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateTimeUtil {
    private DateTimeUtil(){}
    
    @SafeVarargs
    public static Date addDate(Date base, Pair<Long, TimeUnit>...args){
        var baseAsMillisecond = base.getTime();
        
        var milsec = TimeUnit.MILLISECONDS;
        for (var arg: args) {
            var duration = arg.getLeft();
            var durationTimeUnit = arg.getRight();
            var timeUnitInMillisecond = milsec.convert(duration, durationTimeUnit);
            baseAsMillisecond += timeUnitInMillisecond;
        }
        
        return new Date(baseAsMillisecond);
    }
    
    public static Date getDateFromNow(long duration, TimeUnit timeUnit) {
        var now = new Date();
        return addDate(now, Pair.of(duration, timeUnit));
    }
    
    
    public static long substractDate(Date date1, Date date2, TimeUnit preferredTimeUnit){
        var differenceInMilliSecond = date1.getTime() - date2.getTime();
        return preferredTimeUnit.convert(differenceInMilliSecond, TimeUnit.MILLISECONDS);
    }
    
    public static long getDatesDifference(Date date1, Date date2, TimeUnit preferredTimeUnit){
        var ret = substractDate(date1, date2, preferredTimeUnit);
        return Math.abs(ret);
    }
    
    public static Date asDate(LocalDateTime localDateTime){
        var defaultZoneId = ZoneId.systemDefault();
        var zonedDateTime = localDateTime.atZone(defaultZoneId);
        var asAnInstant = zonedDateTime.toInstant();
        return Date.from(asAnInstant);
    }
    
    public static Date asDate(int year, int month, int dayOfMonth, int hour, int minute, int second){
        var localDateTime = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
        return asDate(localDateTime);
    }
    
    public static LocalDateTime asLocalDateTime(Date date){
        var dateAsAnInstant = date.toInstant();
        var defaultZoneId = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(dateAsAnInstant, defaultZoneId);
    }
}
