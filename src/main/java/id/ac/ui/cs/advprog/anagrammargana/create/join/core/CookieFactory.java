package id.ac.ui.cs.advprog.anagrammargana.create.join.core;

import javax.servlet.http.Cookie;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class CookieFactory {
    Charset charset;
    
    
    public CookieFactory(){
        this(StandardCharsets.UTF_8);
    }
    
    public CookieFactory(Charset charset){
        this.charset = charset;
    }
    
    public Cookie createCookie(String name, String value){
        var newUrlEncoding = URLEncoder.encode(value, charset);
        var oldUrlEncoding = newUrlEncoding.replace("+", "%20");
        var cookie = new Cookie(name, oldUrlEncoding);
        cookie.setSecure(true);
        cookie.setHttpOnly(false);
        return cookie;
    }
    
}
