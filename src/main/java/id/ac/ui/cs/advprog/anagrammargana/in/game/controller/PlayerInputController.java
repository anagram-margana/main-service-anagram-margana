package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.GameSettingsData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.AnswerHandlingService;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.StartNewRoundService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/communication-api/msg-from-client")
public class PlayerInputController {
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @Autowired
    IRoomHostValidatorService roomHostValidatorService;
    
    
    @Autowired
    StartNewRoundService startNewRoundService;
    
    @Autowired
    AnswerHandlingService answerHandlingService;
    
    
    @PostMapping(value = "/submit-answer", produces = "application/json")
    public void getInput(@RequestBody MessageData<String> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        answerHandlingService.handleAnswer(messageDTO.getRoomCode(),
                                           messageDTO.getPlayerId(),
                                           messageDTO.getBody());
    }

    @PostMapping(value = "/submit-game-settings", produces = "application/json")
    public void submitGameSettings(@RequestBody MessageData<GameSettingsData> messageDTO) {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        roomHostValidatorService.throwIfNotRoomHost(messageDTO);
        startNewRoundService.startNewRound(messageDTO.getRoomCode(), messageDTO.getBody());
    }
}
