package id.ac.ui.cs.advprog.anagrammargana.common.exceptions;

import lombok.Generated;

@Generated
public class RoomCodeAlreadyExistException extends RuntimeException{
    public RoomCodeAlreadyExistException() {
    }
    
    public RoomCodeAlreadyExistException(String message) {
        super(message);
    }
}
