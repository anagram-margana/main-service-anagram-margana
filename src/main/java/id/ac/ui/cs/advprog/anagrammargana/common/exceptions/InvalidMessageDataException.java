package id.ac.ui.cs.advprog.anagrammargana.common.exceptions;

import lombok.Generated;

@Generated
public class InvalidMessageDataException extends RuntimeException{
    public InvalidMessageDataException() {
    }
    
    public InvalidMessageDataException(String message) {
        super(message);
    }
    
    public InvalidMessageDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
