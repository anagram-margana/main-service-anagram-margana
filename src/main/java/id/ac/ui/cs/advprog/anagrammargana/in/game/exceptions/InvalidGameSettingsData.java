package id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions;

import lombok.Generated;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Generated
@ResponseStatus(code= HttpStatus.BAD_REQUEST, reason="Bad game settings data")
public class InvalidGameSettingsData extends RuntimeException{
    public InvalidGameSettingsData() {
    }
    
    public InvalidGameSettingsData(String message) {
        super(message);
    }
    
    public InvalidGameSettingsData(String message, Throwable cause) {
        super(message, cause);
    }
}
