package id.ac.ui.cs.advprog.anagrammargana;

import lombok.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnagramMarganaApplication {
    @Generated
    public static void main(String[] args) {
        SpringApplication.run(AnagramMarganaApplication.class, args);
    }
}
