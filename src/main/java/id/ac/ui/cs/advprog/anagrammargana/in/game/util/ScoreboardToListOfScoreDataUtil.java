package id.ac.ui.cs.advprog.anagrammargana.in.game.util;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.IScoreboard;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.PlayerScoreData;

import java.util.ArrayList;
import java.util.List;

public class ScoreboardToListOfScoreDataUtil {
    private ScoreboardToListOfScoreDataUtil(){}
    
    public static List<PlayerScoreData> getPlayerScoresAsList(IScoreboard scoreboard){
        var playerScoreData = new ArrayList<PlayerScoreData>();
        for (var entry: scoreboard.asHashmap().entrySet()) {
            var scoreData = new PlayerScoreData();
            
            var player = entry.getKey();
            var playerId = player.getId();
            var playerName = player.getName();
            
            scoreData.setPlayerId(playerId);
            scoreData.setName(playerName);
            scoreData.setScore(entry.getValue());
            
            playerScoreData.add(scoreData);
        }
        return playerScoreData;
    }
}
