package id.ac.ui.cs.advprog.anagrammargana.lobby.service;

import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;

import java.util.List;

public interface ILobbyMessageService {
    MessageData<List<String>> refreshRoomMember(MessageData<Object> messageData);
    
    
    void startGame(String roomCode);
}
