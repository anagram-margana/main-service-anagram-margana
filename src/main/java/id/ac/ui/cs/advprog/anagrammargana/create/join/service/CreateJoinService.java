package id.ac.ui.cs.advprog.anagrammargana.create.join.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.create.join.core.CookieFactory;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.PlayerRandomUtil;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.RoomRandomUtil;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;

@Service
public class CreateJoinService implements ICreateJoinService {
    @Autowired
    PlayerRandomUtil playerRandomUtil;

    @Autowired
    RoomRandomUtil roomRandomUtil;
    
    @Autowired
    IMediatorRepository mediatorRepository;
    
    @Autowired
    IWordRepository wordRepository;
    

    @Autowired
    IPlayerRepository playerRepository;

    @Autowired
    IRoomRepository roomRepository;
    
    @Override
    public IRoom createHost(String name) {
        IPlayer playerAsRoomHost = createPlayer(name);
        
        var roomCode = roomRandomUtil.getRandomRoomCode();
        IRoom room = new Room(playerAsRoomHost, roomCode);
        IMediator mediator = new Mediator(room, wordRepository);
        
        roomRepository.add(room);
        mediatorRepository.add(mediator);
        return room;
    }
    
    @Override
    public IPlayer createGuest(String name, String roomCode){
        var playerAsGuest = createPlayer(name);
        var room = roomRepository.findByRoomCodeOrException(roomCode);
        
        room.addMember(playerAsGuest);
        notifyWholeRoomMembers(room);

        return playerAsGuest;
    }
    
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    private void notifyWholeRoomMembers(IRoom room){
        var listOfNames = room.getMemberNames();
        var messageSender = messageSenderFactory.create();
        messageSender.send(room.getMembers(), "update-room-member", listOfNames);
    }
    
    
    
    private IPlayer createPlayer(String name){
        long id = playerRandomUtil.getRandomPlayerId();
        String sessionToken = playerRandomUtil.getRandomPlayerSessionToken();

        IPlayer player = new Player(name, id, sessionToken);
        playerRepository.add(player);

        return player;
    }
    
    
    @Override
    public Cookie getPlayerNameCookie(String playerName) {
        var cookieFactory = new CookieFactory();
        return cookieFactory.createCookie("playerName", playerName);
    }
    
    @Override
    public Cookie getPlayerIdCookie(long playerId) {
        var cookieFactory = new CookieFactory();
        return cookieFactory.createCookie("playerId", String.valueOf(playerId));
    }
    
    @Override
    public Cookie getRoomCodeCookie(String roomCode) {
        var cookieFactory = new CookieFactory();
        return cookieFactory.createCookie("roomCode", String.valueOf(roomCode));
    }
    
    @Override
    public Cookie getSessionTokenCookie(String sessionToken) {
        var cookieFactory = new CookieFactory();
        return cookieFactory.createCookie("sessionToken", sessionToken);
    }
}
