package id.ac.ui.cs.advprog.anagrammargana.common.core;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.PlayerIdAlreadyExistException;

import java.util.Collection;

public interface IRoom {
    void close();
    String getCode();
    IPlayer getRoomHost();
    Collection<IPlayer> getMembers();
    int getMemberSize();
    Collection<String> getMemberNames();
    boolean hasMemberWithId(long playerId);
    
    /**
     * Menambahkan objek player ke dalam daftar anggota.
     * Apabila terdapat ID objek player tersebut sudah terdaftar, throw {@link PlayerIdAlreadyExistException}
     * @param player objek player yang ingin ditambahkan
     */
    void addMember(IPlayer player);
    
    /**
     * Menghapus objek player dari daftar anggota.
     * Apabila terdapat ID objek player tersebut belum terdaftar, throw {@link InvalidPlayerIdException}
     * @param playerId ID objek player yang ingin dihapus
     */
    void removeMemberWithId(long playerId);
}
