package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.LatestUpdateDto;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;

import static id.ac.ui.cs.advprog.anagrammargana.in.game.util.ScoreboardToListOfScoreDataUtil.getPlayerScoresAsList;

public class AllAloneAnswerHandlingStrategy extends DefaultAnswerHandlingStrategy{
    @Setter
    IMessageSenderFactory messageSenderFactory = new MessageSenderFactory();
    
    @Override
    public void notifyPlayersWhenAnswerValid(IRound round, IRoom room, IScoreboard scoreboard,
                                             IPlayer playerWhoSubmittedTheAnswer, String answer) {
        var playersToBeNotified = room.getMembers();
        var messageSender = this.messageSenderFactory.create();
        var playerScoreData = getPlayerScoresAsList(scoreboard);
        
        var dataToBeSent = new HashMap<IPlayer, LatestUpdateDto>();
        for (var player: playersToBeNotified) {
            var latestUpdate = new LatestUpdateDto();
            var wordRepository = round.getWordPool(player);
            var wordsFound = wordRepository.getWordsFound();
            
            latestUpdate.setWordsFound(new ArrayList<>(wordsFound));
            latestUpdate.setPlayerIdWhoSubmittedAnswer(playerWhoSubmittedTheAnswer.getId());
            latestUpdate.setPlayerNameWhoSubmittedAnswer(playerWhoSubmittedTheAnswer.getName());
            latestUpdate.setScoreData(playerScoreData);
            
            dataToBeSent.put(player, latestUpdate);
        }
        
        messageSender.send("aa-new-answer-submission", dataToBeSent);
    }
}
