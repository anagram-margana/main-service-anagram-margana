package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import java.util.Collection;

public interface IWordPool {
    boolean doesWordExist(String word);
    boolean addWord(String word);
    Collection<String> getWordsFound();

}
