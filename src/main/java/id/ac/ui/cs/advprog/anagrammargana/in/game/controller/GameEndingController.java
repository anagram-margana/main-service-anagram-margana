package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.GameEndingService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/communication-api/msg-from-client")
public class GameEndingController {
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @Autowired
    IRoomHostValidatorService roomHostValidatorService;
    
    @Autowired
    GameEndingService gameEndingService;
    
    
    
    @PostMapping(value = "/finish-the-game", produces = "application/json")
    public void hostDecideToFinishTheGame(@RequestBody MessageData<Object> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        roomHostValidatorService.throwIfNotRoomHost(messageDTO);
        gameEndingService.finishTheGame(messageDTO.getRoomCode());
    }
}
