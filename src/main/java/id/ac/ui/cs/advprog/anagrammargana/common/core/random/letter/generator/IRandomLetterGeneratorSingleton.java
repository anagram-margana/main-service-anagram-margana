package id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.ISortedChars;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;

import java.util.concurrent.CompletableFuture;

public interface IRandomLetterGeneratorSingleton {
    void setWordRepository(IWordRepository wordRepository);
    CompletableFuture<ISortedChars> getRandomLetter(int minimumNumberOfWords, int numberOfLetters);
}
