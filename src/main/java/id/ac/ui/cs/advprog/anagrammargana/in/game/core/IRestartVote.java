package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;

import java.util.Collection;

public interface IRestartVote {
    Collection<IPlayer> getAffirmativePlayer();
    int getNumberOfAffirmativePlayer();
    void addAffirmativePlayer(IPlayer player);
    boolean isAffirmative(IPlayer player);
    int getRequiredNumberOfVotes();
    boolean isRestartVoteEligible();
}
