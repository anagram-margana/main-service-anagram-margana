package id.ac.ui.cs.advprog.anagrammargana.lobby.controller;


import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.lobby.service.ILobbyMessageService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * Daftar event-type:
 *  Menerima:
 *      - refresh-room-member:
 *          dikirim oleh client ketika client ingin meminta data terbaru mengenai room member
 *  Mengirim:
 *      - update-room-member:
 *          dikirim oleh server setiap ada pengguna baru yang bergabung ke dalam ruangan,
 *          atau setiap kali user mengirimkan refresh-room-member.
 *
 *          Data yang dikirim berupa daftar nama pemain
 *
 * @see id.ac.ui.cs.advprog.anagrammargana.websocket.communication
 */
@RestController
@RequestMapping("/communication-api/msg-from-client")
public class LobbyMessageController {
    @Autowired
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @Autowired
    IRoomHostValidatorService roomHostValidatorService;
    
    @Autowired
    ILobbyMessageService lobbyMessageService;
    
    @PostMapping(value = "/refresh-room-member", produces = "application/json")
    public MessageData<List<String>> refreshRoomMember(@RequestBody MessageData<Object> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        return lobbyMessageService.refreshRoomMember(messageDTO);
    }
    
    @PostMapping(value = "/start-first-round", produces = "application/json")
    public MessageData<List<String>> startGame(@RequestBody MessageData<Object> messageDTO)
    {
        playerAuthenticationService.throwIfNotAuthenticated(messageDTO);
        roomHostValidatorService.throwIfNotRoomHost(messageDTO);
        lobbyMessageService.startGame(messageDTO.getRoomCode());
        return null;
    }
}
