package id.ac.ui.cs.advprog.anagrammargana.common.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.lang.Nullable;

/**
 * Baca DOCUMENTATION.md pada repository microservice websocket.
 * @param <T>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageData<T> {
    long playerId;
    
    @Nullable
    String roomCode;
    
    @Nullable
    String sessionToken;
    
    String type;
    
    T body;
    
    
    
    /**
     * Copy all data except body
     */
    public void copyDataFrom(MessageData<?> other){
        this.playerId = other.playerId;
        this.roomCode = other.roomCode;
        this.sessionToken = other.sessionToken;
        this.type = other.type;
    }
    
    @SneakyThrows
    public static <T> MessageData<T> fromJson(String json, Class<T> type){
        var objectMapper = new ObjectMapper();
        var typeReference = new TypeReference<MessageData<T>>(){};
        var jsonResult = objectMapper.readValue(json, typeReference);
    
        
        
        // memastikan bahwa body-nya memang benar-benar bertipe T.
        // ada kasus dimana ini bisa berbeda. Misal, kalau T berupa char dan json memberikan string 1 huruf.
        // Meskipun secara compile-time object yang dikembalikan bertipe MessageData<Character>(),
        // bisa saja sebenarnya yang dikembalikan adalah MessageData<String>(). Jadi perlu dicek apakah benar-benar
        // bertipe T atau bukan
        
        Object bodyObj = jsonResult.getBody();
        if (bodyObj != null) {
            var expectedBody = objectMapper.convertValue(bodyObj, type);
            jsonResult.setBody(expectedBody);
        }
    
        return jsonResult;
    }
}
