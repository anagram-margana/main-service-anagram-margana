package id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto;

import id.ac.ui.cs.advprog.anagrammargana.in.game.model.GameMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameSettingsData {
    GameMode gameMode;
    int timeLimit;
    int minWords;
}
