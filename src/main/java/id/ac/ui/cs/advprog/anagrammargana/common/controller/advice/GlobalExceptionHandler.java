package id.ac.ui.cs.advprog.anagrammargana.common.controller.advice;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingRequestCookieException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@ControllerAdvice
public class GlobalExceptionHandler {
    @Setter
    Logger logger = LogManager.getLogger("GlobalExceptionHandler");
    
    
    @ExceptionHandler(value = {InvalidRoomCodeException.class,
            InvalidPlayerIdException.class, InvalidSessionTokenException.class})
    public String invalidIdentityInformation(HttpServletResponse httpResponse) throws IOException {
        httpResponse.sendRedirect("/");
        return null;
    }
    
    
    
    @ExceptionHandler(value = {MissingRequestCookieException.class})
    public String someoneWithoutCookieTryingToAccessLobbyOrInGame(HttpServletResponse httpResponse)
            throws IOException {
        logger.info("someone is trying to access /lobby or /in-game without having the required cookies");
        httpResponse.sendRedirect("/");
        return null;
    }
    
    
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void logBadRequest(HttpMessageNotReadableException e) {
        logger.warn("Returning HTTP 400 Bad Request", e);
        throw e;
    }
}
