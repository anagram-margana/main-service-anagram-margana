package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.PlayerIdAlreadyExistException;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Repository
public class PlayerRepository implements IPlayerRepository{
    Cache<Long, IPlayer> players;
    
    
    public PlayerRepository(){
        players = CacheBuilder
                .newBuilder()
                .expireAfterAccess(3, TimeUnit.HOURS)
                .build();
    }
    
    @Override
    public boolean hasPlayerId(long id) {
        return findByIdOrNull(id).isPresent();
    }
    
    @Override
    public Optional<IPlayer> findByIdOrNull(long id) {
        var player = players.getIfPresent(id);
        return Optional.ofNullable(player);
    }
    
    @Override
    public IPlayer findByIdOrException(long id) {
        var ret = findByIdOrNull(id);
        if (ret.isPresent())
            return ret.get();
        throw new InvalidPlayerIdException("Not found IPlayer for ID: " + id);
    }
    
    
    @Override
    public void add(IPlayer player) {
        var playerIdToBeAdded = player.getId();
        if (hasPlayerId(playerIdToBeAdded)) {
            var playerIdAsString = Long.toString(playerIdToBeAdded);
            throw new PlayerIdAlreadyExistException(
                    String.format("Player ID %s has been taken already", playerIdAsString));
        }
        
        players.put(player.getId(), player);
    }
    
    @Override
    public IPlayer authenticateAndGetPlayer(long id, String sessionToken){
        var player = findByIdOrException(id);
        if (!player.getSessionToken().equals(sessionToken))
            throw new InvalidSessionTokenException("Invalid session token for player ID: " + id);
        
        return player;
    }
    
    @Override
    public void removePlayerWithId(long playerId) {
        if (!hasPlayerId(playerId))
            throw new InvalidPlayerIdException("Not found IPlayer for ID: " + playerId);
        players.invalidate(playerId);
    }
}
