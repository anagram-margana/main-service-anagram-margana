package id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LatestUpdateDto {
    List<PlayerScoreData> scoreData;
    long playerIdWhoSubmittedAnswer;
    String playerNameWhoSubmittedAnswer;
    
    @Nullable String recentlyDiscoveredWord;
    List<String> wordsFound;
}
