package id.ac.ui.cs.advprog.anagrammargana.common.exceptions;

import lombok.Generated;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Generated
@ResponseStatus(code= HttpStatus.BAD_REQUEST, reason="Player ID not found")
public class InvalidPlayerIdException extends RuntimeException {
    public InvalidPlayerIdException() {
    }
    
    public InvalidPlayerIdException(String message) {
        super(message);
    }
}
