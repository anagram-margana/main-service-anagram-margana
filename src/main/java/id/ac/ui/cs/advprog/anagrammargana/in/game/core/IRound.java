package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;

import java.util.Date;
import java.util.Map;

public interface IRound {
    void stopRound();
    Date getRoundEndTime();
    ISortedChars getRandomLetters();
    int getMinimumWord();
    IWordPool getWordPool(IPlayer player);
    Map<IPlayer, IWordPool> getWordPools();
    void setWordPool(IPlayer player, IWordPool wordPool);
}
