package id.ac.ui.cs.advprog.anagrammargana.in.game.core;


import id.ac.ui.cs.advprog.anagrammargana.in.game.model.GameMode;

public class AnswerHandlingFactory implements IAnswerHandlingFactory {
    @Override
    public IGameModeAnswerHandlingStrategy createAnswerHandlingStrategy(GameMode gameMode) {
        if (gameMode == GameMode.ALL_ALONE) {
            return new AllAloneAnswerHandlingStrategy();
        }
        return new FreeForAllAnswerHandlingStrategy();
    }
}
