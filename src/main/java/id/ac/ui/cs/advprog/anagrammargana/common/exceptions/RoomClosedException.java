package id.ac.ui.cs.advprog.anagrammargana.common.exceptions;

import lombok.Generated;

@Generated
public class RoomClosedException extends RuntimeException{
    public RoomClosedException() {
    }
    
    public RoomClosedException(String message) {
        super(message);
    }
    
    public RoomClosedException(String message, Throwable cause) {
        super(message, cause);
    }
}
