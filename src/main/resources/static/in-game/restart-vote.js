var restartBtnSelector = "#restart-btn";
var restartNumberOfVote = "#restart-number-of-vote";



$(document).ready(function () {
    $(restartBtnSelector).on('click', function () {
        if (IS_HOST && isVoteEligible()){
            communicationProtocol.send("restart-round", "");
        }else{
            communicationProtocol.send("vote-restart", "");
        }
    });
})



class ObserveSomeoneVoteForRestartingGame extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "vote-restart";
    }

    handle(messageBody, eventType) {
        var numberOfVote = messageBody['numberOfVote'];
        var requiredNumberOfVotes = messageBody['requiredNumberOfVotes'];
        setRestartVoteLabel(numberOfVote, requiredNumberOfVotes);
    }
}

class OurVoteHasBeenReceived extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "vote-accepted";
    }

    handle(messageBody, eventType) {
        if (!isVoteEligible())
            disableOrEnableVoteBtn(true);
    }
}

class HostVoteEligible extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "vote-restart-eligible";
    }

    handle(messageBody, eventType) {
        onHostRestartEligible();
    }
}

function onHostRestartEligible(){
    disableOrEnableVoteBtn(false);
    setVoteEligible(true);
}

class HostSendRestartSignal extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "round-restart";
    }

    handle(messageBody, eventType) {
        endRound(false);
    }
}


communicationProtocol.addObserver(new ObserveSomeoneVoteForRestartingGame());
communicationProtocol.addObserver(new OurVoteHasBeenReceived());
communicationProtocol.addObserver(new HostVoteEligible());
communicationProtocol.addObserver(new HostSendRestartSignal());



function setRestartVoteLabel(numberOfVotes, requiredNumberOfVotes){
    var numberOfVoteElement = $(restartNumberOfVote);
    if (numberOfVotes == null)
        numberOfVoteElement.html("");
    else
        numberOfVoteElement.html("(" + numberOfVotes + "/" + requiredNumberOfVotes + ")");
}

function disableOrEnableVoteBtn(disable){
    $(restartBtnSelector).prop('disabled', disable);
}



// only for host
const eligibleClass = "eligible";
function setVoteEligible(isEligible){
    var restartBtn = $(restartBtnSelector);
    if (isEligible)
        restartBtn.addClass(eligibleClass);
    else
        restartBtn.removeClass(eligibleClass);
}

// only for host
function isVoteEligible(){
    var restartBtn = $(restartBtnSelector);
    return restartBtn.hasClass(eligibleClass);
}
