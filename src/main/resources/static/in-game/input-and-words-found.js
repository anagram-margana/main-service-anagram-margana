var wordsFoundWrapperTemplateSelector = "#words-found-wrapper-template";
var wordsFoundBodySelector = "#words-found-body";


var listKata = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S" ,"T" , "U", "V", "W", "X",
    "Y", "Z"]
function shuffleHuruf(){
    var x = getAllElements("b1 b2 b3 b4 b5 b6 b7 b8 b9")
    var y = shuffle(x)
    for(var i = 0 ; i < x.length ; i++){
        x[i].innerHTML = y[i]
        x[i].value = y[i]
    }
}

function setCurrentRoundRandomLetters(arrayOfLetters){
    var x = getAllElements("b1 b2 b3 b4 b5 b6 b7 b8 b9")
    var y = shuffleGetKata(arrayOfLetters)
    for(var i = 0 ; i < x.length ; i++){
        x[i].innerHTML = y[i]
        x[i].value = y[i]
    }
}
function shuffle(arrayOfInputButtons) {
    var newArray = []
    for(var i = 0 ; i < arrayOfInputButtons.length ;i++){
        newArray[i] = arrayOfInputButtons[i].value
    }
    let currentIndex = arrayOfInputButtons.length,  randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex !== 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [newArray[currentIndex], newArray[randomIndex]] = [
            newArray[randomIndex], newArray[currentIndex]];
    }

    return newArray;
}
function shuffleGetKata(array) {
    var newArray = []
    for(var i = 0 ; i < array.length ;i++){
        newArray[i] = array[i]
    }
    let currentIndex = array.length,  randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex !== 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [newArray[currentIndex], newArray[randomIndex]] = [
            newArray[randomIndex], newArray[currentIndex]];
    }

    return newArray;
}
function randomLetterButtonClickEventHandler(value){
    var x = getAllElements("b1 b2 b3 b4 b5 b6 b7 b8 b9")
    var result;
    for (var i = 0 ; i < x.length ; i++){
        if (x[i] === value){
            result = x[i].value;
        }
    }

    var inputBtnElement = document.getElementById("answer-input-box");
    inputBtnElement.value = inputBtnElement.value + result;
}
function getAllElements(ids) {
    var idList = ids.split(" ");
    var results = [], item;
    for (var i = 0; i < idList.length; i++) {
        item = document.getElementById(idList[i]);

        if (item) {
            results.push(item);
        }
    }
    return(results);
}
function submitAnswer(){
    var inputElement = document.getElementById("answer-input-box")
    var answer = inputElement.value;
    communicationProtocol?.send("submit-answer", answer);
}

function updateWordsFound(wordsFound){
    var wordsFoundTemplate = $(wordsFoundWrapperTemplateSelector);
    var wordsFoundBody = $(wordsFoundBodySelector);

    if (wordsFound == null || !wordsFound.length){
        console.log("AAA aaa")
        var element = $('<div></div>');
        element.addClass("text-white");
        element.addClass("pl-2");
        element.html("None yet :(");

        wordsFoundBody.empty();
        wordsFoundBody.append(element);
        return;
    }

    wordsFoundBody.empty();
    for (let i = 0; i < wordsFound.length; i++) {
        var word = wordsFound[i];
        var wordWrapper = $(wordsFoundTemplate.html());
        wordWrapper.html(word);
        wordsFoundBody.append(wordWrapper);
    }
}
