const WORDS_TO_FIND_ELEMENTS_SELECTOR = {
    4: "#wtf-4-letters",
    5: "#wtf-5-letters",
    6: "#wtf-6-letters",
    7: "#wtf-7-letters",
    8: "#wtf-8-letters",
    9: "#wtf-9-letters"
};



/**
 *
 * @param duration0 the duration of the timer (in second)
 * @param onTimerExpired a function to be fired when the timer is finished
 * @return {cancelTimer} the function that's used to cancel currently running timer
 */
var cancelCurrentlyRunningTimer = () => {};
function setTimer(duration, onTimerExpired=null){
    if (onTimerExpired == null)
        onTimerExpired = () => {};

    var durationMin = Math.floor(duration/60);
    var durationSec = Math.floor(duration%60);
    cancelCurrentlyRunningTimer();


    function createTimer() {
        document.getElementById("time").innerHTML = durationMin + "m " + durationSec + "s";
        var timerInterval = setInterval(function () {
            duration = duration-1;
            var minutes = Math.floor(duration/60);
            var seconds = Math.floor(duration%60);
            document.getElementById("time").innerHTML = minutes + "m " + seconds + "s";
            if (duration < 0) {
                clearInterval(timerInterval);
                document.getElementById("time").innerHTML = "-";
            }
        }, 1000);

        return timerInterval;
    }


    /**
      *  Creates a progressbar.
      *  @param id the id of the div we want to transform in a progressbar
      *  @param duration the duration of the timer example: '10s'
      *  @param callback, optional function which is called when the progressbar reaches 0.
      *  @param progressBarId ID for the newly created progress bar inner
      */
    function createProgressbar(id, duration, callback, progressBarId) {
        // We select the div that we want to turn into a progressbar
        var progressbar = document.getElementById(id);
        progressbar.className = 'progressbar';

        // We create the div that changes width to show progress
        var progressbarinner = document.createElement('div');
        progressbarinner.setAttribute("id", progressBarId);
        progressbarinner.className = 'inner';

        // Now we set the animation parameters
        progressbarinner.style.animationDuration = duration;

        // Eventually couple a callback
        if (typeof(callback) === 'function') {
            progressbarinner.addEventListener('animationend', callback);
        }

        // Append the progressbar to the main progressbardiv
        progressbar.appendChild(progressbarinner);

        // When everything is set up we start the animation
        progressbarinner.style.animationPlayState = 'running';
    }


    const newlyCreatedProgressbarPrefixId = "time-left-progress-bar";
    var timerInterval = createTimer();

    function cancelTimer(){
        clearInterval(timerInterval);
        $("#" + newlyCreatedProgressbarPrefixId + timerInterval).remove();
    }


    createProgressbar('progressbar', (duration + 's'), onTimerExpired,
        newlyCreatedProgressbarPrefixId + timerInterval);

    cancelCurrentlyRunningTimer = cancelTimer;
    return cancelTimer;
}




//Fungsi buat fitur Words To Find

/**
 * Mengupdate komponen words to find pada html
 * @param wordsToFindDictionary dictionary<Int, Int>. Menyatakan banyaknya kata yang perlu dicari oleh user,
 *                                 dikelompokkan berdasarkan panjang kata tersebut (sebagai key)
 */
function updateWordsToFind(wordsToFindDictionary){
    for (let j = 4; j <= 9; j++) {
        var value = 0;
        if (j in wordsToFindDictionary)
            value = wordsToFindDictionary[j];

        $(WORDS_TO_FIND_ELEMENTS_SELECTOR[j]).html(value);
    }
}
