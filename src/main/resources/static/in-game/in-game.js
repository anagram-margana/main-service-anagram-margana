var rankStatusBodySelector = "#rank-status-body";
var scoreStatusBodySelector = "#score-status-body";
var answerInputBoxSelector = "#answer-input-box";
var fimishGameBtnSelector = "#finish-game-btn";

$(document).ready(function () {
    $(fimishGameBtnSelector).on('click', function () {
        communicationProtocol.send("finish-the-game", "");
    });

    communicationProtocol.addOnConnectEventHandler(function () {
        communicationProtocol.send("refresh-round-information", "");
    });
});




class FreeForAllNewAnswerSubmission extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "ffa-new-answer-submission";
    }
    handle(messageBody, eventType) {
        var playerNameWhoSubmittedAnswer = messageBody['playerNameWhoSubmittedAnswer'];
        var recentlyDiscoveredWords = messageBody['recentlyDiscoveredWord'];
        var notificationMessage = playerNameWhoSubmittedAnswer + " has found a word: " + recentlyDiscoveredWords;
        newAnserSubmission(messageBody, notificationMessage);
    }
}


class AllAloneNewAnswerSubmission extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "aa-new-answer-submission";
    }
    handle(messageBody, eventType) {
        var playerNameWhoSubmittedAnswer = messageBody['playerNameWhoSubmittedAnswer'];
        var notificationMessage = playerNameWhoSubmittedAnswer + " has found a word";
        newAnserSubmission(messageBody, notificationMessage);
    }
}

class InvalidAnswerSubmission extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "invalid-answer";
    }
    handle(message, eventType) {
        $(answerInputBoxSelector).val("");
        showNotification(message, showNotification.ERROR);
    }
}

communicationProtocol.addObserver(new FreeForAllNewAnswerSubmission());
communicationProtocol.addObserver(new AllAloneNewAnswerSubmission());
communicationProtocol.addObserver(new InvalidAnswerSubmission());



class SomeoneDisconnected extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "player-disconnected";
    }
    handle(messageBody, eventType) {
        var nama = messageBody["name"]
        var isRoomHost = messageBody["isRoomHost"]
        if (isRoomHost){
            newHostNotification("host disconnected: " + nama)
        }else{
            newHostNotification(nama + " disconnected")
        }
    }
}


class SomeoneConnected extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "player-connected";
    }
    handle(messageBody, eventType) {
        var nama = messageBody["name"]
        var isRoomHost = messageBody["isRoomHost"]
        if (isRoomHost){
            newHostNotification("host connected: " + nama)
        }else{
            newHostNotification(nama + " connected")
        }
    }
}


// ketika user merefresh saat ditengah-tengah round, server akan mengirimkan
// data ke observer ini
class RoundRefreshInformation extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "refresh-round-information";
    }
    handle(messageBody, eventType) {
        if (messageBody == null)
            return;  // no active round

        var voteInformation = messageBody['voteInformation'];
        var latestScoreUpdate = messageBody['latestScoreUpdate'];
        var roundInformation = messageBody['roundInformation'];

        updateRoundInformation(roundInformation);

        var scoreData = latestScoreUpdate['scoreData'];
        var wordsFound = latestScoreUpdate['wordsFound'];
        updateWordsFound(wordsFound);
        updateScoreboardFromListOfDict(scoreData);
        updateWordToFindFromWordsFound(wordsFound);

        var numberOfVote = voteInformation['numberOfVote'];
        var requiredNumberOfVotes = voteInformation['requiredNumberOfVotes'];
        if (numberOfVote > 0)
            setRestartVoteLabel(numberOfVote, requiredNumberOfVotes);
        if (numberOfVote >= requiredNumberOfVotes && IS_HOST)
            onHostRestartEligible();
    }
}
communicationProtocol.addObserver(new SomeoneDisconnected());
communicationProtocol.addObserver(new SomeoneConnected());
communicationProtocol.addObserver(new RoundRefreshInformation());




function newAnserSubmission(messageBody, notificationMessage){
    var scoreData = messageBody['scoreData'];
    var wordsFound = messageBody['wordsFound'];
    var playerIdWhoSubmittedAnswer = messageBody['playerIdWhoSubmittedAnswer'];


    updateWordsFound(wordsFound);
    updateScoreboardFromListOfDict(scoreData);
    updateWordToFindFromWordsFound(wordsFound);

    if (String(playerIdWhoSubmittedAnswer) === String(playerId)) {
        showNotification("You've found a valid answer", showNotification.SUCCESS);
        $(answerInputBoxSelector).val("");
    } else
        showNotification(notificationMessage, showNotification.INFO);


    if (IS_HOST)
        newHostNotification(notificationMessage);
}



function updateWordToFindFromWordsFound(wordsFOund){
    var foundWordLengthCount = listOfWordsToWordLengthCount(wordsFOund);
    var leftWordsToFind = {};

    var temp = wordLengthToNumberOfWordsMapping;
    for (const wordLength in temp) {
        var numberOfWord = temp[wordLength];

        if (wordLength in foundWordLengthCount){
            leftWordsToFind[wordLength] = numberOfWord - foundWordLengthCount[wordLength];
        }else
            leftWordsToFind[wordLength] = numberOfWord;
    }
    updateWordsToFind(leftWordsToFind);
}

function listOfWordsToWordLengthCount(listOfWords){
    var ret = {};
    for (let i = 0; i < listOfWords.length; i++) {
        var word = listOfWords[i];

        var len = word.length;
        if (!(len in ret))
            ret[len] = 0;
        ret[len] += 1;
    }
    return ret;
}



function updateScoreboardFromListOfDict(listOfDict){
    console.log(listOfDict);
    var asTwoDimensionalArr = convertListOfDictTo2dArray(listOfDict);
    console.log(asTwoDimensionalArr);
    updateScoreboard(asTwoDimensionalArr);
}


function ordinal_suffix_of(i) {
    var j = i % 10;
    var k = i % 100;
    if (j === 1 && k !== 11) {
        return i + "st";
    }
    if (j === 2 && k !== 12) {
        return i + "nd";
    }
    if (j === 3 && k !== 13) {
        return i + "rd";
    }
    return i + "th";
}



function convertListOfDictTo2dArray(listOfDict){

    var result = [];
    for (let i = 0; i < listOfDict.length; i++) {
        var dict = listOfDict[i];
        var temp = [];

        temp.push(dict['playerId']);
        temp.push(dict['name']);
        temp.push(dict['score']);

        result.push(temp);
    }
    return result;
}


