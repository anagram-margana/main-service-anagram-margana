var SCOREBOARDS_SELECTOR = [
    "#mini-scoreboard",
    "#large-scoreboard",
];


/**
 * Mengupdate mini-scoreboard maupun large-scoreboard berdasarkan listOfScoreData
 * @param listOfScoreData {Array} list of list di mana
 *                  item 0 dari inner-list merupakan Int id pemain
 *                  item 1 dari inner-list merupakan string nama pemain,
 *                  item 2 dari inner-list merupakan Int score pemain tersebut
 */
function updateScoreboard(listOfScoreData){
    // compare by its score, descending
    listOfScoreData.sort((a, b) => {
        if (b[2] !== a[2])
            return b[2] - a[2];
        return b[1] - a[1];
    });


    for (let scoreboardIndex = 0; scoreboardIndex < SCOREBOARDS_SELECTOR.length; scoreboardIndex++) {
        var scoreboardElement = $(SCOREBOARDS_SELECTOR[scoreboardIndex]);
        var scoreboardBody = scoreboardElement.find(".scoreboard-body");
        var rowTemplateElement = scoreboardElement.find(".scoreboard-row-tempalate");
        var playerIsFound = false;

        scoreboardBody.empty();

        for (let i = 0; i < listOfScoreData.length; i++) {
            var newRow = $(rowTemplateElement.html());

            var rankNumberElement = newRow.find(".rank-number");
            var playerNameElement = newRow.find(".player-name");
            var playerScoreElement = newRow.find(".player-score");

            var currRank = i+1;
            var currPlayerId = listOfScoreData[i][0];
            var currPlayerName = listOfScoreData[i][1];
            var currPlayerScore = listOfScoreData[i][2];

            rankNumberElement.html(currRank);
            playerNameElement.html(currPlayerName);
            playerScoreElement.html(currPlayerScore);

            if (String(currPlayerId) == String(playerId)){
                playerIsFound = true;
                updateStatusBox(currRank, currPlayerScore)
            }

            scoreboardBody.append(newRow);
        }

        if (!playerIsFound){
            currRank += 1;
            updateStatusBox(currRank, 0);
        }
    }
}

function updateStatusBox(rank, score){
    var rankText = ordinal_suffix_of(rank);
    $(rankStatusBodySelector).html(rankText);
    $(scoreStatusBodySelector).html(score);
}


