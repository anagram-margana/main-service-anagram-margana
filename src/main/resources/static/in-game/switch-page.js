const SHOW_ON_FIRST_ROUND_ONLY_SELECTOR = ".show-on-first-round-only";
const HIDE_ON_FIRST_ROUND_ONLY_SELECTOR = ".hide-on-first-round-only";
const ALL_PAGE_SELECTOR = ".page";
const IN_GAME_PAGE_SELECTOR = ".page-in-game";
const BIG_LEADERBOARD_PAGE_SELECTOR = ".page-big-leaderboard";
const GAME_SETTINGS_PAGE_SELECTOR = ".page-game-settings";

const HIDE_ON_GAME_ENDED = ".hide-on-game-ended";
const SHOW_ON_GAME_ENDED = ".show-on-game-ended-only";


function finishTheGame(){
    $(HIDE_ON_GAME_ENDED).hide();
    $(SHOW_ON_GAME_ENDED).show();
    showLeaderboardPage();
}


function endRound(isFirstRound){
    if (isFirstRound){
        $(SHOW_ON_FIRST_ROUND_ONLY_SELECTOR).show();
        $(HIDE_ON_FIRST_ROUND_ONLY_SELECTOR).hide();
    }else{
        $(SHOW_ON_FIRST_ROUND_ONLY_SELECTOR).hide();
        $(HIDE_ON_FIRST_ROUND_ONLY_SELECTOR).show();
    }

    if (IS_HOST){
        showGameSettingsPage();
    }else{
        showLeaderboardPage();
    }
}


function startRound(){
    disableOrEnableVoteBtn(false);
    setVoteEligible(false);
    setRestartVoteLabel(null);
    updateWordsFound([]);
    $(answerInputBoxSelector).val("");
    showNotification("-", showNotification.INFO);
    showInGamePage();
}


function hideAllPage(){
    $(ALL_PAGE_SELECTOR).addClass("hidden");
}

function showInGamePage(){
    hideAllPage();
    setUrlHash("in-game")
    $(IN_GAME_PAGE_SELECTOR).removeClass("hidden");
}

function showLeaderboardPage(){
    hideAllPage();
    setUrlHash("leaderboard")
    $(BIG_LEADERBOARD_PAGE_SELECTOR).removeClass("hidden");
}

function showGameSettingsPage(){
    hideAllPage();
    setUrlHash("game-settings")
    $(GAME_SETTINGS_PAGE_SELECTOR).removeClass("hidden");
}

function submitGameSettings() {
    console.log("Submitted");
    var gameSettingData = $('form#game-settings-form').serializeArray()
    var gameMode = gameSettingData[0].value
    var timeLimit = parseInt(gameSettingData[1].value)
    var minWords = parseInt(gameSettingData[2].value)

    var requestBody = {
        'gameMode': gameMode,
        'timeLimit': timeLimit,
        'minWords': minWords
    }
    console.log(requestBody)

    communicationProtocol?.send("submit-game-settings", requestBody)
}


function setUrlHash(urlHash){
    if(history.pushState) {
        history.replaceState(null, null, '#' + urlHash);
    }
    else {
        location.hash = '#' + urlHash;
    }
}

$(document).ready(function () {
    endRound(true);
})


/**
 * key: panjang karakter, value: jumlah kata yang bisa dibentuk <br> <br>
 *
 * Misal kita bisa membentuk 9 kata, yakni: <br>
 *      4 kata panjangnya 2 karakter <br>
 *      5 kata panjangnya 3 karakter <br>
 *
 * Maka: <br>
 *      wordLengthToNumberOfWordsMapping = { <br>
 *          4: 2, <br>
 *          5: 3 <br>
 *      }
 * @type {Object.<number, number>}
 */
var wordLengthToNumberOfWordsMapping = {};

class StartNewRoundObserver extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "start-new-round";
    }
    handle(roundInformation, eventType) {
        updateRoundInformation(roundInformation);
    }
}

function updateRoundInformation(roundInformation){
    var timeLimitInSeconds = roundInformation["timeLimit"];
    var randomLetters = roundInformation["randomLetters"];
    wordLengthToNumberOfWordsMapping = roundInformation["wordLengthToNumberOfWordsMapping"];

    console.log(roundInformation);

    setTimer(timeLimitInSeconds, () => {
        endRound();
    });

    startRound();
    setVoteEligible(false);
    setRestartVoteLabel(null);
    setCurrentRoundRandomLetters(randomLetters);
    updateWordsToFind(wordLengthToNumberOfWordsMapping);
}



class GameEndedObserver extends AbstractStompWebsocketEventObserver{
    isAllowedToHandle(eventType) {
        return eventType === "game-ended";
    }
    handle(messageBody, eventType) {
        finishTheGame();
    }
}


communicationProtocol.addObserver(new StartNewRoundObserver());
communicationProtocol.addObserver(new GameEndedObserver());