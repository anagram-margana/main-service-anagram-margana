var primaryNotificationSelector = "#primary-notification";
var primaryNotificationHeaderSelector = "#primary-notification-header";
var primaryNotificationBodySelector = "#primary-notification-body";
var hostNotificationTemplateSelector = "#host-notif-template";
var hostNotificationBodyContainerSelector = "#host-notif-body-container";


/* ------------- fungsionalitas ------------- */
function showNotification(text, type){
    var notifHeader = $(primaryNotificationHeaderSelector);
    var notifBody = $(primaryNotificationBodySelector);
    notifBody.html(escapeHtml(text));

    var notificationTypes = {};
    notificationTypes[showNotification.NONE] = '';
    notificationTypes[showNotification.INFO] = 'info-notification';
    notificationTypes[showNotification.ERROR] = 'error-notification';
    notificationTypes[showNotification.SUCCESS] = 'success-notification';

    for (const key in notificationTypes) {
        var value = notificationTypes[key];
        notifHeader.removeClass(value);
        notifBody.removeClass(value);
    }
    notifHeader.addClass(notificationTypes[type]);
    notifBody.addClass(notificationTypes[type]);
    reset_animation(notifHeader[0])
    reset_animation(notifBody[0])
}

showNotification.NONE = 0
showNotification.INFO = 1
showNotification.ERROR = 2
showNotification.SUCCESS = 3


function newHostNotification(notificationText){
    newHostNotification.numberOfNotifications += 1;

    var notificationBodyTemplate = $(hostNotificationTemplateSelector);
    var hostNotificationBodyContainer = $(hostNotificationBodyContainerSelector);

    var newNotifBody = $(notificationBodyTemplate.html());
    newNotifBody.find(".notif-number").html(newHostNotification.numberOfNotifications);
    newNotifBody.find(".notif-body").html(escapeHtml(notificationText));
    hostNotificationBodyContainer.append(newNotifBody);
}
newHostNotification.numberOfNotifications = 0;



function reset_animation(domElement) {
    domElement.style.animation = 'none';
    domElement.offsetHeight; /* trigger reflow */
    domElement.style.animation = null;
}


function escapeHtml(unsafe)
{
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}



/* ------------- observer ------------- */
class InfoNotificationObserver extends AbstractStompWebsocketEventObserver{

    isAllowedToHandle(eventType) {
        return eventType === "info-notification"
    }
    handle(notificationBody, eventType) {
        showNotification(notificationBody, showNotification.INFO);
    }
}

class ErrorNotificationObserver extends AbstractStompWebsocketEventObserver{

    isAllowedToHandle(eventType) {
        return eventType === "error-notification"
    }
    handle(notificationBody, eventType) {
        showNotification(notificationBody, showNotification.ERROR);
    }
}

class SuccessNotificationObserver extends AbstractStompWebsocketEventObserver{

    isAllowedToHandle(eventType) {
        return eventType === "success-notification"
    }
    handle(notificationBody, eventType) {
        showNotification(notificationBody, showNotification.SUCCESS);
    }
}

communicationProtocol.addObserver(new InfoNotificationObserver());
communicationProtocol.addObserver(new ErrorNotificationObserver());
communicationProtocol.addObserver(new SuccessNotificationObserver());





