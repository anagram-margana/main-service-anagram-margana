// depends on websocket-config.js


const mulaiPermainanBtnSelector = "#mulai-permainan-btn";
const mulaiPermainanTextSelector = "#mulai-permainan-text";
const mulaiPermainanLoadingTextSelector = "#mulai-permainan-loading-text";

const undangTemanBtnSelector = "#undang-teman-id";
const undangTemanTextSelector = "#undang-teman-text";
const undangTemanCopiedTextSelector = "#undang-teman-copied-text";
const undangTemanFailedTextSelector = "#undang-teman-failed-text";

const playerNameBoxTemplateSelector = "#player-name-box-template";
const roomMemberListSelector = "#room-member-list";
const roomCodeLabelSelector = "#room-code-label";




// add event listener untuk invite teman
{
    $(document).ready(function (){
        var delay = 2500;
        var durationStart = 200;
        var durationEnd = 100;

        function clipboardCopiedSuccessfully(){
            $(undangTemanTextSelector)
                .fadeTo(durationStart, 0)
                .delay(delay)
                .fadeTo(durationEnd, 1);
            $(undangTemanCopiedTextSelector)
                .fadeTo(durationStart, 1)
                .delay(delay)
                .fadeOut(durationEnd, 0);
        }

        function clipboardCopyFailed(){
            $(undangTemanTextSelector)
                .fadeTo(durationStart, 0)
                .delay(delay)
                .fadeTo(durationEnd, 1);
            $(undangTemanFailedTextSelector)
                .fadeTo(durationStart, 1)
                .delay(delay)
                .fadeOut(durationEnd, 0);
        }


        var undangTemanBtn = $(undangTemanBtnSelector);
        undangTemanBtn.on('click', function () {
            $(undangTemanBtnSelector).attr("disabled", true);
            setTimeout(function (){
                $(undangTemanBtnSelector).attr("disabled", false);
            }, delay);

            var invitationEndpointUrl = new URL("/join?room=" + encodeURI(roomCode), MAIN_SERVER_ADDR);

            navigator.clipboard.writeText(invitationEndpointUrl.href).then(clipboardCopiedSuccessfully, clipboardCopyFailed);
        });
    });
}



// add event listener untuk mulai permainan host
{
    $(document).ready(function (){
        var mulaiPermainanButton = $(mulaiPermainanBtnSelector);
        mulaiPermainanButton.on("click", function (){
            communicationProtocol?.send("start-first-round", "");

            $(mulaiPermainanTextSelector).fadeTo(150, 0);
            $(mulaiPermainanLoadingTextSelector).fadeTo(150, 1);
        });
    });
}



// kode untuk request update room secara periodik
{
    function scheduledRequestRoomUpdate() {
        console.log("updating");
        if (communicationProtocol?.isConnected() ?? false)
            communicationProtocol?.send("refresh-room-member", "");

        clearTimeout(scheduledRequestRoomUpdate.timeoutId);
        scheduledRequestRoomUpdate.timeoutId = setTimeout(scheduledRequestRoomUpdate, 8000);
    }

    $(document).ready(function () {
        $(roomCodeLabelSelector).html(roomCode);
        setTimeout(scheduledRequestRoomUpdate, 100);

        communicationProtocol?.addOnConnectEventHandler(() => {
            if (communicationProtocol?.isConnected() ?? false)
                communicationProtocol?.send("refresh-room-member", "");
        });
    });

    communicationProtocol.addOnConnectEventHandler(() => scheduledRequestRoomUpdate());
}




/**
 * Mengupdate tampilan daftar pemain berdasarkan arrayOfNames yang diberikan
 */
function updatePlayerList(arrayOfNames){
    clearPlayerList();
    for (let i = 0; i < arrayOfNames.length; i++) {
        var name = arrayOfNames[i];
        addNewPlayer(name);
    }
}

function addNewPlayer(name){
    var playerNameBoxTemplate = $(playerNameBoxTemplateSelector);
    var roomMemberListDom = $(roomMemberListSelector);

    // convert template element to concrete element
    var newConcreteElement = $(playerNameBoxTemplate.html());
    var nameContainerElement = newConcreteElement.find(".name-container");
    nameContainerElement.html(name);

    roomMemberListDom.append(newConcreteElement);
}

/**
 * Menghapus semua elemen di `roomMemberListSelector` kecuali elemen <template>
 */
function clearPlayerList(){
    var roomMemberListDom = $(roomMemberListSelector);
    var templateChildren = roomMemberListDom.find("template");

    templateChildren.detach();
    roomMemberListDom.empty();
    roomMemberListDom.append(templateChildren);
}


// when new member join
{
    class NewMemberObserver extends AbstractStompWebsocketEventObserver {
        isAllowedToHandle(eventType) {
            return eventType === "update-room-member";
        }

        handle(listOfPlayerNames, eventType) {
            updatePlayerList(listOfPlayerNames);
        }
    }

    var observer = new NewMemberObserver();
    observer.observe(communicationProtocol);
}


// when the host starts the game
{
    class StartFirstRoundObserver extends AbstractStompWebsocketEventObserver {
        isAllowedToHandle(eventType) {
            return eventType === "start-first-round";
        }

        handle(listOfPlayerNames, eventType) {
            window.location.href = "/in-game";
        }
    }

    var observer = new StartFirstRoundObserver();
    observer.observe(communicationProtocol);
}