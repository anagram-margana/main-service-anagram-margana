const COLLAPSED_CLASS = "collapsed";
const ON_CREATE_HOST_PAGE_CLASS = "on-create-host-page";
const CLICKED_CLASS = "clicked";
const CREATE_HOST_URL_HASH = "#create-host";

const LOGO_IMG_SELECTOR = "#logo-img";
const CREATE_JOIN_SELECTOR = "#create-join-container";
const LOGO_CONTAINER_SELECTOR = "#logo-container";

const PLAYER_INFO_INP_LABEL_SELECTOR = ".player-information-input-label";
const PLAYER_INFO_INPUT_SELECTOR = ".player-information-input";
const CREATE_HOST_BTN_SELECTOR = "#create-room-as-host-btn";
const JOIN_AS_GUEST_BTN_SELECTOR = "#join-as-guest-btn";






$(document).ready(function () {
    var clickableButtons = $(".clickable-effect");
    clickableButtons.on('mousedown', function (e) {
        $(this).addClass(CLICKED_CLASS);
    });
    clickableButtons.on('mouseup mouseleave', function (e) {
        $(this).removeClass(CLICKED_CLASS);
    });


    var createJoinContainer = $(CREATE_JOIN_SELECTOR);
    var logoImg = $(LOGO_IMG_SELECTOR);
    var logoContainer = $(LOGO_CONTAINER_SELECTOR);
    createJoinContainer.on('mouseenter', function () {
        logoImg.addClass("artificial-hover-effect");
    });
    createJoinContainer.on('mouseleave', function () {
        logoImg.removeClass("artificial-hover-effect");
    });


    var playerNameInput = $(PLAYER_INFO_INPUT_SELECTOR);
    var createHostBtn = $(CREATE_HOST_BTN_SELECTOR);
    var joinAsGuestBtn = $(JOIN_AS_GUEST_BTN_SELECTOR);
    function goToPlayerNameInputPage(animationTime = 400){
        if (animationTime === 0) {
            $("*").addClass("no-transition");
        }else {
            $("*").removeClass("no-transition");
        }


        $(LOGO_IMG_SELECTOR).addClass(COLLAPSED_CLASS);
        $(LOGO_CONTAINER_SELECTOR).addClass(COLLAPSED_CLASS);
        $(CREATE_JOIN_SELECTOR).addClass(ON_CREATE_HOST_PAGE_CLASS);
        $(PLAYER_INFO_INP_LABEL_SELECTOR).slideDown(animationTime);

        $(LOGO_IMG_SELECTOR).slideUp(animationTime, function () {
            // after animation finished
            $(CREATE_HOST_BTN_SELECTOR).addClass(COLLAPSED_CLASS);
            $(PLAYER_INFO_INPUT_SELECTOR).fadeIn(animationTime);
            $(JOIN_AS_GUEST_BTN_SELECTOR).addClass(COLLAPSED_CLASS);

        });
    }
    function backFromPlayerNameInputPage(animationTime=400){
        if (animationTime === 0)
            $("*").addClass("no-transition");
        else
            $("*").removeClass("no-transition");

        $(LOGO_CONTAINER_SELECTOR).removeClass(COLLAPSED_CLASS);
        $(PLAYER_INFO_INP_LABEL_SELECTOR).slideUp(animationTime);
        $(LOGO_IMG_SELECTOR).slideDown(animationTime, () => {
            $(LOGO_IMG_SELECTOR).removeClass(COLLAPSED_CLASS);
            $(CREATE_HOST_BTN_SELECTOR).removeClass(COLLAPSED_CLASS);
            $(PLAYER_INFO_INPUT_SELECTOR).fadeOut(animationTime);
            $(JOIN_AS_GUEST_BTN_SELECTOR).removeClass(COLLAPSED_CLASS);
            $(CREATE_JOIN_SELECTOR).removeClass(ON_CREATE_HOST_PAGE_CLASS);
        });
    }


    if (window.location.hash === CREATE_HOST_URL_HASH) {
        console.log("run");
        goToPlayerNameInputPage(0);
    }

    createHostBtn.on('click', function (){
        window.location.hash = CREATE_HOST_URL_HASH;
        goToPlayerNameInputPage(400);
    });
    $(window).on('hashchange', function (){
        if (window.location.hash.replace("#","").length === 0){
            backFromPlayerNameInputPage();
        }
    });
});