package id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.WordRepository;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.RandomStringUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;


class RandomLetterGeneratorSingletonTest {
    IWordRepository wordRepository;
    
    GenerateRandomLetterTask generateRandomLetterTask;
    private MockedStatic<RandomStringUtil> mockedStatic;
    
    
    @BeforeEach
    void setUp() {
        mockedStatic = mockStatic(RandomStringUtil.class);
        
        wordRepository = new WordRepository();
        wordRepository.addWord("cow");
        wordRepository.addWord("dog");
        wordRepository.addWord("good");
        wordRepository.addWord("cat");
        wordRepository.addWord("tictac");
        wordRepository.addWord("fish");
        wordRepository.addWord("caterpilar");
    }
    
    @AfterEach
    public void close() {
        mockedStatic.close();
    }
    
    
    @Test
    void getInstance() {
        assertNotNull(RandomLetterGeneratorSingleton.getInstance());
        assertSame(RandomLetterGeneratorSingleton.getInstance(), RandomLetterGeneratorSingleton.getInstance());
        assertSame(RandomLetterGeneratorSingleton.getInstance(), RandomLetterGeneratorSingleton.getInstance());
    }
    
    
    @Test
    void getRandomLetter() {
        var randomLetterGenerator = RandomLetterGeneratorSingleton.getInstance();
        randomLetterGenerator.setWordRepository(wordRepository);
        var randomLetter = randomLetterGenerator.getRandomLetter(2, 9);
        var randomChar = randomLetter.join();
    
        var wordsCanBeMade = wordRepository.getWordsFromGivenLetters(randomChar);
        assertTrue(wordsCanBeMade.size() >= 2);
    }
}