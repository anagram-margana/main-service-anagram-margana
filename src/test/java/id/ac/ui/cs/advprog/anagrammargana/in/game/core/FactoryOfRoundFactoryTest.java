package id.ac.ui.cs.advprog.anagrammargana.in.game.core;


import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.GameMode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class FactoryOfRoundFactoryTest {
    GameMode gameModeAllAlone;
    GameMode gameModeFreeForAll;
    AllAloneRoundFactory allAloneRoundFactory;
    FreeForAllRoundFactory freeForAllRoundFactory;

    @InjectMocks
    FactoryOfRoundFactory factoryOfRoundFactory;


    @BeforeEach
    void setUp() {
        gameModeAllAlone = GameMode.ALL_ALONE;
        gameModeFreeForAll = GameMode.FREE_FOR_ALL;
        allAloneRoundFactory = new AllAloneRoundFactory();
        freeForAllRoundFactory = new FreeForAllRoundFactory();
    }

    @Test
    void createRoundFactoryTest(){
        var initiateRoundFactory = factoryOfRoundFactory.createRoundFactory(gameModeAllAlone);
        var anotherRoundFactory = factoryOfRoundFactory.createRoundFactory(gameModeFreeForAll);
        assertEquals(allAloneRoundFactory.getClass(), initiateRoundFactory.getClass());
        assertEquals(freeForAllRoundFactory.getClass(), anotherRoundFactory.getClass());
    }
}
