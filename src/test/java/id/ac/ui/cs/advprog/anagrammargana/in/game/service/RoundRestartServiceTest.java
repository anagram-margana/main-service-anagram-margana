package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.MediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.PlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.VoteInformationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class RoundRestartServiceTest {

    VoteInformationDto voteInformationDto;
    IMessageSender messageSender;
    IMediator mediator;
    IPlayer playerHost;
    LoggerFactory loggerFactory;
    Logger logger;
    IRoom room;

    IPlayerRepository playerRepository;
    IMediatorRepository mediatorRepository;
    IMessageSenderFactory messageSenderFactory;


    @InjectMocks
    RoundRestartService roundRestartService;

    @BeforeEach
    void setUp(){
        playerRepository = Mockito.mock(PlayerRepository.class);
        mediatorRepository = Mockito.mock(MediatorRepository.class);
        messageSenderFactory = Mockito.mock(MessageSenderFactory.class);
        messageSender = Mockito.mock(MessageSender.class);
        mediator = Mockito.mock(Mediator.class);
        loggerFactory = Mockito.mock(LoggerFactory.class);
        logger = Mockito.mock(Logger.class);

        playerHost = new Player("Tantra", 123, "ABC");
        room = new Room(playerHost, "MyRoom");


        MockitoAnnotations.openMocks(this);
        when(playerRepository.findByIdOrException(123)).thenReturn(playerHost);
        when(mediatorRepository.findByRoomCodeOrException("MyRoom")).thenReturn(mediator);
        when(messageSenderFactory.create()).thenReturn(messageSender);
        when(mediator.getRoom()).thenReturn(room);
    }

    @Test
    void handleSomeoneVotedForRestartTest(){
        roundRestartService.setLogger(logger);
        roundRestartService.setMessageSenderFactory(messageSenderFactory);

        when(mediator.handlePlayerVoteForRestartingRound(playerHost)).thenReturn(true);
        when(mediator.isRestartEligible()).thenReturn(true);
        var resultForRestart = roundRestartService.handleSomeoneVotedForRestart("MyRoom",123);
        voteInformationDto = new VoteInformationDto(mediator.getNumberOfRestartVotes(), mediator.getRequiredNumberOfVotes());

        verify(messageSender, times(1)).send(room.getMembers(), "vote-restart", voteInformationDto);
        verify(messageSender, times(1)).send(room.getRoomHost(), "vote-restart-eligible", "");
        assertEquals(true, resultForRestart);
    }

    @Test
    void handleSomeoneVotedForRestartFalseTest(){
        roundRestartService.setLogger(logger);
        roundRestartService.setMessageSenderFactory(messageSenderFactory);

        when(mediator.handlePlayerVoteForRestartingRound(playerHost)).thenReturn(false);

        var resultForRestart = roundRestartService.handleSomeoneVotedForRestart("MyRoom",123);
        assertEquals(false, resultForRestart);
    }

    @Test
    void handleSomeoneVotedForRestartEligibleFalseTest(){
        roundRestartService.setLogger(logger);
        roundRestartService.setMessageSenderFactory(messageSenderFactory);

        when(mediator.handlePlayerVoteForRestartingRound(playerHost)).thenReturn(true);
        when(mediator.isRestartEligible()).thenReturn(false);
        var resultForRestart = roundRestartService.handleSomeoneVotedForRestart("MyRoom",123);
        voteInformationDto = new VoteInformationDto(mediator.getNumberOfRestartVotes(), mediator.getRequiredNumberOfVotes());

        verify(messageSender, times(1)).send(room.getMembers(), "vote-restart", voteInformationDto);
        verify(messageSender, never()).send(room.getRoomHost(), "vote-restart-eligible", "");
        assertEquals(true, resultForRestart);
    }

    @Test
    void restartRoundTryTest(){
        roundRestartService.setLogger(logger);
        roundRestartService.setMessageSenderFactory(messageSenderFactory);

        roundRestartService.restartRound("MyRoom");
        verify(mediator, times(1)).restartRound();
        verify(messageSender,times(1)).send(room.getMembers(), "round-restart", "");
    }

    @Test
    void restartRoundCatchTest(){
        roundRestartService.setLogger(logger);
        roundRestartService.setMessageSenderFactory(messageSenderFactory);
        doThrow(new IllegalStateException()).when(mediator).restartRound();

        roundRestartService.restartRound("MyRoom");
        verify(logger, times(1)).info(any(String.class), any(Exception.class));
    }
}
