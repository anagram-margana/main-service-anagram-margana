package id.ac.ui.cs.advprog.anagrammargana.common.util;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DateTimeUtilTest {
    
    @Test
    void addDate() {
        var source = DateTimeUtil.asDate(2022, 10, 3, 4, 3, 2);
        var target = DateTimeUtil.asDate(2022, 10, 6, 4, 5, 2);
        var resultingDate = DateTimeUtil.addDate(source,
                                                Pair.of(3L, TimeUnit.DAYS),
                                                Pair.of(2L, TimeUnit.MINUTES));
        assertEquals(target, resultingDate);
    }
    
    @Test
    void getDateFromNow() {
        var fiveHoursInTheFuture = DateTimeUtil.getDateFromNow(5, TimeUnit.HOURS);
        var now = new Date();
        var difference = DateTimeUtil.getDatesDifference(fiveHoursInTheFuture, now, TimeUnit.SECONDS);
        assertTrue(Math.abs(difference - 5*60*60) <= 5);  // toleransi 5 detik
    }
    
    @Test
    void substractDate() {
        var date1 = DateTimeUtil.asDate(2022, 10, 11, 4, 3, 2);
        var date2 = DateTimeUtil.asDate(2022, 10, 3, 4, 3, 2);
    
        assertEquals(8, DateTimeUtil.substractDate(date1, date2, TimeUnit.DAYS));
        assertEquals(-8, DateTimeUtil.substractDate(date2, date1, TimeUnit.DAYS));
        assertEquals(8*24, DateTimeUtil.substractDate(date1, date2, TimeUnit.HOURS));
        assertEquals(-8*24, DateTimeUtil.substractDate(date2, date1, TimeUnit.HOURS));
    }
    
    @Test
    void getDatesDifference() {
        var date1 = DateTimeUtil.asDate(2022, 10, 11, 4, 3, 2);
        var date2 = DateTimeUtil.asDate(2022, 10, 3, 4, 3, 2);
        
        assertEquals(8, DateTimeUtil.getDatesDifference(date1, date2, TimeUnit.DAYS));
        assertEquals(8, DateTimeUtil.getDatesDifference(date2, date1, TimeUnit.DAYS));
        assertEquals(8*24, DateTimeUtil.getDatesDifference(date1, date2, TimeUnit.HOURS));
    }
    
    @Test
    void asDateAndasLocalDateTimeWorksAsExpected() {
        var localDateTime = LocalDateTime.of(2022, 10, 5, 4, 3, 2);
        var asDate = DateTimeUtil.asDate(localDateTime);
        var asDate2 = DateTimeUtil.asDate(2022, 10, 5, 4, 3, 2);
        var asLocalDate = DateTimeUtil.asLocalDateTime(asDate);
    
        var dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        var dateAsString = dateFormat.format(asDate);
        
        assertEquals(localDateTime, asLocalDate);
        assertEquals(asDate, asDate2);
        assertEquals("2022-10-05 04:03:02", dateAsString);
    }
}