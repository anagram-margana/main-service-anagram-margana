package id.ac.ui.cs.advprog.anagrammargana.common.websocket;


import id.ac.ui.cs.advprog.anagrammargana.common.Constants;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.util.CreateUrl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class MessageSenderFactoryTest {
    MessageSenderFactory messageSenderFactory;
    static URL originalWebsocketUrl;
    static URL websocketServerAddrForTesting = CreateUrl.create("http://192.168.1.9:7070/from-main-site");
    static URL anotherDummyWebsocketServiceAddr = CreateUrl.create("http://192.168.1.21:7075");
    
    
    @SneakyThrows
    @BeforeAll
    static void setUpAll() {
        originalWebsocketUrl = Constants.getWebsocketServerDefaultAddress();
        Constants.setWebsocketServerDefaultAddress(websocketServerAddrForTesting);
    }
    
    @SneakyThrows
    @AfterAll
    static void tearDownAll(){
        Constants.setWebsocketServerDefaultAddress(originalWebsocketUrl);
    }
    
    
    @BeforeEach
    void setUp() {
        messageSenderFactory = new MessageSenderFactory();
    }
    
    @Test
    void create() {
        var result = messageSenderFactory.create();
        assertNotNull(result);
    }
}
