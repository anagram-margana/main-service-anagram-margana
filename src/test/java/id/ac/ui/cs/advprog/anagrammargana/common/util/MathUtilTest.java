package id.ac.ui.cs.advprog.anagrammargana.common.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathUtilTest {
    
    @Test
    void ceilDiv() {
        assertEquals(0, MathUtil.ceilDiv(0, 3));
        assertEquals(1, MathUtil.ceilDiv(1, 3));
        assertEquals(1, MathUtil.ceilDiv(2, 3));
        assertEquals(1, MathUtil.ceilDiv(3, 3));
        assertEquals(2, MathUtil.ceilDiv(4, 3));
        assertEquals(2, MathUtil.ceilDiv(5, 3));
        assertEquals(2, MathUtil.ceilDiv(6, 3));
        assertEquals(3, MathUtil.ceilDiv(7, 3));
    }
}