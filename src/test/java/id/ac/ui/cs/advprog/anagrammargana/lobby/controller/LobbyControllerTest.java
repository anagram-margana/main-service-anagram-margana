package id.ac.ui.cs.advprog.anagrammargana.lobby.controller;

import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {LobbyController.class})
@ExtendWith(SpringExtension.class)
class LobbyControllerTest {
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    IRoomHostValidatorService roomHostValidatorService;
    
    
    @Test
    void lobbyPage() throws Exception {
        var playerId = new Cookie("playerId", "1234");
        var playerName = new Cookie("playerName", "my name");
        var roomCode = new Cookie("roomCode", "RoomCode");
    
        mockMvc.perform(get("/lobby/")
                                .cookie(playerId, playerName, roomCode)
        ).andExpect(status().isOk());
    }
}