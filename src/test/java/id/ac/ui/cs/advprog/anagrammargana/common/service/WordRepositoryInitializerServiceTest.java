package id.ac.ui.cs.advprog.anagrammargana.common.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator.IRandomLetterGeneratorSingleton;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.WordRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class WordRepositoryInitializerServiceTest {
    IRandomLetterGeneratorSingleton randomLetterGeneratorSingleton;
    
    @MockBean
    WordRepository wordRepository;
    
    @InjectMocks
    WordRepositoryInitializerService wordRepositoryInitializerService;
    
    @BeforeEach
    @SneakyThrows
    void setup(){
        randomLetterGeneratorSingleton = Mockito.mock(IRandomLetterGeneratorSingleton.class);
        wordRepositoryInitializerService.setRandomLetterGeneratorSingleton(randomLetterGeneratorSingleton);
        MockitoAnnotations.openMocks(this);
        wordRepositoryInitializerService.setDictionaryFilePath(
                Path.of("src/test/resources/static/dictionary.txt")
        );
    }
    
    @Test
    void wordRepositoryInitializer() {
        wordRepositoryInitializerService.wordRepositoryInitializer();
        
        var addedWords = List.of(
                "halo",
                "dunia",
                "adalah"
        );
    
        var argCaptor = ArgumentCaptor.forClass(String.class);
        verify(wordRepository, times(addedWords.size())).addWord(argCaptor.capture());
        
        var calledParams = argCaptor.getAllValues();
        var calledParamsSet = new HashSet<>(calledParams);
        var addedWordsSet = new HashSet<>(addedWords);
        assertEquals(addedWordsSet, calledParamsSet);
    }
    
    
    @Test
    void wordRepositoryInitializer_doNotHandleException() {
        doThrow(IllegalStateException.class).when(wordRepository).addWord(anyString());
        assertThrows(IllegalStateException.class,
                     () -> wordRepositoryInitializerService.wordRepositoryInitializer());
    }
    
    
    @Test
    void randomLetterGeneratorSingletonInitializeWordRepository(){
        wordRepositoryInitializerService.initializeWordRepositoryForRandomLetterGenerator();
        verify(randomLetterGeneratorSingleton, times(1))
                .setWordRepository(wordRepository);
    }
}