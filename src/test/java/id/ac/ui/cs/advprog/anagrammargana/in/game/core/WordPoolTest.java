package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import com.google.common.collect.Sets;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.util.DateTimeUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class WordPoolTest {

    Room room1;
    String roomCode = "84321";
    final static IPlayer roomHost = new Player("I am host", 123L, "SESSION TOKEN");
    Date endTime = DateTimeUtil.asDate(2022, 5, 21, 9, 3, 30);
    int minimumWord = 1;
    String word = "COASTER";
    Round round1;
    ISortedChars sortedChars;
    Collection<String> wordPoolCollection = new ArrayList<>();
    Set<String> wordsFound = new HashSet<>();
    WordPool wordPool;

    @BeforeEach
    public void setup(){
        sortedChars = new SortedChars(word);
        room1 = new Room(roomHost, roomCode);
        round1 = new Round(room1, endTime, minimumWord, sortedChars);
        
        wordPool = new WordPool();
        wordPool.addWord("COASTER");
    }

    @Test
    void doesWordExistWorksProperly(){
        assertTrue(wordPool.doesWordExist("COASTER"));
    }

    @Test
    void addWordWorksProperly(){
        assertFalse(wordPool.doesWordExist("SISTER"));
        wordPool.addWord("SISTER");
        assertTrue(wordPool.doesWordExist("SISTER"));
    }

    @Test
    void addWordRunsCorrectlyEvenIfWordHasAlreadyBeenAdded(){
        assertFalse(wordPool.doesWordExist("SISTER"));
        wordPool.addWord("SISTER");
        assertTrue(wordPool.doesWordExist("SISTER"));
        wordPool.addWord("SISTER");
        assertTrue(wordPool.doesWordExist("SISTER"));
    }

    @Test
    void getWordsFoundWorksProperly(){
        wordPool.addWord("SISTER");
        
        wordsFound.add("COASTER");
        wordsFound.add("SISTER");
        var temp = new HashSet<>(wordPool.getWordsFound());
        assertEquals(0, Sets.difference(temp, wordsFound).size());
    }

    @Test
    void testEquals() {
        assertEquals(wordPool, wordPool);
        assertNotEquals(null, wordPool);
        assertNotEquals(1234, wordPool);
        assertNotEquals(wordPool, new WordPool());
    }

    @Test
    void testHashCode() {
        assertEquals(1657491906, wordPool.hashCode());
    }
}
