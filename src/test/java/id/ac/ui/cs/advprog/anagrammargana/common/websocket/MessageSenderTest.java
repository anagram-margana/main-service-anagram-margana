package id.ac.ui.cs.advprog.anagrammargana.common.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammargana.common.Constants;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidMessageDataException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.util.CreateUrl;
import lombok.SneakyThrows;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class MessageSenderTest {
    public static MockWebServer mockBackEnd;
    private static URL originalWebsocketUrl;
    private static String dummyArbitraryHost = "http://192.168.1.1:8081";
    private static String websocketServerAddrForTesting;
    private static URL websocketServerUrlForTesting;
    private static String targetUrl = "/from-main-site";
    
    @SneakyThrows
    @BeforeAll
    static void setUp() {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
        
        websocketServerAddrForTesting = String.format("http://localhost:%s", mockBackEnd.getPort());
        websocketServerUrlForTesting = CreateUrl.create(websocketServerAddrForTesting);
        
        originalWebsocketUrl = Constants.getWebsocketServerDefaultAddress();
        Constants.setWebsocketServerDefaultAddress(CreateUrl.create(websocketServerAddrForTesting));
    }
    
    @SneakyThrows
    @AfterAll
    static void tearDown(){
        mockBackEnd.shutdown();
        Constants.setWebsocketServerDefaultAddress(originalWebsocketUrl);
    }
    
    
    
    ObjectMapper objectMapper = new ObjectMapper();
    IPlayer player1;
    IPlayer player2;
    String type = "testing-message";
    String body = "body";
    
    MessageData<String> messageData1;
    
    MessageData<String> messageData2;
    
    @BeforeEach
    void setup(){
        var dummyArbitraryHostUrl = CreateUrl.create(websocketServerAddrForTesting);
        player1 = new Player("azdaha", 101, "asdfghjkl", dummyArbitraryHostUrl);
        player2 = new Player("dahasa", 102, "qwertyuiop", dummyArbitraryHostUrl);
        messageData1 = new MessageData<>(player1.getId(), null, player1.getSessionToken(), type, body);
        messageData2 = new MessageData<>(player2.getId(), null, player2.getSessionToken(), type, body);
    }
    
    
    
    
    @SneakyThrows
    @Test
    void sendWithMessageDataRunsCorrectly() {
        mockBackEnd.enqueue(new MockResponse().setBody("1"));
        var messageSender = new MessageSender();
        messageSender.send(player1, messageData1.getType(), messageData1.getBody());

        var recordedRequest = mockBackEnd.takeRequest(9, TimeUnit.SECONDS);
        var sentMessage = objectMapper.readValue(recordedRequest.getBody().readUtf8(), MessageData.class);
        assertEquals("POST", recordedRequest.getMethod());
        assertEquals(messageData1, sentMessage);
    }
    
    @SneakyThrows
    @Test
    void sendWithMessageDataRunsCorrectlyEvenIfBodyNull() {
        mockBackEnd.enqueue(new MockResponse().setBody("1"));
        var messageSender = new MessageSender();
        messageSender.send(player1, messageData1.getType(), null);

        var recordedRequest = mockBackEnd.takeRequest(9, TimeUnit.SECONDS);
        var sentMessage = objectMapper.readValue(recordedRequest.getBody().readUtf8(), MessageData.class);

        var messageData = new MessageData<String>();
        messageData.copyDataFrom(messageData1);
        messageData.setBody(null);

        assertEquals("POST", recordedRequest.getMethod());
        assertEquals(messageData, sentMessage);
    }
    
    
    
    @SneakyThrows
    @Test
    void sendWithMessageDataRunsCorrectly_2() {
        mockBackEnd.enqueue(new MockResponse().setBody("1"));
        var messageSender = new MessageSender();
        messageSender.send(player1, messageData1.getType(), messageData1.getBody());

        var recordedRequest = mockBackEnd.takeRequest(9, TimeUnit.SECONDS);
        var sentMessage = objectMapper.readValue(recordedRequest.getBody().readUtf8(), MessageData.class);
        assertEquals("POST", recordedRequest.getMethod());
        assertEquals(messageData1, sentMessage);
    }
    
    
    
    @SneakyThrows
    @Test
    void sendWithMessageDataRunsCorrectly_3() {
        mockBackEnd.enqueue(new MockResponse().setBody("1"));
        mockBackEnd.enqueue(new MockResponse().setBody("1"));
        var messageSender = new MessageSender();
        messageSender.send(List.of(player1, player2), messageData1.getType(), messageData1.getBody());

        var recordedRequest1 = mockBackEnd.takeRequest(6, TimeUnit.SECONDS);
        var recordedRequest2 = mockBackEnd.takeRequest(6, TimeUnit.SECONDS);
        var sentMessage1 = objectMapper.readValue(recordedRequest1.getBody().readUtf8(), MessageData.class);
        var sentMessage2 = objectMapper.readValue(recordedRequest2.getBody().readUtf8(), MessageData.class);

        assertEquals("POST", recordedRequest1.getMethod());
        assertEquals("POST", recordedRequest2.getMethod());
        assertEquals(Set.of(messageData1, messageData2),
                     Set.of(sentMessage1, sentMessage2));
    }
    
    
    @SuppressWarnings("ConstantConditions")
    @SneakyThrows
    @Test
    void sendWithMessageHashMapRunsCorrectly() {
        mockBackEnd.enqueue(new MockResponse().setBody("1"));
        mockBackEnd.enqueue(new MockResponse().setBody("1"));
        
        var type = "testing-msg-type";
        var data1 = "qwert";
        var data2 = "asdfg";
        
        var map = Map.of(
                player1, data1,
                player2, data2
        );
        
        var messageSender = new MessageSender();
        messageSender.send(type, map);
    
        var recordedRequest1 = mockBackEnd.takeRequest(6, TimeUnit.SECONDS);
        var recordedRequest2 = mockBackEnd.takeRequest(6, TimeUnit.SECONDS);
        var sentMessage1 = objectMapper.readValue(recordedRequest1.getBody().readUtf8(), MessageData.class);
        var sentMessage2 = objectMapper.readValue(recordedRequest2.getBody().readUtf8(), MessageData.class);
    
        assertEquals("POST", recordedRequest1.getMethod());
        assertEquals("POST", recordedRequest2.getMethod());
        assertEquals(Set.of(data1, data2),
                     Set.of(sentMessage1.getBody(), sentMessage2.getBody()));
        assertEquals(type, sentMessage1.getType());
        assertEquals(type, sentMessage2.getType());
    }
    
    
    @SneakyThrows
    @Test
    void sendWithNullSessionShouldBeHandled() {
        var messageSender = new MessageSender();
        var player = new Player(player1.getName(), player1.getId(), null, player1.getWebsocketUrl());
        
        assertThrows(InvalidMessageDataException.class,
                     () -> messageSender.send(player, null, messageData1)
        );
    }
    
    @SneakyThrows
    @Test
    void sendWithNullTypeShouldBeHandled() {
        var messageSender = new MessageSender();

        assertThrows(InvalidMessageDataException.class,
                     () -> messageSender.send(player1, null, messageData1)
        );
    }
    
    
    @SneakyThrows
    @Test
    void sendWithFailResponseShouldBeLogged() {
        mockBackEnd.enqueue(new MockResponse().setBody("0"));
        var logger = Mockito.mock(Logger.class);
        doNothing().when(logger).error(anyString(), any(), any());

        var messageSender = new MessageSender();
        messageSender.setLogger(logger);
        var completableFuture = messageSender.send(player1, messageData1.getType(), messageData1.getBody());

        completableFuture.get(6, TimeUnit.SECONDS);
        verify(logger, times(1)).error(anyString(), any(Object.class), any(Object.class));
    }

}