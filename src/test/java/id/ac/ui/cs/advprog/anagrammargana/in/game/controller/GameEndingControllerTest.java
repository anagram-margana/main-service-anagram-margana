package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.GameEndingService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {GameEndingController.class})
class GameEndingControllerTest {
    @MockBean
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @MockBean
    IRoomHostValidatorService roomHostValidatorService;
    
    @MockBean
    GameEndingService gameEndingService;
    
    
    @Autowired
    MockMvc mockMvc;
    
    
    long playerId = 123;
    String roomCode = "MyRoom";
    String sessionToken = "asdfghjkl";
    MessageData<String> messageData;
    
    ObjectMapper objectMapper = new ObjectMapper();
    
    @BeforeEach
    void setUp() {
        messageData = new MessageData<>(playerId, roomCode, sessionToken, null, "some answer");
        doNothing().when(playerAuthenticationService).throwIfNotAuthenticated(messageData);
    }
    
    @Test
    @SneakyThrows
    void hostDecideToFinishTheGame(){
        var result =
                mockMvc.perform(MockMvcRequestBuilders.post("/communication-api/msg-from-client/finish-the-game")
                                        .content(objectMapper.writeValueAsString(messageData))
                                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();
        assertEquals("", result.getResponse().getContentAsString());
        
        verify(playerAuthenticationService, times(1)).throwIfNotAuthenticated(messageData);
        verify(roomHostValidatorService, times(1)).throwIfNotRoomHost(messageData);
        verify(gameEndingService, times(1)).finishTheGame(messageData.getRoomCode());
    }
    
}