package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.LatestUpdateDto;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.PlayerScoreData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class AllAloneAnswerHandlingStrategyTest {
    IMessageSender messageSenderMock;
    IMessageSenderFactory messageSenderFactoryMock;
    AllAloneAnswerHandlingStrategy allAloneAnswerHandlingStrategy;

    IPlayer roomHost;
    IPlayer guest;
    IWordPool sharedWordPoolHost;
    IWordPool sharedWordPoolGuest;

    IRoom room;
    IScoreboard scoreboard;
    IRound round;
    ISortedChars currentRoundRandomLetters;
    String answer = "TestAbcdef";

    String wordFound1 = "dog";
    String wordFound2 = "cat";


    @BeforeEach
    void setUp() {
        messageSenderMock = Mockito.mock(MessageSender.class);
        messageSenderFactoryMock = Mockito.mock(MessageSenderFactory.class);
        when(messageSenderFactoryMock.create()).thenReturn(messageSenderMock);
        when(messageSenderMock.send(eq("aa-new-answer-submission"),
                any(HashMap.class))
        ).thenReturn(null);
        allAloneAnswerHandlingStrategy = new AllAloneAnswerHandlingStrategy();

        roomHost = new Player("Tantra", 123, "");
        guest = new Player("Nuel", 124, "");
        room = new Room(roomHost, "MyRoom");
        room.addMember(guest);
        currentRoundRandomLetters = new SortedChars("TestAbcdef" + "asdasdasdasd");

        round = new Round(room, new Date(), 5, currentRoundRandomLetters);
        sharedWordPoolHost = new WordPool();
        sharedWordPoolGuest = new WordPool();

        sharedWordPoolHost.addWord(wordFound1);
        sharedWordPoolGuest.addWord(wordFound2);

        round.setWordPool(roomHost, sharedWordPoolHost);
        round.setWordPool(guest, sharedWordPoolGuest);

        scoreboard = Mockito.mock(Scoreboard.class);
    }

    @Test
    @SuppressWarnings("unchecked")
    void notifyPlayersWhenAnswerValid() {
        var scoreHashmap = Map.of(
                roomHost, 10,
                guest, 8
        );
        var playerScoreDataHost = new PlayerScoreData(roomHost.getId(), 10, roomHost.getName());
        var playerScoreDataGuest = new PlayerScoreData(guest.getId(), 8, guest.getName());

        when(scoreboard.asHashmap()).thenReturn(scoreHashmap);


        allAloneAnswerHandlingStrategy.setMessageSenderFactory(messageSenderFactoryMock);
        allAloneAnswerHandlingStrategy.notifyPlayersWhenAnswerValid(
                round, room, scoreboard, guest, answer);

       var dataToBeSent = ArgumentCaptor.forClass(HashMap.class);


        verify(messageSenderMock, times(1)).
                send(eq("aa-new-answer-submission"),
                        dataToBeSent.capture()
                );

        var latestUpdateSent = dataToBeSent.getValue();
        assertEquals(2, latestUpdateSent.size());

        var latestUpdateHost = (LatestUpdateDto) latestUpdateSent.get(roomHost);
        Assertions.assertNotEquals(roomHost.getId(), latestUpdateHost.getPlayerIdWhoSubmittedAnswer());
        Assertions.assertNotEquals(roomHost.getName(), latestUpdateHost.getPlayerNameWhoSubmittedAnswer());

        var latestUpdateGuest = (LatestUpdateDto) latestUpdateSent.get(guest);
        assertEquals(guest.getId(), latestUpdateGuest.getPlayerIdWhoSubmittedAnswer());
        assertEquals(guest.getName(), latestUpdateGuest.getPlayerNameWhoSubmittedAnswer());

        var wordsFoundHost = latestUpdateHost.getWordsFound();
        assertEquals(1, wordsFoundHost.size());
        assertTrue(wordsFoundHost.contains(wordFound1));

        var wordsFoundGuest = latestUpdateGuest.getWordsFound();
        assertEquals(1, wordsFoundGuest.size());
        assertTrue(wordsFoundGuest.contains(wordFound2));

        var playerScoreDataHostList = latestUpdateHost.getScoreData();
        assertEquals(2, playerScoreDataHostList.size());
        assertTrue(playerScoreDataHostList.contains(playerScoreDataHost));

        var playerScoreDataGuestList = latestUpdateGuest.getScoreData();
        assertEquals(2, playerScoreDataGuestList.size());
        assertTrue(playerScoreDataGuestList.contains(playerScoreDataGuest));
    }
}