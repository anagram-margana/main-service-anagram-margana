package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.PlayerIdAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Answers.CALLS_REAL_METHODS;
import static org.mockito.Mockito.*;

class PlayerRepositoryTest {
    IPlayerRepository playerRepository;
    
    String player1_name = "Davi Hizki";
    long player1_id = 3463L;
    String player1_session_token = "ABCDEFGH";
    IPlayer player1;
    
    
    @BeforeEach
    public void createMocks() {
        playerRepository = Mockito.mock(PlayerRepository.class, Mockito.withSettings()
                        .useConstructor()
                        .defaultAnswer(CALLS_REAL_METHODS)
        );
        player1 = new Player(player1_name, player1_id, player1_session_token);
    }
    
    
    @Test
    void add() {
        var playerObjWithInvalidId = new Player("abcde", player1_id, "ABCDEFGH");
        assertFalse(playerRepository.hasPlayerId(player1_id));
        
        playerRepository.add(player1);
        assertTrue(playerRepository.hasPlayerId(player1_id));
        assertSame(player1, playerRepository.findByIdOrException(player1_id));
        
        assertThrows(PlayerIdAlreadyExistException.class,
                     () -> playerRepository.add(playerObjWithInvalidId));
    }
    
    
    @Test
    void findByIdOrException() {
        assertThrows(InvalidPlayerIdException.class,
                     () -> playerRepository.findByIdOrException(player1_id));
    
        playerRepository.add(player1);
        assertSame(player1, playerRepository.findByIdOrException(player1_id));
    }
    
    @Test
    void authenticateAndGetPlayer() {
        var anExampleOfInvalidId = 205L;
        var anExampleOfInvalidToken = "POIUYTREWQ";
        
        doReturn(player1).when(playerRepository).findByIdOrException(player1_id);
        
        assertThrows(InvalidPlayerIdException.class,
                     () -> playerRepository.authenticateAndGetPlayer(anExampleOfInvalidId, anExampleOfInvalidToken));
        assertThrows(InvalidSessionTokenException.class,
                     () -> playerRepository.authenticateAndGetPlayer(player1_id, anExampleOfInvalidToken));
        
        var player = playerRepository.authenticateAndGetPlayer(player1_id, player1_session_token);
        assertSame(player1, player);
    }
    
    @Test
    void isIdTaken() {
        var id2 = 207L;
        
        var name2 = "Fadeline Zahvaria";
        var session2 = "QWERTYUUIOP";
        
        assertFalse(playerRepository.hasPlayerId(id2));
        var nonExistentPlayer = playerRepository.findByIdOrNull(id2);
        assertTrue(nonExistentPlayer.isEmpty());
        
        var player2 = new Player(name2, id2, session2);
        playerRepository.add(player2);
    
        assertTrue(playerRepository.hasPlayerId(id2));
        assertSame(player2, playerRepository.findByIdOrException(id2));
    
        playerRepository.add(player1);
        assertSame(player1, playerRepository.findByIdOrException(player1_id));
        assertTrue(playerRepository.hasPlayerId(player1_id));
        assertTrue(playerRepository.hasPlayerId(id2));
    }
    
    @Test
    void removePlayerWithId(){
        playerRepository.add(player1);
        
        assertTrue(playerRepository.hasPlayerId(player1_id));
        playerRepository.removePlayerWithId(player1_id);
        assertFalse(playerRepository.hasPlayerId(player1_id));
        
        assertThrows(InvalidPlayerIdException.class,
                     () -> playerRepository.removePlayerWithId(player1_id));
    }
    
}