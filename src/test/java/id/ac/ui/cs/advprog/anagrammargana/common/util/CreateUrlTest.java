package id.ac.ui.cs.advprog.anagrammargana.common.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class CreateUrlTest {
    String dummyUrlPath = "http://localhost:8080";
    String dummyFilePath = "arbitrary-path/dummy-server-homepage.html";
    URL dummyContext;
    
    @BeforeEach
    void setup(){
        try {
            dummyContext = new URL(dummyUrlPath);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    
    
    @Test
    void createReturnsCorrectObject() {
        var result = CreateUrl.create(dummyUrlPath);
        assertEquals(dummyUrlPath, result.toString());
        assertEquals(URL.class, result.getClass());
    }
    
    @Test
    void createReturnsCorrectObject_2() {
        var result = CreateUrl.create(dummyContext, dummyFilePath);
        assertEquals(dummyUrlPath + "/" + dummyFilePath,
                     result.toString());
        assertEquals(URL.class, result.getClass());
    }
    
  
}