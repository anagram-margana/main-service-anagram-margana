package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.util.CreateUrl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URL;

import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
class PlayerWebsocketAddressUpdaterServiceTest {
    @MockBean
    IPlayerRepository playerRepository;
    
    @InjectMocks
    PlayerWebsocketAddressUpdaterService playerWebsocketAddressUpdaterService;
    
    IPlayer player;
    long playerId = 123;
    String playerName = "Player";
    String sessionToken = "asdfgh";
    URL oldWebsocketUrl = CreateUrl.create("http://localhost:8080");
    URL newWebsocketUrl = CreateUrl.create("http://localhost:8081");
    
    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
        player = new Player(playerName, playerId, sessionToken, oldWebsocketUrl);
        when(playerRepository.findByIdOrException(playerId)).thenReturn(player);
    }
    
    @Test
    void updatePlayerWebsocketAddress() {
        Assertions.assertEquals(oldWebsocketUrl, player.getWebsocketUrl());
        playerWebsocketAddressUpdaterService.updatePlayerWebsocketAddress(playerId, newWebsocketUrl);
        Assertions.assertEquals(newWebsocketUrl, player.getWebsocketUrl());
    }
}