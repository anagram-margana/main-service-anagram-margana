package id.ac.ui.cs.advprog.anagrammargana.common.repository;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.ISortedChars;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.SortedChars;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class WordRepositoryTest {
    IWordRepository wordRepository;
    Set<String> expectedWordsFromGivenLetters;
    ISortedChars randomSortedChars;

    @BeforeEach
    void setUp() {
        expectedWordsFromGivenLetters = new HashSet<>();
        wordRepository = new WordRepository();
        randomSortedChars = new SortedChars("bkomnaibo");
        expectedWordsFromGivenLetters.add("bomb");
        expectedWordsFromGivenLetters.add("book");
        expectedWordsFromGivenLetters.add("moon");
        wordRepository.addWord("bomb");
        wordRepository.addWord("book");
        wordRepository.addWord("moon");
        wordRepository.addWord("zoom");
        wordRepository.addWord("computer");
    }

    @Test
    void getWordsFromGivenLetters() {
        assertEquals(expectedWordsFromGivenLetters, wordRepository.getWordsFromGivenLetters(randomSortedChars));
    }

    @Test
    void doesWordExist() {
        assertTrue(wordRepository.doesWordExist("computer"));
        assertFalse(wordRepository.doesWordExist("laptop"));
        wordRepository.addWord("laptop");
        assertTrue(wordRepository.doesWordExist("laptop"));
    }

    @Test
    void addWord() {
        assertFalse(wordRepository.doesWordExist("notes"));
        wordRepository.addWord("notes");
        assertTrue(wordRepository.doesWordExist("notes"));
    }
}