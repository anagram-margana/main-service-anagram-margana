package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.util.DateTimeUtil;
import id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions.InvalidAnswer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class DefaultAnswerHandlingStrategyTest {
    DefaultAnswerHandlingStrategy defaultAnswerHandlingStrategy;
    IRound round;
    IWordRepository wordRepository;
    IWordPool wordPool;
    IPlayer player;
    String answer = "halo dunia";
    
    @BeforeEach
    void setUp() {
        defaultAnswerHandlingStrategy = Mockito.mock(DefaultAnswerHandlingStrategy.class,
                                                     Mockito.CALLS_REAL_METHODS);
        round = Mockito.mock(IRound.class);
        wordRepository = Mockito.mock(IWordRepository.class);
        wordPool = Mockito.mock(IWordPool.class);
        player = new Player("yow", 123, "");
        when(round.getWordPool(player)).thenReturn(wordPool);
    }
    
    @Test
    void throwIfAnswerNotValid_doThrowIfRoundHasBeenEnded() {
        var timeInThePast = DateTimeUtil.getDateFromNow(-5, TimeUnit.SECONDS);
        when(round.getRoundEndTime()).thenReturn(timeInThePast);
        assertThrows(InvalidAnswer.class,
                     () -> defaultAnswerHandlingStrategy.throwIfAnswerNotValid(wordRepository, round, player, answer));
    }
    
    
    @Test
    void throwIfAnswerNotValid_doThrowIfGivenWordIsNotInRepository() {
        var timeInTheFuture = DateTimeUtil.getDateFromNow(100, TimeUnit.SECONDS);
        when(round.getRoundEndTime()).thenReturn(timeInTheFuture);
        when(wordRepository.doesWordExist(answer)).thenReturn(false);
        assertThrows(InvalidAnswer.class,
                     () -> defaultAnswerHandlingStrategy.throwIfAnswerNotValid(wordRepository, round, player, answer));
    }
    
    @Test
    void throwIfAnswerNotValid_doThrowIfGivenWordCannotBeMadeFromAvailableLetters() {
        var timeInTheFuture = DateTimeUtil.getDateFromNow(100, TimeUnit.SECONDS);
        when(round.getRoundEndTime()).thenReturn(timeInTheFuture);
        when(wordRepository.doesWordExist(answer)).thenReturn(true);
        when(round.getRandomLetters()).thenReturn(new SortedChars(""));
        
        assertThrows(InvalidAnswer.class,
                     () -> defaultAnswerHandlingStrategy.throwIfAnswerNotValid(wordRepository, round, player, answer));
    }
    
    @Test
    void throwIfAnswerNotValid_doThrowIfGivenWordHasAlreadyBeenTaken() {
        var timeInTheFuture = DateTimeUtil.getDateFromNow(100, TimeUnit.SECONDS);
        when(round.getRoundEndTime()).thenReturn(timeInTheFuture);
        when(wordRepository.doesWordExist(answer)).thenReturn(true);
        when(round.getRandomLetters()).thenReturn(new SortedChars(answer));
        when(wordPool.doesWordExist(answer)).thenReturn(true);
        
        assertThrows(InvalidAnswer.class,
                     () -> defaultAnswerHandlingStrategy.throwIfAnswerNotValid(wordRepository, round, player, answer));
    }
    
    @Test
    void throwIfAnswerNotValid_doNotThrowIfEverythingIsOkay() {
        var timeInTheFuture = DateTimeUtil.getDateFromNow(100, TimeUnit.SECONDS);
        when(round.getRoundEndTime()).thenReturn(timeInTheFuture);
        when(wordRepository.doesWordExist(answer)).thenReturn(true);
        when(round.getRandomLetters()).thenReturn(new SortedChars(answer));
        when(wordPool.doesWordExist(answer)).thenReturn(false);
        
        
        assertDoesNotThrow(() -> defaultAnswerHandlingStrategy.throwIfAnswerNotValid(wordRepository, round, player, answer));
    }
}