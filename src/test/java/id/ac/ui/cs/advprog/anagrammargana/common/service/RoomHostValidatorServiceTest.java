package id.ac.ui.cs.advprog.anagrammargana.common.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomHostException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RoomHostValidatorServiceTest {
    @MockBean
    RoomRepository roomRepository;
    
    @InjectMocks
    RoomHostValidatorService roomHostValidatorService;
    
    
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    void isHostCorrectlyReturnsTrue() {
        var roomCode = "ROOM";
        var hostId = 123;
        
        var host = new Player("name", hostId, "session");
        var room = new Room(host, roomCode);
        var messageData = new MessageData<>(hostId, roomCode, "", "", "");
        
        when(roomRepository.findByRoomCodeOrNull(roomCode)).thenReturn(Optional.of(room));
        
        assertTrue(roomHostValidatorService.isHost(roomCode, hostId));
        assertTrue(roomHostValidatorService.isHost(messageData));
    }
    
    @Test
    void isHostCorrectlyReturnsFalse() {
        var roomCode = "ROOM";
        var hostId = 123;
        var notHostId = 124;
        
        var host = new Player("name", hostId, "session");
        var room = new Room(host, roomCode);
        var messageData = new MessageData<>(notHostId, roomCode, "", "", "");
        
        when(roomRepository.findByRoomCodeOrNull(roomCode)).thenReturn(Optional.of(room));
        
        assertFalse(roomHostValidatorService.isHost(roomCode, notHostId));
        assertFalse(roomHostValidatorService.isHost(messageData));
    }
    
    
    @Test
    void isHostThrowsExceptionOnInvalidRoomCode() {
        var invalidRoomCode = "IDK";
        var arbitraryNumber = 123;
        var messageData = new MessageData<>(arbitraryNumber, invalidRoomCode, "", "", "");
        
        when(roomRepository.findByRoomCodeOrNull(invalidRoomCode)).thenReturn(Optional.empty());
        
        assertThrows(InvalidRoomCodeException.class,
                     () -> roomHostValidatorService.isHost(invalidRoomCode, arbitraryNumber));
        assertThrows(InvalidRoomCodeException.class,
                     () -> roomHostValidatorService.isHost(messageData));
    }
    
    
    @Test
    void throwIfNotRoomHostRunsCorrectly() {
        var roomCode = "ROOM";
        var hostId = 123;
        var notHostId = 124;
        
        var host = new Player("name", hostId, "session");
        var room = new Room(host, roomCode);
        
        // assert no exception thrown
        {
            var messageData = new MessageData<>(hostId, roomCode, "", "", "");
            when(roomRepository.findByRoomCodeOrNull(roomCode)).thenReturn(Optional.of(room));
            
            roomHostValidatorService.throwIfNotRoomHost(roomCode, hostId);
            roomHostValidatorService.throwIfNotRoomHost(messageData);
        }
    
        // assert InvalidRoomHostException is thrown
        {
            var messageData = new MessageData<>(notHostId, roomCode, "", "", "");
            when(roomRepository.findByRoomCodeOrNull(roomCode)).thenReturn(Optional.of(room));
    
            assertThrows(InvalidRoomHostException.class,
                         () -> roomHostValidatorService.throwIfNotRoomHost(roomCode, notHostId));
            assertThrows(InvalidRoomHostException.class,
                         () -> roomHostValidatorService.throwIfNotRoomHost(messageData));
        }
    }
}