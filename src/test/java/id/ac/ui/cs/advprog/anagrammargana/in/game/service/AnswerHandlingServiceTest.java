package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.MediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class AnswerHandlingServiceTest {

    Logger logger;
    IRoom room;
    IPlayer playerHost;
    IMediator mediator;
    ILoggerFactory loggerFactory;

    IMediatorRepository mediatorRepository;
    IPlayerRepository playerRepository;

    @InjectMocks
    AnswerHandlingService answerHandlingService;

    @BeforeEach
    void setUp(){
        mediatorRepository = Mockito.mock(MediatorRepository.class);
        playerRepository = Mockito.mock(PlayerRepository.class);
        mediator = Mockito.mock(Mediator.class);

        loggerFactory = Mockito.mock(ILoggerFactory.class);
        logger = Mockito.mock(Logger.class);

        playerHost = new Player("Tantra", 123, "ABC");
        room = new Room(playerHost, "MyRoom");

        MockitoAnnotations.openMocks(this);
        when(loggerFactory.getLogger("AnswerHandlingService")).thenReturn(logger);
        when(mediatorRepository.findByRoomCodeOrException("MyRoom")).thenReturn(mediator);
        when(playerRepository.findByIdOrNull(123)).thenThrow(IllegalStateException.class);
        when(playerRepository.findByIdOrException(123)).thenReturn(playerHost);
    }
    @Test
    void handleAnswerTryTest(){
        answerHandlingService.setLogger(logger);
        answerHandlingService.handleAnswer("MyRoom", 123, "TEST");
        verify(mediator, times(1)).handleNewAnswer(playerHost, "TEST");
        verify(logger, never()).info(any(String.class), any(Exception.class));
    }

    @Test
    void handleAnswerCatchTest(){
        answerHandlingService.setLogger(logger);
        doThrow(new IllegalStateException()).when(mediator).handleNewAnswer(playerHost, "TEST");
        answerHandlingService.handleAnswer("MyRoom", 123, "TEST");
        verify(mediator, times(1)).handleNewAnswer(playerHost, "TEST");
        verify(logger, times(1)).info(any(String.class), any(Exception.class));
    }
}
