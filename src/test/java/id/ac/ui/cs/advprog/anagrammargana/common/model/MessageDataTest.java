package id.ac.ui.cs.advprog.anagrammargana.common.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MessageDataTest {
    
    MessageData<String> messageData;
    String json = "{\"playerId\": 234, " +
            "\"playerId\": 234, " +
            "\"roomCode\": \"SomeRoomCode\", " +
            "\"sessionToken\": \"SomeSession\", " +
            "\"type\": \"testing-type\", " +
            "\"body\": \"c\" " +
            "}";
    
    @BeforeEach
    void setUp() {
        messageData = new MessageData<>(234, "SomeRoomCode",
                                        "SomeSession", "testing-type", "c");
    }
    
    @Test
    void copyDataFrom() {
        var copiedMessageData = new MessageData<String>();
        copiedMessageData.copyDataFrom(messageData);
    
    
        assertNotSame(messageData, copiedMessageData);
        
        assertNull(copiedMessageData.getBody());
        copiedMessageData.setBody(messageData.body);
        assertEquals(messageData, copiedMessageData);
    }
    
    @Test
    void fromJson_returnCorrectMessageDataEvenIfNullBody() {
        String json = "{\"playerId\": 234, " +
                "\"playerId\": 234, " +
                "\"roomCode\": \"SomeRoomCode\", " +
                "\"sessionToken\": \"SomeSession\", " +
                "\"type\": \"testing-type\"" +
                "}";
        
        var resultingMessageData = MessageData.fromJson(json, String.class);
        assertNotSame(messageData, resultingMessageData);
    
        messageData.setBody(null);
        assertNull(resultingMessageData.getBody());
        assertEquals(messageData, resultingMessageData);
    }
    
    @Test
    void fromJson_returnCorrectMessageData() {
        var resultingMessageData = MessageData.fromJson(json, String.class);
        assertNotSame(messageData, resultingMessageData);
        assertEquals(String.class, resultingMessageData.getBody().getClass());
        assertEquals(messageData, resultingMessageData);
    }
    
    @Test
    void fromJson_immediatelyDetectClassCastExceptionForCertainTypes() {
        // character is one of those types that's not supported
        assertDoesNotThrow(() -> {
            var result = MessageData.fromJson(json, Character.class);
            assertEquals('c', result.getBody());
        });
    }
}