package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.controller;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.PlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.RoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.WebsocketPlayerAuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class WebsocketPlayerAuthenticationServiceTest {
    
    IPlayerRepository playerRepository;
    IRoomRepository roomRepository;
    
    @InjectMocks
    WebsocketPlayerAuthenticationService websocketPlayerAuthenticationService;
    
    long validId = 123;
    long invalidId = 240;
    
    String name = "orang";
    String sessionToken = "asdfghjkl";
    String invalidSessionToken = "qwertyuiop";
    String roomCode = "myRoom";
    String invalidRoomCode = "asdfg";
    
    IPlayer player;
    
    
    @BeforeEach
    void setup(){
        player = new Player(name, validId, sessionToken);
        playerRepository = Mockito.mock(PlayerRepository.class);
        roomRepository = Mockito.mock(RoomRepository.class);
    
        MockitoAnnotations.openMocks(this);
        
        when(playerRepository.authenticateAndGetPlayer(validId, sessionToken))
                .thenReturn(player);
        when(playerRepository.authenticateAndGetPlayer(invalidId, sessionToken))
                .thenThrow(new InvalidPlayerIdException());
        when(playerRepository.authenticateAndGetPlayer(validId, invalidSessionToken))
                .thenThrow(new InvalidSessionTokenException());
        var room = new Room(player, roomCode);
        when(roomRepository.findByRoomCodeOrNull(roomCode))
                .thenReturn(Optional.of(room));
    }
    
    @Test
    void authenticate() {
        assertTrue(websocketPlayerAuthenticationService.authenticate(roomCode, validId, sessionToken));
        assertFalse(websocketPlayerAuthenticationService.authenticate(roomCode, invalidId, sessionToken));
    }
    
    @Test
    void authenticateWithMessageDto() {
        var message = new MessageData<>(validId, roomCode, sessionToken, "", "");
        assertTrue(websocketPlayerAuthenticationService.authenticate(message));
    
        var messageWithInvalidId = new MessageData<>(invalidId, roomCode, sessionToken, "", "");
        assertFalse(websocketPlayerAuthenticationService.authenticate(messageWithInvalidId));
    }
    
    @Test
    void throwIfNotAuthenticatedMessageData() {
        // no throw
        var message = new MessageData<>(validId, roomCode, sessionToken, "", "");
        websocketPlayerAuthenticationService.throwIfNotAuthenticated(message);
    
        var messageWithInvalidId = new MessageData<>(invalidId, roomCode, sessionToken, "", "");
        assertThrows(InvalidPlayerIdException.class,
                     () -> websocketPlayerAuthenticationService.throwIfNotAuthenticated(messageWithInvalidId));
        
        
    
        var messageWithInvalidRoomCode
                = new MessageData<>(validId, invalidRoomCode, sessionToken, "", "");
        assertThrows(InvalidRoomCodeException.class,
                     () -> websocketPlayerAuthenticationService.throwIfNotAuthenticated(messageWithInvalidRoomCode));
    }
    
}