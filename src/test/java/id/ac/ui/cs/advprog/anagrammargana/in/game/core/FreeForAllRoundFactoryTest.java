package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.util.DateTimeUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class FreeForAllRoundFactoryTest {

    IPlayer host;
    ISortedChars randomChars;
    IRoom room;
    IRound round;
    IWordPool wordPool;
    int roundDurationInMinute;
    int minimumWords;

    @InjectMocks
    FreeForAllRoundFactory freeForAllRoundFactory;

    @BeforeEach
    public void setUp(){
        host = new Player("Tantra", 123, "abcd");
        room = new Room(host, "MyRoom");
        roundDurationInMinute = 5;
        minimumWords = 3;

        var endTime = DateTimeUtil.getDateFromNow(roundDurationInMinute, TimeUnit.MINUTES);

        wordPool = new WordPool();
        round = new Round(room, endTime, minimumWords, randomChars);
        round.setWordPool(host, wordPool);
    }

    @Test
    void initiateRoundTest(){
        var inititateRound = freeForAllRoundFactory.initiateRound(room, roundDurationInMinute,
                                                                  minimumWords, randomChars);

        assertEquals(round.getWordPool(host), inititateRound.getWordPool(host));
        assertRoundEquals(round, inititateRound);
    }
    
    private void assertRoundEquals(IRound round1, IRound round2){
        assertEquals(round1.getWordPools(), round2.getWordPools());
        assertEquals(round1.getMinimumWord(), round2.getMinimumWord());
        assertEquals(round1.getRandomLetters(), round2.getRandomLetters());
        
        var roundEndTime1 = round1.getRoundEndTime();
        var roundEndTime2 = round2.getRoundEndTime();
        var timeDifference = DateTimeUtil.getDatesDifference(roundEndTime1, roundEndTime2, TimeUnit.SECONDS);
        assertTrue(timeDifference <= 3);
    }
}
