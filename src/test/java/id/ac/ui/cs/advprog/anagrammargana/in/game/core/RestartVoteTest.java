package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class RestartVoteTest {
    RestartVote restartVote;
    int minimumVote;
    IPlayer player;
    ArrayList<IPlayer> affirmativePlayers;

    @BeforeEach
    void setUp() {
        minimumVote = 2;
        affirmativePlayers = new ArrayList<>();
        player = new Player("Vyll", 12345678, "ABCDEFGHIJ");
        restartVote = new RestartVote(minimumVote);
        restartVote.addAffirmativePlayer(player);
        affirmativePlayers.add(player);
    }

    @Test
    void getAffirmativePlayer() {
        assertEquals(affirmativePlayers, restartVote.getAffirmativePlayer());
    }
    
    
    @Test
    void getNumberOfAffirmativePlayer() {
        assertEquals(affirmativePlayers.size(), restartVote.getNumberOfAffirmativePlayer());
        
        var anotherPlayer = new Player("idk", 123, "");
        restartVote.addAffirmativePlayer(anotherPlayer);
        affirmativePlayers.add(anotherPlayer);
        assertEquals(affirmativePlayers.size(), restartVote.getNumberOfAffirmativePlayer());
    }

    @Test
    void addAffirmativePlayer() {
        restartVote.getAffirmativePlayer().remove(player);
        assertFalse(restartVote.isAffirmative(player));
        
        restartVote.addAffirmativePlayer(player);
        assertTrue(restartVote.isAffirmative(player));
    }
    
    @Test  // edge case
    void multipleAddAffirmativePlayerWontCauseAnyProblem(){
        restartVote.addAffirmativePlayer(player);
        restartVote.addAffirmativePlayer(player);
        restartVote.addAffirmativePlayer(player);
        assertTrue(restartVote.isAffirmative(player));
        assertEquals(1, restartVote.getAffirmativePlayer().size());
    }
    

    @Test
    void isAffirmative() {
        assertTrue(restartVote.isAffirmative(player));
    }

    @Test
    void getMinimumRequiredNumberOfAgreement() {
        assertEquals(minimumVote, restartVote.getRequiredNumberOfVotes());
    }

    @Test
    void isRestartVoteEligible() {
        assertFalse(restartVote.isRestartVoteEligible());
        
        var anotherPlayer1 = new Player("abc", 123, "");
        var anotherPlayer2 = new Player("abc", 124, "");
        
        
        restartVote.addAffirmativePlayer(anotherPlayer1);
        assertTrue(restartVote.isRestartVoteEligible());
    
        // edge case
        restartVote.addAffirmativePlayer(anotherPlayer2);
        assertTrue(restartVote.isRestartVoteEligible());
    }
}