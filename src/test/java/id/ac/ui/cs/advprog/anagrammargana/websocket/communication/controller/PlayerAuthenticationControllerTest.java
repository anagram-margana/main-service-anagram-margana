package id.ac.ui.cs.advprog.anagrammargana.websocket.communication.controller;

import id.ac.ui.cs.advprog.anagrammargana.common.util.CreateUrl;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.PlayerWebsocketAddressUpdaterService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.WebsocketPlayerAuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {PlayerAuthenticationController.class})
@ExtendWith(SpringExtension.class)
class PlayerAuthenticationControllerTest {
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    WebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @MockBean
    PlayerWebsocketAddressUpdaterService playerWebsocketAddressUpdaterService;
    
    
    String roomCode = "abcd";
    long playerId = 1234;
    String sessionToken = "asdfghjkl";
    URL websocketUrl = CreateUrl.create("http://localhost:8081");
    
    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
    }
    
    
    @Test
    void authenticateSuccessAndValidUrlReturnsOne() throws Exception {
        when(playerAuthenticationService.authenticate(roomCode, playerId, sessionToken)).thenReturn(true);
        
        var result =
                mockMvc.perform(post("/communication-api/init")
                                        .param("roomCode", roomCode)
                                        .param("playerId", Long.toString(playerId))
                                        .param("sessionToken", sessionToken)
                                        .param("websocketServiceAddress", websocketUrl.toString())
        ).andExpect(status().isOk()).andReturn();
    
        verify(playerWebsocketAddressUpdaterService, times(1))
                .updatePlayerWebsocketAddress(playerId, websocketUrl);
        assertEquals("1", result.getResponse().getContentAsString());
    }
    
    @Test
    void authenticateInValidUrlReturnsNegative() throws Exception {
        when(playerAuthenticationService.authenticate(roomCode, playerId, sessionToken)).thenReturn(true);
        
        var result =
                mockMvc.perform(post("/communication-api/init")
                                        .param("roomCode", roomCode)
                                        .param("playerId", Long.toString(playerId))
                                        .param("sessionToken", sessionToken)
                                        .param("websocketServiceAddress", "some-invalid-url")
        ).andExpect(status().isOk()).andReturn();
    
        assertEquals("-1", result.getResponse().getContentAsString());
    }
    
    
    @Test
    void authenticateFailReturnsZero() throws Exception {
        when(playerAuthenticationService.authenticate(roomCode, playerId, sessionToken)).thenReturn(false);
        
        var result =
                mockMvc.perform(post("/communication-api/init")
                                        .param("roomCode", roomCode)
                                        .param("playerId", Long.toString(playerId))
                                        .param("sessionToken", sessionToken)
                                        .param("websocketServiceAddress", websocketUrl.toString())
        ).andExpect(status().isOk()).andReturn();
    
        assertEquals("0", result.getResponse().getContentAsString());
    }
}