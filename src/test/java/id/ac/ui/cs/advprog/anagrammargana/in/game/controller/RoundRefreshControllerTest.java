package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.*;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.RoundRefreshService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {RoundRefreshController.class})
class RoundRefreshControllerTest {
    @MockBean
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @MockBean
    RoundRefreshService roundRefreshService;
    
    @Autowired
    MockMvc mockMvc;
    
    
    long playerId = 123;
    String roomCode = "MyRoom";
    String sessionToken = "asdfghjkl";
    MessageData<String> messageData;
    
    ObjectMapper objectMapper = new ObjectMapper();
    
    @BeforeEach
    void setUp() {
        messageData = new MessageData<>(playerId, roomCode, sessionToken, null, "");
        doNothing().when(playerAuthenticationService).throwIfNotAuthenticated(messageData);
    }
    
    @Test
    @SneakyThrows
    void handleInformationRefreshRequest() {
        var playerScoreData = new PlayerScoreData(playerId, 80, "aw");
        var listOfPlayerScoreData = List.of(playerScoreData);
        var latestUpdateDto = new LatestUpdateDto(listOfPlayerScoreData, playerId, "yow",
                                                  null, List.of(""));
        var startNewRoundDto = new StartNewRoundDto(3, "abcde", Map.of(4, 5));
        var voteInformationDto = new VoteInformationDto(1, 2);
        var roundRefreshData = new RoundRefreshData(latestUpdateDto, startNewRoundDto, voteInformationDto);
    
        when(roundRefreshService.getRoundRefreshData(roomCode, playerId)).thenReturn(roundRefreshData);
        
        
        var result = mockMvc.perform(
                MockMvcRequestBuilders.post("/communication-api/msg-from-client/refresh-round-information")
                        .content(objectMapper.writeValueAsString(messageData))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        
        var responseAsStr = result.getResponse().getContentAsString();
        var resultingMessageData = MessageData.fromJson(responseAsStr, RoundRefreshData.class);
        
        var expectedMessageData = new MessageData<RoundRefreshData>();
        expectedMessageData.copyDataFrom(messageData);
        expectedMessageData.setType("refresh-round-information");
        expectedMessageData.setBody(roundRefreshData);
        
        assertEquals(roundRefreshData, resultingMessageData.getBody());
        assertEquals(expectedMessageData, resultingMessageData);
        verify(playerAuthenticationService, times(1)).throwIfNotAuthenticated(messageData);
    }
}