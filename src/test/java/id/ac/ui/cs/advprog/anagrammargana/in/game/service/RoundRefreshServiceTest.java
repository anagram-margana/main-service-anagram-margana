package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.util.DateTimeUtil;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.*;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.LatestUpdateDto;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.PlayerScoreData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.StartNewRoundDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RoundRefreshServiceTest {
    IPlayer player1;
    IPlayer player2;
    long playerId1 = 123;
    long playerId2 = 124;
    int playerScore1 = 17;
    int playerScore2 = 10;
    IRoom room;
    String roomCode = "MyRoom";
    int numberOfRestartVote = 3;
    int requiredNumberOfVote = 5;
    
    IMediator mediator;
    IScoreboard scoreboard;
    
    
    IPlayerRepository playerRepository;
    IMediatorRepository mediatorRepository;
    
    
    int timeRemainingInMinute = 10;
    Date now;
    
    Round round;
    int arbitraryInt = 10;
    ISortedChars givenRandomLetters = new SortedChars("asdfghjkl");
    Map<Integer, Integer> wordLengthCount = Map.of(
            4, 5,
            7, 2,
            8, 1
    );
    
    @InjectMocks
    RoundRefreshService roundRefreshService;
    
    
    @BeforeEach
    void setUp() {
        now = DateTimeUtil.getDateFromNow(timeRemainingInMinute, TimeUnit.MINUTES);
        
        playerRepository = Mockito.mock(IPlayerRepository.class);
        mediatorRepository = Mockito.mock(IMediatorRepository.class);
        MockitoAnnotations.openMocks(this);
        
        player1 = new Player("abc", playerId1, "");
        player2 = new Player("def", playerId2, "");
        room = new Room(player1, roomCode);
        room.addMember(player2);
        round = new Round(room, now, arbitraryInt, givenRandomLetters);
        
        var vote = mock(RestartVote.class);
        when(vote.getNumberOfAffirmativePlayer()).thenReturn(numberOfRestartVote);
        when(vote.getRequiredNumberOfVotes()).thenReturn(requiredNumberOfVote);
    
        scoreboard = Mockito.mock(Scoreboard.class);
        Map<IPlayer, Integer> hashmap = Map.of(
                player1, playerScore1,
                player2, playerScore2
        );
        when(scoreboard.asHashmap()).thenReturn(hashmap);
        
        mediator = mock(Mediator.class);
        when(mediator.getRoom()).thenReturn(room);
        when(mediator.getScoreBoard()).thenReturn(scoreboard);
        when(mediator.getRestartVote()).thenReturn(vote);
        
        var player2Id = player2.getId();
        when(playerRepository.findByIdOrException(player2Id)).thenReturn(player2);
        when(mediatorRepository.findByRoomCodeOrException(room.getCode())).thenReturn(mediator);
    }
    
    @Test
    void getRoundRefreshDataReturnsNullWhenMediatorHasNoRound() {
        when(mediator.getCurrentRound()).thenReturn(null);
        assertNull(roundRefreshService.getRoundRefreshData(roomCode, playerId2));
    }
    
    
    @Test
    void getRoundRefreshDataReturnsNullWhenMediatorRoundHasEnded() {
        var round = Mockito.mock(Round.class);
        var now = new Date();
        when(round.getRoundEndTime()).thenReturn(now);
        when(mediator.getCurrentRound()).thenReturn(round);
        
        assertNull(roundRefreshService.getRoundRefreshData(roomCode, playerId2));
    }
    
    @Test
    void getRoundRefreshData() {
        when(mediator.getCurrentRound()).thenReturn(round);
        when(mediator.getWordLengthToNumberOfWordsMapping()).thenReturn(wordLengthCount);
    
        
        var word1 = "abcd";
        var word2 = "efghi";
        
        var wordPool = new WordPool();
        wordPool.addWord(word1);
        wordPool.addWord(word2);
        round.setWordPool(player2, wordPool);
        
        var result = roundRefreshService.getRoundRefreshData(room.getCode(), player2.getId());
        var resultRoundInfo = result.getRoundInformation();
        var resultLatestScoreUpdate = result.getLatestScoreUpdate();
        var voteResult = result.getVoteInformation();
    
        verifyRoundInformation(resultRoundInfo);
        verifyScoreData(resultLatestScoreUpdate);
    
        var resultWordsFound = new HashSet<>(resultLatestScoreUpdate.getWordsFound());
        assertEquals(Set.of(word1, word2), resultWordsFound);
        
        assertEquals(numberOfRestartVote, voteResult.getNumberOfVote());
        assertEquals(requiredNumberOfVote, voteResult.getRequiredNumberOfVotes());
    }
    
    void verifyRoundInformation(StartNewRoundDto roundInformation){
        var resultRandomLetters = roundInformation.getRandomLetters();
        var resultTimeLimit = roundInformation.getTimeLimit();
        var resultWordLengthCount = roundInformation.getWordLengthToNumberOfWordsMapping();
        assertEquals(givenRandomLetters.getSortedString(), resultRandomLetters);
        assertEquals(timeRemainingInMinute*60, resultTimeLimit, 3);
        assertEquals(wordLengthCount, resultWordLengthCount);
    }
    
    void verifyScoreData(LatestUpdateDto resultLatestScoreUpdate){
        //        verify score data
        var scoreData = resultLatestScoreUpdate.getScoreData();
        scoreData.sort(Comparator.comparing(PlayerScoreData::getPlayerId));
        var scoreData1 = scoreData.get(0);
        var scoreData2 = scoreData.get(1);
    
        assertEquals(playerId1, scoreData1.getPlayerId());
        assertEquals(player1.getName(), scoreData1.getName());
        assertEquals(playerScore1, scoreData1.getScore());
    
        assertEquals(playerId2, scoreData2.getPlayerId());
        assertEquals(player2.getName(), scoreData2.getName());
        assertEquals(playerScore2, scoreData2.getScore());
    }
    
}