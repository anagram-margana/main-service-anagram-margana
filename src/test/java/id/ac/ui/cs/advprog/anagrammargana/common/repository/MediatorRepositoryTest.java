package id.ac.ui.cs.advprog.anagrammargana.common.repository;


import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomCodeAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Answers.CALLS_REAL_METHODS;


class MediatorRepositoryTest {
    IMediatorRepository mediatorRepository;
    
    IMediator mediator;
    IWordRepository wordRepository ;
    
    IRoom room;
    String roomCode = "87654";
    IPlayer roomHost;
    IPlayer roomMember;
    
    
    @BeforeEach
    public void setup(){
        mediatorRepository = Mockito.mock(MediatorRepository.class, Mockito.withSettings()
                .useConstructor()
                .defaultAnswer(CALLS_REAL_METHODS)
        );
        wordRepository = Mockito.mock(WordRepository.class, Mockito.withSettings()
                .useConstructor()
                .defaultAnswer(Answers.RETURNS_DEFAULTS)
        );
    
        roomHost = new Player("I am host", 123L, "");
        roomMember = new Player("I am normal member", 124L, "");
        room = new Room(roomHost, roomCode);
        room.addMember(roomMember);
        mediator = new Mediator(room, wordRepository);
        
        mediatorRepository.add(mediator);
    }
    
    
    @Test
    void doesRoomCodeExist() {
        var validRoomCode = mediator.getRoomCode();
        var invalidRoomCode = "000000";
        
        assertTrue(mediatorRepository.hasMediator(validRoomCode));
        assertFalse(mediatorRepository.hasMediator(invalidRoomCode));
    }
    
    @Test
    void findByRoomCodeOrNull() {
        var validRoomCode = mediator.getRoomCode();
        var invalidRoomCode = "000000";
    
        assertSame(mediator, mediatorRepository.findByRoomCodeOrException(validRoomCode));
        var mediator = mediatorRepository.findByRoomCodeOrNull(invalidRoomCode);
        assertTrue(mediator.isEmpty());
    }
    
    
    @Test
    void findByRoomCodeOrException() {
        var validRoomCode = mediator.getRoomCode();
        var invalidRoomCode = "000000";
    
        assertSame(mediator, mediatorRepository.findByRoomCodeOrException(validRoomCode));
        assertThrows(InvalidRoomCodeException.class,
                     () -> mediatorRepository.findByRoomCodeOrException(invalidRoomCode));
    }
    
    @Test
    void add() {
        var newHost = new Player("I am a host of another room", 300L, "");
        var newRoomCode = "another room";
        var newRoom = new Room(newHost, newRoomCode);
        var newMediator = new Mediator(newRoom, wordRepository);
        
        assertFalse(mediatorRepository.hasMediator(newRoomCode));
        mediatorRepository.add(newMediator);
        assertTrue(mediatorRepository.hasMediator(newRoomCode));
        assertSame(newMediator, mediatorRepository.findByRoomCodeOrException(newRoomCode));
        
        var anotherNewHost = new Player("Idk who I am", 400L, "");
        var roomWithInvalidRoomCode = new Room(anotherNewHost, newRoomCode);
        var mediatorWithInvalidRoomCode = new Mediator(roomWithInvalidRoomCode, wordRepository);
        assertThrows(RoomCodeAlreadyExistException.class,
                     () -> mediatorRepository.add(mediatorWithInvalidRoomCode));
    }
    
    @Test
    void removeRoomByRoomCode() {
        var invalidRoomCode = "POIUYTREWQ";
        assertThrows(InvalidRoomCodeException.class,
                     () -> mediatorRepository.removeMediatorByRoomCode(invalidRoomCode));
        
        assertTrue(mediatorRepository.hasMediator(roomCode));
        mediatorRepository.removeMediatorByRoomCode(roomCode);
        assertFalse(mediatorRepository.hasMediator(roomCode));
    }
}