package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;

import id.ac.ui.cs.advprog.anagrammargana.common.service.RoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.create.join.core.CookieFactory;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.servlet.http.Cookie;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {InGamePageController.class})
class InGamePageControllerTest {
    @MockBean
    RoomHostValidatorService roomHostValidatorService;
    
    @Autowired
    MockMvc mockMvc;
    
    long playerId = 123;
    String roomCode = "MyRoom";
    String playerName = "sujo kunchoro";
    
    @BeforeEach
    void setUp() {
    }
    
    CookieFactory cookieFactory = new CookieFactory();
    
    @Test
    @SneakyThrows
    void receiveGetRequestForInGamePage_whenPlayerIsAHost() {
        var resultAsStr = receiveGetRequestForInGamePage(true);
        assertTrue(resultAsStr.contains("const IS_HOST = true;"));
    }
    
    @Test
    @SneakyThrows
    void receiveGetRequestForInGamePage_whenPlayerIsGuest() {
        var resultAsStr = receiveGetRequestForInGamePage(false);
        assertTrue(resultAsStr.contains("const IS_HOST = false;"));
    }
    
    @SneakyThrows
    String receiveGetRequestForInGamePage(boolean asHost){
        Mockito.when(roomHostValidatorService.isHost(roomCode, playerId)).thenReturn(asHost);
        var cookies = new Cookie[]{
                cookieFactory.createCookie("playerId", Long.toString(playerId)),
                cookieFactory.createCookie("roomCode", roomCode),
                cookieFactory.createCookie("playerName", playerName)
        };
    
        var result =
                mockMvc.perform(MockMvcRequestBuilders.get("/in-game")
                                        .cookie(cookies))
                        .andExpect(status().isOk())
                        .andReturn();
    
        var resultAsStr = result.getResponse().getContentAsString();
        assertTrue(resultAsStr.contains(playerName));
        return resultAsStr;
    }
}