package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.*;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.LatestUpdateDto;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.PlayerScoreData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class FreeForAllAnswerHandlingStrategyTest {
    IMessageSender messageSenderMock;
    IMessageSenderFactory messageSenderFactoryMock;
    FreeForAllAnswerHandlingStrategy freeForAllAnswerHandlingStrategy;
    
    IPlayer roomHost;
    IPlayer guest;
    IWordPool sharedWordPool;
    
    IRoom room;
    IScoreboard scoreboard;
    IRound round;
    ISortedChars currentRoundRandomLetters;
    String answer = "TestAbcdef";
    
    String wordFound1 = "dog";
    String wordFound2 = "cat";
    
    
    @BeforeEach
    void setUp() {
        messageSenderMock = Mockito.mock(MessageSender.class);
        messageSenderFactoryMock = Mockito.mock(MessageSenderFactory.class);
        when(messageSenderFactoryMock.create()).thenReturn(messageSenderMock);
        when(messageSenderMock.send(any(Collection.class), eq("ffa-new-answer-submission"),
                                    any(LatestUpdateDto.class))
            ).thenReturn(null);
        freeForAllAnswerHandlingStrategy = new FreeForAllAnswerHandlingStrategy();
        
        roomHost = new Player("Tantra", 123, "");
        guest = new Player("Nuel", 124, "");
        room = new Room(roomHost, "MyRoom");
        room.addMember(guest);
        currentRoundRandomLetters = new SortedChars("TestAbcdef" + "asdasdasdasd");
    
        round = new Round(room, new Date(), 5, currentRoundRandomLetters);
        sharedWordPool = new WordPool();
        sharedWordPool.addWord(wordFound1);
        sharedWordPool.addWord(wordFound2);
        round.setWordPool(roomHost, sharedWordPool);
        round.setWordPool(guest, sharedWordPool);
        
        scoreboard = Mockito.mock(Scoreboard.class);
    }
    
    @Test
    void notifyPlayersWhenAnswerValid() {
        var scoreHashmap = Map.of(
                roomHost, 10,
                guest, 8
        );
        var playerScoreDataHost = new PlayerScoreData(roomHost.getId(), 10, roomHost.getName());
        var playerScoreDataGuest = new PlayerScoreData(guest.getId(), 8, guest.getName());
        
        when(scoreboard.asHashmap()).thenReturn(scoreHashmap);
    
    
        freeForAllAnswerHandlingStrategy.setMessageSenderFactory(messageSenderFactoryMock);
        freeForAllAnswerHandlingStrategy.notifyPlayersWhenAnswerValid(
                round, room, scoreboard, guest, answer);
        
        var latestUpdateCaptor = ArgumentCaptor.forClass(LatestUpdateDto.class);
        
        verify(messageSenderMock, times(1)).
                send(eq(room.getMembers()),
                     eq("ffa-new-answer-submission"),
                     latestUpdateCaptor.capture()
                );

        var latestUpdateYgDikirim = latestUpdateCaptor.getValue();
        assertEquals(answer, latestUpdateYgDikirim.getRecentlyDiscoveredWord());
        assertEquals(guest.getId(), latestUpdateYgDikirim.getPlayerIdWhoSubmittedAnswer());
        assertEquals(guest.getName(), latestUpdateYgDikirim.getPlayerNameWhoSubmittedAnswer());
        
        var wordsFound = latestUpdateYgDikirim.getWordsFound();
        assertEquals(2, wordsFound.size());
        assertTrue(wordsFound.contains(wordFound1));
        assertTrue(wordsFound.contains(wordFound2));
        
        var playerScoreData = latestUpdateYgDikirim.getScoreData();
        assertEquals(2, playerScoreData.size());
        assertTrue(playerScoreData.contains(playerScoreDataGuest));
        assertTrue(playerScoreData.contains(playerScoreDataHost));
    }
}