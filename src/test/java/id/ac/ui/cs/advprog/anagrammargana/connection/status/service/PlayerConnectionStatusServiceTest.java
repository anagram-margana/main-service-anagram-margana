package id.ac.ui.cs.advprog.anagrammargana.connection.status.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.service.RoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.PlayerConnectionStatusDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.*;


@ExtendWith(SpringExtension.class)
class PlayerConnectionStatusServiceTest {
    
    @MockBean
    IPlayerRepository playerRepository;
    
    @MockBean
    IRoomRepository roomRepository;
    
    @MockBean
    RoomHostValidatorService roomHostValidatorService;
    
    @InjectMocks
    PlayerConnectionStatusService playerConnectionStatusService;
    
    String roomCode = "MyRoom";
    long hostId = 123;
    long playerId = 124;
    IPlayer host = new Player("host", hostId, "");
    IPlayer player = new Player("player", playerId, "");
    IRoom room = new Room(host, roomCode);
    
    @Mock
    IMessageSender messageSender;
    @Mock
    IMessageSenderFactory messageSenderFactory;
    
    @BeforeEach
    void setUp() {
        room.addMember(player);
        
        MockitoAnnotations.openMocks(this);
        when(playerRepository.findByIdOrException(hostId)).thenReturn(host);
        when(playerRepository.findByIdOrException(playerId)).thenReturn(player);
        when(roomRepository.findByRoomCodeOrException(roomCode)).thenReturn(room);
        
        when(roomHostValidatorService.isHost(roomCode, playerId)).thenReturn(false);
        when(roomHostValidatorService.isHost(roomCode, hostId)).thenReturn(true);
        
        when(messageSenderFactory.create()).thenReturn(messageSender);
        playerConnectionStatusService.setMessageSenderFactory(messageSenderFactory);
    }
    
    @Test
    void handleSomeoneDisconnected_runsWellWhenRoomIsHost() {
        verifyHandleSomeoneDisconnected(hostId, host.getName(), true);
    }
    
    @Test
    void handleSomeoneDisconnected_runsWellWhenRoomIsGuest() {
        verifyHandleSomeoneDisconnected(playerId, player.getName(), false);
    }
    
    @Test
    void handleSomeoneConnected_runsWellWhenRoomIsHost() {
        verifyHandleSomeoneConnected(hostId, host.getName(), true);
    }
    
    @Test
    void handleSomeoneConnected_runsWellWhenRoomIsGuest() {
        verifyHandleSomeoneConnected(playerId, player.getName(), false);
    }
 
    
    private void verifyHandleSomeoneDisconnected(long playerId, String playerName, boolean isRoomHost){
        var playerDisconnectedMessage = new PlayerConnectionStatusDto(playerId, playerName, isRoomHost);
        
        playerConnectionStatusService.handleSomeoneDisconnected(roomCode, playerId);
        verify(messageSender, times(1))
                .send(room.getMembers(), "player-disconnected", playerDisconnectedMessage);
    }
    
    private void verifyHandleSomeoneConnected(long playerId, String playerName, boolean isRoomHost){
        var playerConnectedMessage = new PlayerConnectionStatusDto(playerId, playerName, isRoomHost);
        
        playerConnectionStatusService.handleSomeoneConnected(roomCode, playerId);
        verify(messageSender, times(1))
                .send(room.getMembers(), "player-connected", playerConnectedMessage);
    }
}