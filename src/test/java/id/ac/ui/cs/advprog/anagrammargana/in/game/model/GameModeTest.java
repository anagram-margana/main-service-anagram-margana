package id.ac.ui.cs.advprog.anagrammargana.in.game.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameModeTest {
    @Test
    void values() {
        assertDoesNotThrow(GameMode::values);
    }
}