package id.ac.ui.cs.advprog.anagrammargana.common.core;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PlayerTest {
    final static String nama = "Hanz Fraizko";
    final static long id = 12345678;
    final static String sessionToken = "ABCDEFGH IJKLMNOP QRSTUVWX";
    URL websocketUrl;
    
    
    IPlayer player;
    
    
    @BeforeEach
    @SneakyThrows
    public void setup(){
        websocketUrl = new URL("http://localhost:7071");
        player = new Player(nama, id, sessionToken, websocketUrl);
    }
    
    @Test
    @SneakyThrows
    void equality_shouldNotDependsOnWebsocketAddr() {
        // equality and hashcode should only depends on its name, its id, and its session token.
        var differentWebsocketUrl = new URL("http://localhost:7071");
        var player2 = new Player(nama, id, sessionToken, differentWebsocketUrl);
        
        assertEquals(player2, player);
        assertEquals(player2.hashCode(), player.hashCode());
    }
    
    @Test
    @SneakyThrows
    void equality_dependsOnName() {
        // equality and hashcode should only depends on its name, its id, and its session token.
        var player2 = new Player("arbitrary-name", id, sessionToken, websocketUrl);
        
        assertNotEquals(player2, player);
        assertNotEquals(player2.hashCode(), player.hashCode());
    }
    
    @Test
    @SneakyThrows
    void equality_dependsOnId() {
        // equality and hashcode should only depends on its name, its id, and its session token.
        var player2 = new Player(nama, 0, sessionToken, websocketUrl);
        
        assertNotEquals(player2, player);
        assertNotEquals(player2.hashCode(), player.hashCode());
    }
    
    @Test
    @SneakyThrows
    void equality_dependsOnNSessionToken() {
        // equality and hashcode should only depends on its name, its id, and its session token.
        var player2 = new Player(nama, id, "arbitrary-session", websocketUrl);
        
        assertNotEquals(player2, player);
        assertNotEquals(player2.hashCode(), player.hashCode());
    }
    
    
    
    @Test
    void getNameWorkProperly() {
        assertEquals(nama, player.getName());
    }
    
    @Test
    void getID() {
        assertEquals(id, player.getId());
    }
    
    @Test
    void getSessionToken() {
        assertEquals(sessionToken, player.getSessionToken());
    }
    
    @Test
    void getWebsocketUrl() {
        assertEquals(websocketUrl, player.getWebsocketUrl());
    }
    
    
}