package id.ac.ui.cs.advprog.anagrammargana.common.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.random.letter.generator.IRandomLetterGeneratorSingleton;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.*;
import id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions.InvalidAnswer;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.StartNewRoundDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class MediatorTest {
    @MockBean
    IWordRepository wordRepository;
    
    IPlayer roomHost;
    String roomCode = "RoomCodeXyz";
    IRoom room;
    @Mock
    IScoreboard scoreboard;
    @Mock
    IRestartVote restartVote;
    @Mock
    IGameModeAnswerHandlingStrategy answerHandler;
    @Mock
    IRound currentRound;
    
    @Mock
    IMessageSenderFactory messageSenderFactory;
    @Mock
    IMessageSender messageSender;
    
    Mediator mediator;
    
    
    
    @BeforeEach
    void setUp() {
        roomHost = new Player("idk", 101, "asdfghjkl");
        room = new Room(roomHost, roomCode);
        mediator = new Mediator(room, wordRepository, restartVote,
                                scoreboard, answerHandler, currentRound);
        
        when(messageSenderFactory.create()).thenReturn(messageSender);
        mediator.setMessageSenderFactory(messageSenderFactory);
    }
    
    @Test
    void getRoomCode() {
        assertEquals(roomCode, mediator.getRoomCode());
    }
    
    @Test
    void getRoom() {
        assertSame(room, mediator.getRoom());
    }
    
    @Test
    void getScoreBoard() {
        var scoreboard = mediator.getScoreBoard();
        assertSame(0L, scoreboard.getScore(roomHost));
    }
    
    @Test
    void getCurrentRound() {
        assertSame(currentRound, mediator.getCurrentRound());
    }
    
    @Test
    void getRestartVote() {
        var restartVotes = mediator.getRestartVote();
        assertEquals(0, restartVotes.getRequiredNumberOfVotes());
    }
    
    @Test
    void getWordRepository() {
        assertSame(wordRepository, mediator.getWordRepository());
    }
    
    
    
    @Test
    void getNumberOfRestartVotes() {
        when(restartVote.getNumberOfAffirmativePlayer()).thenReturn(5);
        assertEquals(5, mediator.getNumberOfRestartVotes());
    }
    
    @Test
    void getRequiredNumberOfVotes() {
        when(restartVote.getRequiredNumberOfVotes()).thenReturn(7);
        assertEquals(7, mediator.getRequiredNumberOfVotes());
    }
    
    @Test
    void isRestartEligible() {
        when(restartVote.isRestartVoteEligible())
                .thenReturn(true)
                .thenReturn(false)
                .thenReturn(true);
        assertTrue(mediator.isRestartEligible());
        assertFalse(mediator.isRestartEligible());
        assertTrue(mediator.isRestartEligible());
    }
    
    @Test
    void restartRoundThrowsWhenRestartingIsNotEligible() {
        when(restartVote.isRestartVoteEligible()).thenReturn(false);
        assertThrows(IllegalStateException.class,
                     () -> mediator.restartRound());
    }
    
    @Test
    void restartRoundRunsCorrectly() {
        when(restartVote.isRestartVoteEligible()).thenReturn(true);
        mediator.restartRound();
        verify(currentRound, times(1)).stopRound();
    
        mediator.currentRound = null;
        assertDoesNotThrow(() -> mediator.restartRound());
    }
    
    @Test
    void handlePlayerVoteForRestartingRound() {
        when(restartVote.isAffirmative(roomHost)).thenReturn(true);
        assertFalse(mediator.handlePlayerVoteForRestartingRound(roomHost));
        
        when(restartVote.isAffirmative(roomHost)).thenReturn(false);
        assertTrue(mediator.handlePlayerVoteForRestartingRound(roomHost));
        verify(restartVote, times(1)).addAffirmativePlayer(roomHost);
    }
    
    @Test
    void handleNewAnswerRunsCorrectlyForValidAnswer() {
        var arbitraryAnswer = "cat";
        var mediatorWithNullRound = new Mediator(room, wordRepository, restartVote, scoreboard, answerHandler, null);
        assertThrows(InvalidAnswer.class,
                     () -> mediatorWithNullRound.handleNewAnswer(roomHost, arbitraryAnswer));
        
        doNothing().when(answerHandler).throwIfAnswerNotValid(wordRepository, currentRound,
                                                              roomHost, arbitraryAnswer);
        
        var wordPool = Mockito.mock(WordPool.class);
        when(currentRound.getWordPool(roomHost)).thenReturn(wordPool);
    
        mediator.handleNewAnswer(roomHost, arbitraryAnswer);
        
        verify(wordPool, times(1)).addWord(arbitraryAnswer);
        verify(scoreboard, times(1)).increaseScore(roomHost, arbitraryAnswer.length());
        verify(answerHandler, times(1))
                .notifyPlayersWhenAnswerValid(currentRound, room, scoreboard, roomHost, arbitraryAnswer);
    }
    
    @Test
    void handleNewAnswerRunsCorrectlyForInvalidAnswer() {
        var arbitraryAnswer = "cat";
        var mediatorWithNullRound = new Mediator(room, wordRepository, restartVote, scoreboard, answerHandler, null);
        assertThrows(InvalidAnswer.class,
                     () -> mediatorWithNullRound.handleNewAnswer(roomHost, arbitraryAnswer));
        
        var invalidAnswerException = new InvalidAnswer("ini untuk testing");
        doThrow(invalidAnswerException)
                .when(answerHandler)
                .throwIfAnswerNotValid(wordRepository, currentRound, roomHost, arbitraryAnswer);
        
        mediator.handleNewAnswer(roomHost, arbitraryAnswer);
        verify(messageSender, times(1))
                .send(roomHost, "invalid-answer", invalidAnswerException.getMessage());
    }
    
    @Test
    void initializeNewRound() {
        var roundDurationInMinute = 3;
        var minimumNumberOfWords = 5;
        var numberOfLetters = 9;
        var randomLetters = new SortedChars("abcefghhit");
        var arbitraryRound = new Round(room, new Date(), minimumNumberOfWords, randomLetters);
        CompletableFuture<ISortedChars>
                completableFutureRandomLetter = CompletableFuture.supplyAsync(() ->randomLetters);
        
        
        var anotherAnswerHandlerMock = Mockito.mock(IGameModeAnswerHandlingStrategy.class);
        var roundFactoryMock = Mockito.mock(IRoundFactory.class);
        var letterRandomizer = Mockito.mock(IRandomLetterGeneratorSingleton.class);
        when(letterRandomizer.getRandomLetter(minimumNumberOfWords, numberOfLetters))
                .thenReturn(completableFutureRandomLetter);
        when(roundFactoryMock.initiateRound(room, roundDurationInMinute,
                                            minimumNumberOfWords, randomLetters))
                .thenReturn(arbitraryRound);
        when(wordRepository.getWordsFromGivenLetters(randomLetters))
                .thenReturn(List.of("high", "hit", "hat", "chef", "bath", "randomWord"));  // arbitrary words
        var wordLengthToNumberOfWordsMapping = Map.of(
                3, 2,
                4, 3,
                10, 1
        );
        
        
        mediator.setLetterRandomizer(letterRandomizer);
        var future = mediator.initializeNewRound(roundDurationInMinute, minimumNumberOfWords,
                                                 roundFactoryMock, anotherAnswerHandlerMock);
        future.join();
        
        verify(letterRandomizer, times(1)).getRandomLetter(minimumNumberOfWords, numberOfLetters);
        verify(roundFactoryMock, times(1))
                .initiateRound(room, roundDurationInMinute, minimumNumberOfWords, randomLetters);
        assertSame(arbitraryRound, mediator.getCurrentRound());
        assertEquals(1, mediator.restartVote.getRequiredNumberOfVotes());
        
        var startNewRoundDataArgCaptor = ArgumentCaptor.forClass(StartNewRoundDto.class);
        verify(messageSender, times(1))
                .send(eq(room.getMembers()),
                      eq("start-new-round"),
                      startNewRoundDataArgCaptor.capture());
        
        var startNewRoundDto = startNewRoundDataArgCaptor.getValue();
        assertEquals(roundDurationInMinute*60, startNewRoundDto.getTimeLimit());
        assertEquals(randomLetters.getSortedString(),
                     startNewRoundDto.getRandomLetters());
        assertEquals(wordLengthToNumberOfWordsMapping,
                     startNewRoundDto.getWordLengthToNumberOfWordsMapping());
    }
    
}