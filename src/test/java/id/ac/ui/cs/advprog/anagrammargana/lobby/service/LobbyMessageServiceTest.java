package id.ac.ui.cs.advprog.anagrammargana.lobby.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.WebsocketPlayerAuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class LobbyMessageServiceTest {
    
    @MockBean
    IRoomRepository roomRepository;
    
    @MockBean
    IPlayerRepository playerRepository;
    
    @MockBean
    WebsocketPlayerAuthenticationService websocketPlayerAuthenticationService;
    
    @InjectMocks
    LobbyMessageService lobbyMessageService;
    
    
    String roomCode = "1234";
    IRoom room;
    IPlayer host;
    IPlayer player1;
    IPlayer player2;
    
    String hostName = "I am host";
    String playerName1 = "Nirmala";
    String playerName2 = "Gecko";
    String hostSessionToken = "asdfghjkl";
    String sessionToken1 = "qwertyuiop";
    String sessionToken2 = "zxcvbnm";
    
    List<IPlayer> members;
    
    @BeforeEach
    void setup(){
        host = new Player(hostName, 100, hostSessionToken);
        player1 = new Player(playerName1, 101, sessionToken1);
        player2 = new Player(playerName2, 102, sessionToken2);
        
        room = new Room(host, roomCode);
        room.addMember(player1);
        room.addMember(player2);
        members = List.of(host, player1, player2);
    
        
        when(roomRepository.findByRoomCodeOrNull(roomCode)).thenReturn(Optional.of(room));
        when(roomRepository.findByRoomCodeOrException(roomCode)).thenReturn(room);
        for (var member: members) {
            when(playerRepository.findByIdOrNull(member.getId())).thenReturn(Optional.of(member));
        }
    
        doNothing().when(websocketPlayerAuthenticationService).throwIfNotAuthenticated(any(MessageData.class));
        MockitoAnnotations.openMocks(this);
    }
    
    // positive test
    @Test
    void refreshRoomMember_returnsCorrectAnswer() {
        refreshRoomMember_parameterized("refresh-room-member", "");
    }
    
    
    // negative test
    @Test
    void refreshRoomMember_doNotCareMessageBody() {
        refreshRoomMember_parameterized("refresh-room-member", null);
    }
    
    // edge case
    @Test
    void refreshRoomMember_doNotCareMessageType() {
        refreshRoomMember_parameterized("asdfghjkl", "");
    }
    
    
    <T> void refreshRoomMember_parameterized(String type, T body){
        var senderId = player1.getId();
        var senderRoomCode = roomCode;
        var senderSession = sessionToken2;
    
        var messageData = new MessageData<Object>(senderId, senderRoomCode, senderSession, type, body);
    
        var result = lobbyMessageService.refreshRoomMember(messageData);
        assertEquals(senderId, result.getPlayerId());
        assertEquals(senderSession, result.getSessionToken());
        assertEquals(senderRoomCode, result.getRoomCode());
        assertEquals("update-room-member", result.getType());
    
        var returnedMsgBody = result.getBody();
        assertEquals(members.size(), returnedMsgBody.size());
        for (var member: members) {
            assertTrue(members.contains(member));
        }
    }
    
    
    
    // edge case
    @Test
    void refreshRoomMember_doNotCatchExceptions() {
        var exceptions = List.of(
                InvalidPlayerIdException.class, InvalidSessionTokenException.class,
                InvalidRoomCodeException.class
        );
    
        var senderId = player1.getId();
        var senderRoomCode = roomCode;
        var senderSession = sessionToken2;
        var messageData = new MessageData<Object>(senderId, senderRoomCode, senderSession,
                                                  "refresh-room-member", "");
        
        for (var exception: exceptions) {
            doThrow(exception)
                    .when(websocketPlayerAuthenticationService)
                    .throwIfNotAuthenticated(any(MessageData.class));
    
            assertThrows(exception, () -> {
                lobbyMessageService.refreshRoomMember(messageData);
            });
        }
    }
    
    @Test
    void startGame() {
        var roomCode = "ThisIsRoom";
        var messageType = "start-first-round";
        
        var host = new Player("host", 123, "");
        var player = new Player("player1", 124, "");
        var room = new Room(host, roomCode);
        room.addMember(player);
        
        var mockedMessageSender = mock(MessageSender.class);
        var messageSenderFactory = mock(MessageSenderFactory.class);
        
        when(messageSenderFactory.create()).thenReturn(mockedMessageSender);
        when(mockedMessageSender.send(any(Player.class), eq(messageType), anyString())).thenReturn(null);
        when(mockedMessageSender.send(any(Collection.class), anyString(), any())).thenCallRealMethod();
        
        lobbyMessageService.setMessageSenderFactory(messageSenderFactory);
        when(roomRepository.findByRoomCodeOrNull(roomCode)).thenReturn(Optional.of(room));
        when(roomRepository.findByRoomCodeOrException(roomCode)).thenReturn(room);
        
        lobbyMessageService.startGame(roomCode);
        verify(mockedMessageSender, times(1))
                .send(eq(player), eq(messageType), anyString());
        verify(mockedMessageSender, times(1))
                .send(eq(host), eq(messageType), anyString());
    }
}