package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.IScoreboard;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.Scoreboard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ScoreboardTest {

    String roomCode = "84321";
    final static IPlayer roomHost = new Player("I am host", 123L, "SESSION TOKEN");
    Room room;
    Map<IPlayer, Integer> playersAndScores = new LinkedHashMap<>();
    int roomHostScore = 0;
    IScoreboard scoreboard1;


    @BeforeEach
    public void setup(){
        room = new Room(roomHost, roomCode);
        scoreboard1 = new Scoreboard();
        for (var member: room.getMembers()) {
            scoreboard1.registerPlayer(member);
        }

        playersAndScores.put(roomHost, roomHostScore);
    }

    @Test
    void getPlayerByRankWorkProperly() {
        assertEquals(roomHost, scoreboard1.getPlayerByRank(1));
    }

    @Test
    void getScoreByRankWorkProperly() {
        assertEquals(roomHostScore, scoreboard1.getScoreByRank(1));

    }



    @Test
    void getScoreWorkProperly() {
        assertEquals(roomHostScore, scoreboard1.getScore(roomHost));
        assertNotEquals(5, scoreboard1.getScore(new Player("IDK", 456L, "SESSION TOKEN")));
    }

    @Test
    void increaseScoreWorkProperly() {
        scoreboard1.increaseScore(roomHost, 5);
        assertEquals(5, scoreboard1.getScore(roomHost));
        scoreboard1.increaseScore(roomHost, 7);
        assertEquals(5+7, scoreboard1.getScore(roomHost));
    }

    @Test
    void increaseScoreWorkProperlyEvenForUnregisteredPlayer() {
        var unknownPlayer = new Player("who am I", 124, "");
        scoreboard1.increaseScore(unknownPlayer, 2);
        assertEquals(2, scoreboard1.getScore(unknownPlayer));
    }
    
    @Test
    void asHashmap() {
        var hashmap = scoreboard1.asHashmap();
        assertEquals(1, hashmap.size());
        assertEquals(0, hashmap.getOrDefault(roomHost, -1));
    
        var unknownPlayer = new Player("who am I", 124, "");
        scoreboard1.increaseScore(unknownPlayer, 2);
        scoreboard1.increaseScore(roomHost, 3);
        hashmap = scoreboard1.asHashmap();
        assertEquals(2, hashmap.size());
        assertEquals(3, hashmap.getOrDefault(roomHost, -1));
        assertEquals(2, hashmap.getOrDefault(unknownPlayer, -1));
    }
}
