package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.in.game.model.GameMode;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(MockitoExtension.class)
class AnswerHandlingFactoryTest {

    GameMode gameModeFFA;
    GameMode gameModeAA;
    FreeForAllAnswerHandlingStrategy freeForAllAnswerHandlingStrategy;
    AllAloneAnswerHandlingStrategy allAloneAnswerHandlingStrategy;

    @InjectMocks
    AnswerHandlingFactory answerHandlingFactory;

    @BeforeEach
    void setUp(){
        gameModeFFA = GameMode.FREE_FOR_ALL;
        gameModeAA = GameMode.ALL_ALONE;
        freeForAllAnswerHandlingStrategy = new FreeForAllAnswerHandlingStrategy();
        allAloneAnswerHandlingStrategy = new AllAloneAnswerHandlingStrategy();
    }

    @Test
    void createAnswerHandlingStrategyTest(){
        var initiateAnswerHandlingStrategy = answerHandlingFactory.createAnswerHandlingStrategy(gameModeFFA);
        var anotherAnswerHandlingStrategy = answerHandlingFactory.createAnswerHandlingStrategy(gameModeAA);
        assertEquals(freeForAllAnswerHandlingStrategy.getClass(), initiateAnswerHandlingStrategy.getClass());
        assertEquals(allAloneAnswerHandlingStrategy.getClass(), anotherAnswerHandlingStrategy.getClass());

    }
}
