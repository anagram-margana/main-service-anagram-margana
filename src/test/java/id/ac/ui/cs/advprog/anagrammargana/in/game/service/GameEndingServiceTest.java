package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.*;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class GameEndingServiceTest {

    IRoom room;
    IPlayer playerHost;
    IMessageSender messageSender;

    IRoomRepository roomRepository;
    IMediatorRepository mediatorRepository;
    IPlayerRepository playerRepository;
    IMessageSenderFactory messageSenderFactory;

    @InjectMocks
    GameEndingService gameEndingService;

    @BeforeEach
    void setUp(){
        roomRepository = Mockito.mock(RoomRepository.class);
        mediatorRepository = Mockito.mock(MediatorRepository.class);
        playerRepository = Mockito.mock(PlayerRepository.class);

        messageSenderFactory = Mockito.mock(MessageSenderFactory.class);
        messageSender = Mockito.mock(MessageSender.class);

        playerHost = new Player("Tantra", 123, "ABC");
        room = new Room(playerHost, "MyRoom");

        MockitoAnnotations.openMocks(this);
        when(roomRepository.findByRoomCodeOrException("MyRoom")).thenReturn(room);
        when(messageSenderFactory.create()).thenReturn(messageSender);
    }

    @Test
    void finishTheGameTest(){
        gameEndingService.setMessageSenderFactory(messageSenderFactory);
        gameEndingService.finishTheGame("MyRoom");
        verify(playerRepository, times(1)).removePlayerWithId(123);
        verify(mediatorRepository, times(1)).removeMediatorByRoomCode("MyRoom");
        verify(roomRepository, times(1)).removeRoomByRoomCode("MyRoom");
        verify(messageSender, times(1)).send(any(Collection.class), any(String.class), any(String.class));
    }
}
