package id.ac.ui.cs.advprog.anagrammargana.connection.status.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.connection.status.service.PlayerConnectionStatusService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {PlayerConnectionStatusController.class})
class PlayerConnectionStatusControllerTest {
    @MockBean
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @MockBean
    PlayerConnectionStatusService playerConnectionStatusService;
    
    
    @Autowired
    MockMvc mockMvc;
    
    
    long playerId = 123;
    String roomCode = "MyRoom";
    String sessionToken = "asdfghjkl";
    MessageData<String> messageData;
    
    ObjectMapper objectMapper = new ObjectMapper();
    
    @BeforeEach
    void setUp() {
        messageData = new MessageData<>(playerId, roomCode, sessionToken, null, "some answer");
        doNothing().when(playerAuthenticationService).throwIfNotAuthenticated(messageData);
    }
    
    @Test
    @SneakyThrows
    void someoneDisconnected(){
        var result =
                mockMvc.perform(MockMvcRequestBuilders.post("/communication-api/msg-from-client/disconnected")
                                        .content(objectMapper.writeValueAsString(messageData))
                                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();
        assertEquals("", result.getResponse().getContentAsString());
        
        verify(playerAuthenticationService, times(1)).throwIfNotAuthenticated(messageData);
        verify(playerConnectionStatusService, times(1))
                .handleSomeoneDisconnected(roomCode, playerId);
    }
    
    @Test
    @SneakyThrows
    void someoneConnected(){
        var result =
                mockMvc.perform(MockMvcRequestBuilders.post("/communication-api/msg-from-client/connected")
                                        .content(objectMapper.writeValueAsString(messageData))
                                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();
        assertEquals("", result.getResponse().getContentAsString());
        
        verify(playerAuthenticationService, times(1)).throwIfNotAuthenticated(messageData);
        verify(playerConnectionStatusService, times(1))
                .handleSomeoneConnected(roomCode, playerId);
    }
}