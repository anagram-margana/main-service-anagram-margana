package id.ac.ui.cs.advprog.anagrammargana.common.create.join.random;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.PlayerRandomUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class PlayerRandomUtilTest {
    @Mock
    IPlayerRepository playerRepository;
    
    @InjectMocks
    PlayerRandomUtil playerRandomUtil;
    
    @BeforeEach
    public void createMocks() {
        playerRandomUtil = Mockito.mock(PlayerRandomUtil.class, Mockito.CALLS_REAL_METHODS);
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    void randomPlayerSessionTokenGivesRandomAnswer() {
        for (int i = 0; i < 10; i++) {
            assertNotEquals(playerRandomUtil.getRandomPlayerSessionToken(), playerRandomUtil.getRandomPlayerSessionToken());
        }
    }
    
    
    @Test
    void randomPlayerSessionTokenIsAlphaNumeric() {
        for (int i = 0; i < 10; i++) {
            var randomSessionToken = playerRandomUtil.getRandomPlayerSessionToken();
            assertTrue(StringUtils.isAlphanumeric(randomSessionToken));
        }
    }
    
    
    @Test
    void randomPlayerIdGivesRandomAnswerWithinJavascriptConstraint() {
        when(playerRepository.hasPlayerId(anyLong())).thenReturn(false);
        
        var javascriptMaximumIntegerBit = 53;
        var javascriptMaximumIntegerValue = 2L << javascriptMaximumIntegerBit;

        for (int i = 0; i < 10; i++) {
            var random1 = playerRandomUtil.getRandomPlayerId();
            var random2 = playerRandomUtil.getRandomPlayerId();
            
            assertNotEquals(random1, random2);
            assertTrue(random1 <= javascriptMaximumIntegerValue);
            assertTrue(random2 <= javascriptMaximumIntegerValue);
        }
        verify(playerRandomUtil, times(20)).getRandomPlayerId(any(Random.class));
    }
    
    
    @Test
    void randomPlayerIdWontReturnTakenId() {
        Random random = mock(Random.class);
    
        doReturn(10L)
                .doReturn(11L)
                .doReturn(100L)
                        .when(random).nextLong();
        
        when(playerRepository.hasPlayerId(10L)).thenReturn(true);
        when(playerRepository.hasPlayerId(11L)).thenReturn(true);
        when(playerRepository.hasPlayerId(100L)).thenReturn(false);
        
        var result = playerRandomUtil.getRandomPlayerId(random);
        assertEquals(100, result);
    }
}