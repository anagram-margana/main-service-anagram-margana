package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.util.DateTimeUtil;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class RoundTest {

    Room room;
    String roomCode = "84321";
    final static IPlayer roomHost = new Player("I am a host", 123L, "SESSION TOKEN");
    Date endTime;
    int minimumWord = 1;
    String word = "COASTER";
    IRound round1;
    ISortedChars sortedChars;
    WordPool wordPool;

    @BeforeEach
    public void setup(){
        endTime = DateTimeUtil.asDate(2022, 5, 21, 9, 3, 30);
        
        sortedChars = new SortedChars(word);
        room = new Room(roomHost, roomCode);
        round1 = new Round(room, endTime, minimumWord, sortedChars);
        wordPool = new WordPool();
        wordPool.addWord("COASTER");
    }

    @Test
    void getRoundEndDateWorkProperly() {
        assertEquals(endTime, round1.getRoundEndTime());
    }

    @Test
    void getMinimumWordWorkProperly() {
        assertEquals(minimumWord, round1.getMinimumWord());
    }
    

    @Test
    void getAvailableLettersWorkProperly() {
        assertEquals(sortedChars, round1.getRandomLetters());
    }

    @Test
    void stopRoundWorksProperly() {
        assertEquals(endTime, round1.getRoundEndTime());
    
        var now = new Date();
        var endTime = DateTimeUtil.getDateFromNow(5, TimeUnit.MINUTES);
        round1 = new Round(room, endTime, minimumWord, sortedChars);
        assertTrue(round1.getRoundEndTime().getTime() >= now.getTime());
        
        round1.stopRound();
        now = new Date();
        var newEndTime = round1.getRoundEndTime();
        assertTrue(now.getTime() >= newEndTime.getTime());
    }

    @Test
    void getWordPoolWorkProperly() {
        round1.setWordPool(roomHost, wordPool);
        WordPool falseWordPool = new WordPool();
        assertEquals(wordPool, round1.getWordPool(roomHost));
        assertNotEquals(falseWordPool, round1.getWordPool(roomHost));
    }

    @Test
    void setWordPoolWorkProperly() {
        round1.setWordPool(roomHost, wordPool);
        Player falsePlayer = new Player("name", 456L, "SESSION TOKEN");
        round1.setWordPool(falsePlayer, wordPool);
        assertEquals(wordPool, round1.getWordPool(roomHost));
        assertNotEquals(wordPool, round1.getWordPool(falsePlayer));
    }

    @Test
    void testGetWordPools() {
        IPlayer newRoomHost = new Player("I am a host2", 124L, "SESSION TOKEN");
        String newRoomCode = "12345";
        Date newEndTime = DateTimeUtil.asDate(2023, 5, 21, 9, 3, 30);
        ISortedChars newSortedChars = new SortedChars("WORDS");
        Room newRoom = new Room(newRoomHost, newRoomCode);
        IRound round2 = new Round(newRoom, newEndTime, 2, newSortedChars);

        // masih equals karena keduanya sama-sama mengembalikan {}
        assertEquals(round1.getWordPools(), round2.getWordPools());

        assertEquals(round1.getWordPools(), round1.getWordPools());
    }

}
