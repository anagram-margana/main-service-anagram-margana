package id.ac.ui.cs.advprog.anagrammargana.common.create.join.controller;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomClosedException;
import id.ac.ui.cs.advprog.anagrammargana.create.join.controller.CreateAndJoinRoomController;
import id.ac.ui.cs.advprog.anagrammargana.create.join.service.ICreateJoinService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = CreateAndJoinRoomController.class)
class CreateAndJoinRoomControllerTest {
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    ICreateJoinService service;
    
    String playerName = "Heizki";
    long playerId = 105L;
    String roomCode = "73286";
    IPlayer player;
    IRoom room;
    
    @BeforeEach
    void setup(){
        player = new Player(playerName, playerId, "");
        room = new Room(player, roomCode);
        
        when(service.getRoomCodeCookie(anyString())).thenReturn(new Cookie("playerId", "123"));
        when(service.getPlayerIdCookie(anyLong())).thenReturn(new Cookie("roomCode", "myRoom"));
        when(service.getPlayerNameCookie(anyString())).thenReturn(new Cookie("playerName", "Heizki"));
        when(service.getSessionTokenCookie(anyString())).thenReturn(new Cookie("sessionToken", "asdfghjkl"));
    }
    
    @Test
    void mainPage() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk());
    }
    
    @Test
    void joinAsGuestPage() throws Exception {
        mockMvc.perform(get("/join"))
                .andExpect(status().isOk());
    }
    
    @Test
    void registerGuest() throws Exception {
        when(service.createGuest(playerName, roomCode)).thenReturn(player);
        when(service.createGuest(playerName, "694205")).thenThrow(InvalidRoomCodeException.class);
        when(service.createGuest(playerName, "81201")).thenThrow(RoomClosedException.class);
        mockMvc.perform(
                            post("/join")
                                    .param("playerName", playerName)
                                    .param("roomCode", roomCode)
                        ).andExpect(status().is3xxRedirection());
        verify(service, atLeast(1)).createGuest(playerName, roomCode);
        mockMvc.perform(
                post("/join")
                        .param("playerName", playerName)
                        .param("roomCode", "694205")
        ).andExpect(redirectedUrl("/join/?room=694205"));
        mockMvc.perform(
                post("/join")
                        .param("playerName", playerName)
                        .param("roomCode", "81201")
        ).andExpect(redirectedUrl("/join/?room=81201"));
        verify(service, atLeast(1)).createGuest(playerName,"81201");

    }
    
    @Test
    void registerHost() throws Exception{
        when(service.createHost(playerName)).thenReturn(room);
        
        mockMvc.perform(
                            post("/create")
                                    .param("playerName", "Heizki")
                        ).andExpect(status().is3xxRedirection());
        verify(service, atLeast(1)).createHost(playerName);
    }
}