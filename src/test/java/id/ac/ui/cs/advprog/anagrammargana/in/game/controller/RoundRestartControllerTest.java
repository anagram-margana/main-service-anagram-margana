package id.ac.ui.cs.advprog.anagrammargana.in.game.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.service.IRoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.in.game.service.RoundRestartService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.IWebsocketPlayerAuthenticationService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {RoundRestartController.class})
class RoundRestartControllerTest {
    @MockBean
    IWebsocketPlayerAuthenticationService playerAuthenticationService;
    
    @MockBean
    IRoomHostValidatorService roomHostValidatorService;
    
    @MockBean
    RoundRestartService roundRestartService;
    
    
    @Autowired
    MockMvc mockMvc;
    
    
    long playerId = 123;
    String roomCode = "MyRoom";
    String sessionToken = "asdfghjkl";
    MessageData<String> messageData;
    
    ObjectMapper objectMapper = new ObjectMapper();
    
    @BeforeEach
    void setUp() {
        messageData = new MessageData<>(playerId, roomCode, sessionToken, null, "");
        doNothing().when(playerAuthenticationService).throwIfNotAuthenticated(messageData);
        doNothing().when(roomHostValidatorService).throwIfNotRoomHost(messageData);
    }
    
    @Test
    @SneakyThrows
    void getInput_replyVoteAcceptedMessageIfSuccess() {
        doReturn(true).when(roundRestartService)
                .handleSomeoneVotedForRestart(roomCode, playerId);
        
        var result =
        mockMvc.perform(MockMvcRequestBuilders.post("/communication-api/msg-from-client/vote-restart")
                                .content(objectMapper.writeValueAsString(messageData))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        
        
        var response = result.getResponse();
        var responseBodyString = response.getContentAsString();
        var responseBody = MessageData.fromJson(responseBodyString, Object.class);
    
        verify(playerAuthenticationService, times(1)).throwIfNotAuthenticated(messageData);
        assertEquals("vote-accepted", responseBody.getType());
        
        messageData.setType("vote-accepted");
        messageData.setBody(null);  // do not care about its body
        responseBody.setBody(null);
        assertEquals(messageData, responseBody);
    }
    
    
    @Test
    @SneakyThrows
    void getInput_replyEmptyBodyIfFail() {
        doReturn(false).when(roundRestartService)
                .handleSomeoneVotedForRestart(roomCode, playerId);
        
        var result =
                mockMvc.perform(MockMvcRequestBuilders.post("/communication-api/msg-from-client/vote-restart")
                                        .content(objectMapper.writeValueAsString(messageData))
                                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();
        
        var response = result.getResponse();
        var responseBodyString = response.getContentAsString();
        assertEquals("", responseBodyString);
        verify(playerAuthenticationService, times(1)).throwIfNotAuthenticated(messageData);
    }
    
    @Test
    @SneakyThrows
    void roundRestart() {
        doNothing().when(roomHostValidatorService).throwIfNotRoomHost(messageData);
        
        
        doReturn(true).when(roundRestartService)
                .handleSomeoneVotedForRestart(roomCode, playerId);

        mockMvc.perform(MockMvcRequestBuilders.post("/communication-api/msg-from-client/restart-round")
                                .content(objectMapper.writeValueAsString(messageData))
                                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    
        verify(playerAuthenticationService, times(1)).throwIfNotAuthenticated(messageData);
        verify(roomHostValidatorService, times(1)).throwIfNotRoomHost(messageData);
        verify(roundRestartService, times(1)).restartRound(roomCode);
    }
}