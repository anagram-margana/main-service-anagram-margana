package id.ac.ui.cs.advprog.anagrammargana.common.create.join.random;

import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.RoomRandomUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class RoomRandomUtilTest {
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    IRoomRepository roomRepository;
    
    @InjectMocks
    RoomRandomUtil roomRandomUtil;
    
    @BeforeEach
    void setup(){
        roomRandomUtil = Mockito.mock(RoomRandomUtil.class, Mockito.withSettings()
                .useConstructor()
                .defaultAnswer(Answers.CALLS_REAL_METHODS));
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    void getRandomRoomCodeWillReturnRandomly() {
        when(roomRepository.hasRoomCode(anyString())).thenReturn(false);
        
        for (int i = 0; i < 2; i++) {
            Assertions.assertNotEquals(roomRandomUtil.getRandomRoomCode(),
                                       roomRandomUtil.getRandomRoomCode());
        }
    }
    
    
    @Test
    void getRandomRoomCodeWillNotReturnExistingCode() {
        when(roomRepository.hasRoomCode(anyString()))
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(false);
        final var banyaknyaThenReturn = 4;
    
        roomRandomUtil.getRandomRoomCode();
        
        verify(roomRepository, atLeast(banyaknyaThenReturn)).hasRoomCode(anyString());
    }
    
}