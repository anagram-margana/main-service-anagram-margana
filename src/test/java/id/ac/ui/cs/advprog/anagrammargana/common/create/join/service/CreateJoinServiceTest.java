package id.ac.ui.cs.advprog.anagrammargana.common.create.join.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.Mediator;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IPlayerRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IRoomRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IWordRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.IMessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSender;
import id.ac.ui.cs.advprog.anagrammargana.common.core.websocket.MessageSenderFactory;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.PlayerRandomUtil;
import id.ac.ui.cs.advprog.anagrammargana.create.join.random.RoomRandomUtil;
import id.ac.ui.cs.advprog.anagrammargana.create.join.service.CreateJoinService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CreateJoinServiceTest {
    
    @InjectMocks
    PlayerRandomUtil playerRandomUtil;
    
    @InjectMocks
    RoomRandomUtil roomRandomUtil;
    
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    IPlayerRepository playerRepository;
    
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    IRoomRepository roomRepository;
    
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    IMediatorRepository mediatorRepository;
    
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    IWordRepository wordRepository;
    
    @InjectMocks
    CreateJoinService service;
    
    IMessageSender messageSender;
    IMessageSenderFactory messageSenderFactory;
    
    
    @BeforeEach
    void setup(){
        var mockedMessageSender = Mockito.mock(MessageSender.class, Answers.RETURNS_SMART_NULLS);
        var mockedFactory = mock(MessageSenderFactory.class);
        when(mockedFactory.create()).thenReturn(mockedMessageSender);
        
        playerRandomUtil = Mockito.mock(PlayerRandomUtil.class, CALLS_REAL_METHODS);
        roomRandomUtil = Mockito.mock(RoomRandomUtil.class, CALLS_REAL_METHODS);
        service = Mockito.mock(CreateJoinService.class, Mockito.CALLS_REAL_METHODS);
        service.setMessageSenderFactory(mockedFactory);
        MockitoAnnotations.openMocks(this);
        
        messageSender = Mockito.mock(MessageSender.class);
        messageSenderFactory = Mockito.mock(IMessageSenderFactory.class);
        service.setMessageSenderFactory(messageSenderFactory);
        when(messageSenderFactory.create()).thenReturn(messageSender);
    }
    
    
    @Test
    void createHost() {
        var hostName = "My name";
        var roomObj = service.createHost(hostName);
        var playerAsRoomHost = roomObj.getRoomHost();
        
        assertEquals(hostName, playerAsRoomHost.getName());
        verify(playerRepository, times(1)).add(playerAsRoomHost);
        verify(roomRepository, times(1)).add(roomObj);
    }
    
    @SuppressWarnings("unchecked")
    @Test
    void createGuest() {
        doNothing().when(mediatorRepository).add(any(Mediator.class));
        doNothing().when(roomRepository).add(any(Room.class));
        
        var room = service.createHost("IAmHost");
        var mediator = new Mediator(room, wordRepository);
        var roomCode = room.getCode();
    
        when(mediatorRepository.findByRoomCodeOrNull(roomCode)).thenReturn(Optional.of(mediator));
        when(roomRepository.findByRoomCodeOrException(roomCode)).thenReturn(room);
        clearInvocations(playerRepository);
        
        var newGuestName = "IAmGuest";
        var result = service.createGuest(newGuestName, roomCode);
        
        assertEquals(newGuestName, result.getName());
        assertTrue(room.hasMemberWithId(result.getId()));
        verify(playerRepository, times(1)).add(result);
    
        
        verify(messageSender, times(1))
                .send(room.getMembers(), "update-room-member", room.getMemberNames());
        
    }
    
    @Test
    void createGuest_throwIfInvalidRoomCode() {
        doNothing().when(mediatorRepository).add(any(Mediator.class));
        doNothing().when(roomRepository).add(any(Room.class));
        
        var roomCode = "IAmHost";
        
        when(mediatorRepository.findByRoomCodeOrException(roomCode)).thenThrow(InvalidRoomCodeException.class);
        when(roomRepository.findByRoomCodeOrException(roomCode)).thenThrow(InvalidRoomCodeException.class);
        clearInvocations(playerRepository);
        
        var newGuestName = "IAmGuest";
        
        assertThrows(InvalidRoomCodeException.class, () -> {
            service.createGuest(newGuestName, roomCode);
        });
    }
    
    @Test
    void getPlayerIdCookie() {
        var playerId = 123;
        var cookieValue = service.getPlayerIdCookie(playerId).getValue();
        assertEquals(Long.toString(playerId), cookieValue);
    }
    
    @Test
    void getPlayerNameCookie() {
        var playerName = "Yolanda Suwasti";
        var expected = URLEncoder.encode(playerName, StandardCharsets.UTF_8);
        expected = expected.replace("+", "%20");
        var cookieValue = service.getPlayerNameCookie(playerName).getValue();
        
        assertEquals(expected, cookieValue);
    }
    
    @Test
    void getRoomCodeCookie() {
        var roomCode = "myRoom";
        var cookieValue = service.getRoomCodeCookie(roomCode).getValue();
        assertEquals(roomCode, cookieValue);
    }
    
    @Test
    void getSessionTokenCookie() {
        var sessionToken = "asdfghjkl";
        var cookieValue = service.getSessionTokenCookie(sessionToken).getValue();
        assertEquals(sessionToken, cookieValue);
    }
}