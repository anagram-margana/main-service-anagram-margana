package id.ac.ui.cs.advprog.anagrammargana.common.core;

import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.PlayerIdAlreadyExistException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomClosedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


class RoomTest {
    
    IRoom room;
    String roomCode = "84321";
    IPlayer roomHost;
    List<IPlayer> roomMembers;
    
    @BeforeEach
    void setup(){
        roomHost = new Player("I am host", 123L, "SESSION TOKEN");
        room = new Room(roomHost, roomCode);
        
        roomMembers = new ArrayList<>();
        roomMembers.add(new Player("Member 1", 124L, "SESSION TOKEN"));
        roomMembers.add(new Player("Member 2", 125L, "SESSION TOKEN"));
    
        for (IPlayer member: roomMembers) {
            room.addMember(member);
        }
    }
    
    @Test
    void getMemberSize() {
        var numberOfRoomMemberIncludingHost = roomMembers.size() + 1;
        assertEquals(numberOfRoomMemberIncludingHost,
                     room.getMemberSize());
    }
    
    @Test
    void getCode() {
        assertEquals(roomCode, room.getCode());
    }
    
    @Test
    void getRoomHost() {
        assertSame(roomHost, room.getRoomHost());
    }
    
    @Test
    void getMembers() {
        Comparator<? super IPlayer>
                playerComparator = Comparator.comparingLong(IPlayer::getId);
        var roomMembers = new TreeSet<IPlayer>(playerComparator);
        roomMembers.addAll(room.getMembers());
        
        var actualRoomMembers = new TreeSet<IPlayer>(playerComparator);
        actualRoomMembers.addAll(roomMembers);
        actualRoomMembers.add(roomHost);
        
        assertEquals(actualRoomMembers, roomMembers);
    }
    
    @Test
    void getMembersAsName() {
        var roomMemberNames = new HashSet<>(room.getMemberNames());
        var actualRoomMemberNames = new HashSet<String>();
    
        for (IPlayer member: roomMembers) {
            actualRoomMemberNames.add(member.getName());
        }
        actualRoomMemberNames.add(roomHost.getName());

        assertEquals(actualRoomMemberNames, roomMemberNames);
    }
    
    @Test
    void hasMemberWithId() {
        var roomHostId = roomHost.getId();
        var roomMemberId = roomMembers.get(0).getId();
        
        assertTrue(room.hasMemberWithId(roomHostId));
        assertTrue(room.hasMemberWithId(roomMemberId));
        assertFalse(room.hasMemberWithId(67890L));
    }
    
    @Test
    void addMember() {
        var newMemberId = 200L;
        var newValidMember = new Player("New member", newMemberId, "SESSION");
        
        assertFalse(room.hasMemberWithId(newMemberId));
        room.addMember(newValidMember);
        assertTrue(room.hasMemberWithId(newMemberId));
        
        var newInvalidMember = new Player("Invalid Member", newMemberId, "SESSION");
        assertThrows(PlayerIdAlreadyExistException.class,
                     () -> room.addMember(newInvalidMember));
    }
    
    @Test
    void addMemberThrowsIfRoomIsClosed() {
        var newMemberId = 200L;
        var newMember = new Player("New member", newMemberId, "SESSION");
        
        room.close();
        assertThrows(RoomClosedException.class,
                     () -> room.addMember(newMember));
    }
    
    @Test
    void removeMemberWithId() {
        var invalidMemberId = 200L;
        
        assertThrows(InvalidPlayerIdException.class,
                     () -> room.removeMemberWithId(invalidMemberId));
        
        var member = roomMembers.get(0);
        var memberId = member.getId();
        
        assertTrue(room.hasMemberWithId(memberId));
        room.removeMemberWithId(memberId);
        assertFalse(room.hasMemberWithId(memberId));
    }
    
}