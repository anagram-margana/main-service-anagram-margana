package id.ac.ui.cs.advprog.anagrammargana.in.game.util;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.Scoreboard;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.PlayerScoreData;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Comparator;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ScoreboardToListOfScoreDataUtilTest {
    
    @Test
    void getPlayerScoresAsList() {
        var player1 = new Player("abc", 123, "");
        var player2 = new Player("def", 124, "");
        
        var scoreboard = Mockito.mock(Scoreboard.class);
        Map<IPlayer, Integer> hashmap = Map.of(
                player1, 17,
                player2, 10
        );
        when(scoreboard.asHashmap()).thenReturn(hashmap);
        
        var result = ScoreboardToListOfScoreDataUtil.getPlayerScoresAsList(scoreboard);
        result.sort(Comparator.comparingLong(PlayerScoreData::getPlayerId));
        var result1 = result.get(0);
        var result2 = result.get(1);
        
        assertEquals(player1.getId(), result1.getPlayerId());
        assertEquals(player1.getName(), result1.getName());
        assertEquals(17, result1.getScore());
    
        assertEquals(player2.getId(), result2.getPlayerId());
        assertEquals(player2.getName(), result2.getName());
        assertEquals(10, result2.getScore());
    }
    
}