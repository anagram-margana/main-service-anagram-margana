package id.ac.ui.cs.advprog.anagrammargana.common.controller.advice;

import lombok.SneakyThrows;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class GlobalExceptionHandlerTest {
    
    HttpServletResponse httpServletResponse;
    
    @InjectMocks
    GlobalExceptionHandler globalExceptionHandler;
    
    Logger logger = Mockito.mock(Logger.class);
    
    @BeforeEach
    void setup(){
        httpServletResponse = Mockito.mock(HttpServletResponse.class);
        MockitoAnnotations.openMocks(this);
        globalExceptionHandler.setLogger(logger);
    }
    
    @Test
    @SneakyThrows
    void redirectToHome_redirectToHome() {
        globalExceptionHandler.invalidIdentityInformation(httpServletResponse);
        verify(httpServletResponse, times(1)).sendRedirect("/");
    }
    
    @Test
    @SneakyThrows
    void someoneWithoutCookieTryingToAccessLobbyOrInGame() {
        globalExceptionHandler.someoneWithoutCookieTryingToAccessLobbyOrInGame(httpServletResponse);
        verify(httpServletResponse, times(1)).sendRedirect("/");
    }
    
    @Test
    void logBadRequest(){
        var httpInputMessage = Mockito.mock(HttpInputMessage.class);
        var mockHttpMessage = new HttpMessageNotReadableException("", httpInputMessage);
        assertThrows(HttpMessageNotReadableException.class,
                     () -> globalExceptionHandler.logBadRequest(mockHttpMessage));
        verify(logger, times(1)).warn(anyString(), any(Throwable.class));
    }
}