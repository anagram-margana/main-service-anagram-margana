package id.ac.ui.cs.advprog.anagrammargana.in.game.service;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IMediator;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Mediator;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.IMediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.common.repository.MediatorRepository;
import id.ac.ui.cs.advprog.anagrammargana.in.game.core.*;
import id.ac.ui.cs.advprog.anagrammargana.in.game.exceptions.InvalidGameSettingsData;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.GameMode;
import id.ac.ui.cs.advprog.anagrammargana.in.game.model.dto.GameSettingsData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class StartNewRoundServiceTest {

    IMediator mediator;
    IGameModeAnswerHandlingStrategy gameModeAnswerHandlingStrategy;
    IRoundFactory roundFactory;
    GameSettingsData gameSettingsData;
    GameSettingsData gameSettingsDataExceptions1;
    GameSettingsData gameSettingsDataExceptions2;
    GameSettingsData gameSettingsDataExceptions3;

    IMediatorRepository mediatorRepository;
    IFactoryOfRoundFactory factoryOfRoundFactory;
    IAnswerHandlingFactory answerHandlingFactory;

    @InjectMocks
    StartNewRoundService startNewRoundService;

    @BeforeEach
    void setUp(){
        mediatorRepository = Mockito.mock(MediatorRepository.class);
        factoryOfRoundFactory = Mockito.mock(FactoryOfRoundFactory.class);
        answerHandlingFactory = Mockito.mock(AnswerHandlingFactory.class);
        mediator = Mockito.mock(Mediator.class);

        gameSettingsData =  new GameSettingsData(GameMode.ALL_ALONE, 3, 20);
        gameSettingsDataExceptions1 =  new GameSettingsData(GameMode.ALL_ALONE, 3, 100);
        gameSettingsDataExceptions2 =  new GameSettingsData(GameMode.ALL_ALONE, 6, 20);
        gameSettingsDataExceptions3 =  new GameSettingsData(GameMode.ALL_ALONE, 0, 20);

        gameModeAnswerHandlingStrategy = new AllAloneAnswerHandlingStrategy();
        roundFactory = new AllAloneRoundFactory();

        MockitoAnnotations.openMocks(this);
        when(mediatorRepository.findByRoomCodeOrException("MyRoom")).thenReturn(mediator);
        when(answerHandlingFactory.createAnswerHandlingStrategy(GameMode.ALL_ALONE)).thenReturn(gameModeAnswerHandlingStrategy);
        when(factoryOfRoundFactory.createRoundFactory(GameMode.ALL_ALONE)).thenReturn(roundFactory);


    }

    @Test
    void startNewRoundTest(){
        startNewRoundService.setFactoryOfRoundFactory(factoryOfRoundFactory);
        startNewRoundService.setAnswerHandlingFactory(answerHandlingFactory);
        startNewRoundService.startNewRound("MyRoom", gameSettingsData);
        assertThrows(InvalidGameSettingsData.class, ()->{
            startNewRoundService.startNewRound("MyRoom", gameSettingsDataExceptions1);
        });
        assertThrows(InvalidGameSettingsData.class, ()->{
            startNewRoundService.startNewRound("MyRoom", gameSettingsDataExceptions2);
        });
        assertThrows(InvalidGameSettingsData.class, ()->{
            startNewRoundService.startNewRound("MyRoom", gameSettingsDataExceptions3);
        });
        verify(mediator, times(1)).initializeNewRound(
                3,20,roundFactory,gameModeAnswerHandlingStrategy);
    }
}
