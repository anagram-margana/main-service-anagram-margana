package id.ac.ui.cs.advprog.anagrammargana.in.game.core;

import id.ac.ui.cs.advprog.anagrammargana.in.game.core.SortedChars;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortedCharsTest {
    String testShortRandomString;
    String sortedTestShortRandomString;
    SortedChars shortSortedChars;

    @BeforeEach
    void setUp() {
        testShortRandomString = "book";
        sortedTestShortRandomString = "bkoo";
        shortSortedChars = new SortedChars(testShortRandomString);
    }

    @Test
    void sortString() {
        assertEquals(sortedTestShortRandomString, shortSortedChars.sortString(testShortRandomString));
    }

    @Test
    void getSortedString() {
        assertEquals(sortedTestShortRandomString, shortSortedChars.getSortedString());
    }

    @Test
    void countChar() {
        assertEquals(2, shortSortedChars.countChar('o'));
    }

    @Test
    void isSubsequenceOfReturnsTrue() {
        assertTrue(shortSortedChars.isSubsequenceOf(new SortedChars("lsaoobmki")));
    }

    @Test
    void isSubsequenceOfReturnsFalse() {
        assertFalse(shortSortedChars.isSubsequenceOf(new SortedChars("lsaaobmki")));
    }

    
    @Test
    void testEqualsWhenObjectTypeIsString() {
        assertEqualsBothSide(shortSortedChars, "bkoo");
    }
    
    @Test
    void testEqualsWhenObjectAreNotEquals() {
        assertNotEquals(shortSortedChars, new SortedChars("abc"));
    }
    
    private void assertEqualsBothSide(Object o1, Object o2){
        if (o1 == null && o2 == null)
            return;
        if (o1 != null && o2 != null)
            assertTrue(o1.equals(o2) || o2.equals(o1));
        else
            fail();
    }
    
    
    @Test
    void testEqualsWhenObjectTypeIsSortedChars() {
        assertEquals(shortSortedChars, new SortedChars("boko"));
    }

    @Test
    void testEqualsWhenObjectTypeIsNotStringNorSortedChars() {
        reversedNotEquals(1234, shortSortedChars);
    }
    
    private void reversedNotEquals(Object o1, Object o2){
        if (o1 == null && o2 == null)
            fail();
        if (o1 != null && o2 != null)
            assertNotEquals(o2, o1);
    }

    @Test
    void testHashCode() {
        assertEquals(3025928, shortSortedChars.hashCode());
    }
}