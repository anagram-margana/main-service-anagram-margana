package id.ac.ui.cs.advprog.anagrammargana.common.repository;


import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.RoomCodeAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Answers.CALLS_REAL_METHODS;


class RoomRepositoryTest {
    IRoomRepository roomRepository;
    
    IRoom room;
    String roomCode = "87654";
    IPlayer roomHost;
    IPlayer roomMember;
    
    
    @BeforeEach
    public void setup(){
        roomRepository = Mockito.mock(RoomRepository.class, Mockito.withSettings()
                .useConstructor()
                .defaultAnswer(CALLS_REAL_METHODS)
        );
    
        roomHost = new Player("I am host", 123L, "");
        roomMember = new Player("I am normal member", 124L, "");
        room = new Room(roomHost, roomCode);
        room.addMember(roomMember);
        
        roomRepository.add(room);
    }
    
    
    @Test
    void doesRoomCodeExist() {
        var validRoomCode = room.getCode();
        var invalidRoomCode = "000000";
        
        assertTrue(roomRepository.hasRoomCode(validRoomCode));
        assertFalse(roomRepository.hasRoomCode(invalidRoomCode));
    }
    
    @Test
    void findByRoomCodeOrNull() {
        var validRoomCode = room.getCode();
        var invalidRoomCode = "000000";
    
        assertSame(room, roomRepository.findByRoomCodeOrException(validRoomCode));
        var nullableRoom = roomRepository.findByRoomCodeOrNull(invalidRoomCode);
        assertTrue(nullableRoom.isEmpty());
    }
    
    
    @Test
    void findByRoomCodeOrException() {
        var validRoomCode = room.getCode();
        var invalidRoomCode = "000000";
    
        assertSame(room, roomRepository.findByRoomCodeOrException(validRoomCode));
        assertThrows(InvalidRoomCodeException.class,
                     () -> roomRepository.findByRoomCodeOrException(invalidRoomCode));
    }
    
    @Test
    void add() {
        var newHost = new Player("I am a host of another room", 300L, "");
        var newRoomCode = "another room";
        var newRoom = new Room(newHost, newRoomCode);
        
        assertFalse(roomRepository.hasRoomCode(newRoomCode));
        roomRepository.add(newRoom);
        assertTrue(roomRepository.hasRoomCode(newRoomCode));
        assertSame(newRoom, roomRepository.findByRoomCodeOrException(newRoomCode));
        
        var anotherNewHost = new Player("Idk who I am", 400L, "");
        var roomWithInvalidRoomCode = new Room(anotherNewHost, newRoomCode);
        assertThrows(RoomCodeAlreadyExistException.class,
                     () -> roomRepository.add(roomWithInvalidRoomCode));
    }
    
    @Test
    void removeRoomByRoomCode() {
        var invalidRoomCode = "POIUYTREWQ";
        assertThrows(InvalidRoomCodeException.class,
                     () -> roomRepository.removeRoomByRoomCode(invalidRoomCode));
        
        assertTrue(roomRepository.hasRoomCode(roomCode));
        roomRepository.removeRoomByRoomCode(roomCode);
        assertFalse(roomRepository.hasRoomCode(roomCode));
    }
}