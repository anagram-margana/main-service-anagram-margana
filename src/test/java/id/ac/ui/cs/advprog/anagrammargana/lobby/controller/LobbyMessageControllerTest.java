package id.ac.ui.cs.advprog.anagrammargana.lobby.controller;

import id.ac.ui.cs.advprog.anagrammargana.common.core.IPlayer;
import id.ac.ui.cs.advprog.anagrammargana.common.core.IRoom;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Player;
import id.ac.ui.cs.advprog.anagrammargana.common.core.Room;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidPlayerIdException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidRoomCodeException;
import id.ac.ui.cs.advprog.anagrammargana.common.exceptions.InvalidSessionTokenException;
import id.ac.ui.cs.advprog.anagrammargana.common.model.MessageData;
import id.ac.ui.cs.advprog.anagrammargana.common.service.RoomHostValidatorService;
import id.ac.ui.cs.advprog.anagrammargana.lobby.service.LobbyMessageService;
import id.ac.ui.cs.advprog.anagrammargana.websocket.communication.service.WebsocketPlayerAuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class LobbyMessageControllerTest {
    @MockBean
    WebsocketPlayerAuthenticationService websocketPlayerAuthenticationService;
    
    @MockBean
    RoomHostValidatorService roomHostValidatorService;
    
    
    @MockBean
    LobbyMessageService lobbyMessageService;
    
    @InjectMocks
    LobbyMessageController lobbyMessageController;
    
    
    String roomCode = "1234";
    IRoom room;
    IPlayer host;
    IPlayer player1;
    IPlayer player2;
    
    String hostName = "I am host";
    String playerName1 = "Nirmala";
    String playerName2 = "Gecko";
    String hostSessionToken = "asdfghjkl";
    String sessionToken1 = "qwertyuiop";
    String sessionToken2 = "zxcvbnm";
    
    List<IPlayer> members;
    
    long senderId;
    String senderRoomCode;
    String senderSession;
    
    @BeforeEach
    void setup(){
        host = new Player(hostName, 100, hostSessionToken);
        player1 = new Player(playerName1, 101, sessionToken1);
        player2 = new Player(playerName2, 102, sessionToken2);
    
        senderId = player1.getId();
        senderRoomCode = roomCode;
        senderSession = sessionToken2;
        
        room = new Room(host, roomCode);
        room.addMember(player1);
        room.addMember(player2);
        members = List.of(host, player1, player2);
        
        
        doNothing().when(websocketPlayerAuthenticationService).throwIfNotAuthenticated(any(MessageData.class));
        MockitoAnnotations.openMocks(this);
    }
    
    // positive test
    @Test
    void refreshRoomMember_returnsCorrectAnswer() {
        refreshRoomMember_parameterized("refresh-room-member", "");
    }
    
    
    // negative case
    @Test
    void refreshRoomMember_doNotCareMessageBody() {
        refreshRoomMember_parameterized("refresh-room-member", null);
    }
    
    
    // edge case
    @Test
    void refreshRoomMember_doNotCareMessageType() {
        refreshRoomMember_parameterized("refresh-room-member", null);
    }
    
    <T> void refreshRoomMember_parameterized(String type, T body){
        var messageData = new MessageData<Object>(senderId, senderRoomCode, senderSession,
                                                  type, body);
        var resultingData = new MessageData<>(senderId, senderRoomCode, senderSession,
                                              "update-room-member", List.of(hostName, playerName2, playerName1));
        when(lobbyMessageService.refreshRoomMember(messageData)).thenReturn(resultingData);
    
        var result = lobbyMessageController.refreshRoomMember(messageData);
        assertSame(resultingData, result);
    }
    
    
    
    // edge case
    @SuppressWarnings("unchecked")
    @Test
    void refreshRoomMember_doNotCareMessageReturnedByService() {
        var messageData = new MessageData<Object>(senderId, senderRoomCode, senderSession,
                                                  "asdfghjkl", null);
        var resultingData = new MessageData<>(1, "arbitrary room code", "arbitrary token",
                                              "idk what type is this", (List<String>) new ArrayList());
        when(lobbyMessageService.refreshRoomMember(messageData)).thenReturn(resultingData);
        
        var result = lobbyMessageController.refreshRoomMember(messageData);
        assertSame(resultingData, result);
    }
    
    
    
    // edge case
    @Test
    void refreshRoomMember_doNotCatchExceptions() {
        var exceptions = List.of(
                InvalidPlayerIdException.class, InvalidSessionTokenException.class,
                InvalidRoomCodeException.class
        );
        
        var senderId = player1.getId();
        var senderRoomCode = roomCode;
        var senderSession = sessionToken2;
        var messageData = new MessageData<Object>(senderId, senderRoomCode, senderSession,
                                                  "refresh-room-member", "");
        
        for (var exception: exceptions) {
            doThrow(exception)
                    .when(websocketPlayerAuthenticationService)
                    .throwIfNotAuthenticated(any(MessageData.class));
            
            assertThrows(exception, () -> {
                lobbyMessageController.refreshRoomMember(messageData);
            });
        }
    }
    
    @Test
    void startGame() {
        var senderId = player1.getId();
        var senderRoomCode = roomCode;
        var senderSession = sessionToken2;
        var messageData = new MessageData<Object>(senderId, senderRoomCode, senderSession,
                                                  "refresh-room-member", "");
        
        doNothing().when(roomHostValidatorService).throwIfNotRoomHost(messageData);
        doNothing().when(lobbyMessageService).startGame(senderRoomCode);
        doNothing().when(websocketPlayerAuthenticationService).throwIfNotAuthenticated(messageData);
        
        lobbyMessageController.startGame(messageData);
    
        verify(roomHostValidatorService, times(1))
                .throwIfNotRoomHost(messageData);
        verify(websocketPlayerAuthenticationService, times(1))
                .throwIfNotAuthenticated(messageData);
        verify(lobbyMessageService, times(1))
                .startGame(roomCode);
    }
}