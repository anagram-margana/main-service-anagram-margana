# Dokumentasi API



## Inisiasi

Ketika client sudah terhubung ke microservice websocket, hal pertama yang perlu dilakukan oleh pengguna adalah 
mengirimkan pesan websocket untuk autentikasi. Service ini menyediakan sebuah endpoint yakni `/communication-api/init` (POST). 
Endpoint ini memerlukan parameter berupa:

- roomCode (string)
- playerId (long)
- sessionToken (string)
- websocketServiceAddress (string)

Note: <br>
websocketServiceAddress merupakan alaman websocket service yang mengirimkan request tersebut

Setelah itu, rest-api ini akan mengembalikan sebuah integer berupa 1 apabila autentikasi diterima (data-data yang diberikan valid), dan `0` apabila autentikasi ditolak.



## Komunikasi General

Komunikasi-komunikasi lain selain inisiasi/autentikasi akan ditangani pada endpoint `/communication-api/msg-from-client/{event-type}`.  Note: *event-type* selanjutnya akan disebut sebagai *type*.

Semua endpoint ini menerima dalam bentuk POST JSON, di mana terdapat struktur JSON yang harus diikuti, yakni terdiri atas:

- playerId (long)
- roomCode (String)
- sessionToken (String)
- type (String)
- body (Any)

Masing-masing endpoint ini bisa saja memberikan respon body tertentu, tetapi bisa juga tidak memberikan respon body apapun. Apabila terdapat suatu respon body, artinya data-data pada respon body tersebut perlu diteruskan melalui websocket ke pemain sebagai pesan balasan. Respon body tersebut akan terdiri atas:

- playerId (long)
- roomCode (String)
- sessionToken (String)
- type (String)
- body (Any)

Note: endpoint tertentu memiliki data-type body tertentu. <br><br>

Berikut ini adalah type yang dilayani pada REST API service ini  

### refresh-room-member (Lobby)

Ketika client ingin mendapatkan informasi mengenai daftar pemain room pada saat ini

Endpoint API ini tidak memerlukan body, sehingga isi dari body tersebut akan diabaikan.  API ini akan memberikan response body berupa list of string yang menyatakan daftar nama-nama pemain yang ada di dalam room tersebut.



### start-first-round (Lobby, host only)

Ketika room host ingin memulai ronde pertama.

Endpoint API ini tidak memerlukan body, dan tidak mengembalikan data apapun. API ini digunakan ketika room host ingin memulai ronde pertama. Hal ini akan menyebabkan room tersebut ditutup, sehingga jumlah pemain pada ruangan tersebut tidak akan bertambah lagi. API ini juga akan menyebabkan main-service memanggil websocket-service, memerintahkan para pemain pada room tersebut untuk berpindah ke halaman in-game.



### submit-game-settings (in-game, host only)

Ketika room host men-submit data untuk memulai round baru

Endpoint API tidak  mengembalikan data apapun, tetapi API ini memerlukan body yang berisi data:

- gameMode (String, enum)
- timeLimit (int)
- minWords (int)

Note: 

- gameMode terdiri atas 2 jenis pilihan: `FREE_FOR_ALL` atau `ALL_ALONE`
- timeLimit dalam satuan **menit**
- timeLimit bernilai antara 1 sampai 5 (inklusif)
- minWords maksimal bernilai antara 50 (inklusif)

Memanggil API ini akan menyebabkan main-service menghubungi websocket-service, memerintahkan para pemain pada room tersebut untuk memulai round baru dengan data:

- type: `start-new-round`
- body:
  - timeLimit (int)
  - randomLetters (String)
  - wordLengthToNumberOfWordsMapping (Map<Integer, Integer>)

Note: timeLimit dalam satuan **detik**. 



### submit-answer (in-game)

Ketika pemain men-submit jawaban.

Endpoint API tidak mengembalikan data apapun, tetapi API ini memerlukan body berupa string yang menyatakan jawaban yang disubmit oleh pemain. 

Apabila jawaban pemain tersebut invalid, main-service akan menghubungi websocket-service dengan type `invalid-answer`, body berupa string (alasan mengapa jawaban tersebut salah), serta target berupa pemain spesifik yang mengirimkan jawaban tersebut. 

Apabila jawaban pemain tersebut valid, maka main-service akan menghubungi websocket-service dengan target pemain berupa seluruh pemain pada room tersebut. Type dan body-nya bergantung pada game-mode yang sedang dijalani saat itu.

- Untuk mode ALL_ALONE:
  - type:  `aa-new-answer-submission`
  - body:
    - scoreData (array of dict):
      - playerId (long)
      - score (int)
      - name (String)
    - playerIdWhoSubmittedAnswer (long)
    - playerNameWhoSubmittedAnswer (String)
    - wordsFound (list of string)
  - *note*: masing-masing player bisa memiliki body yang berbeda-beda

- Untuk mode FREE_FOR_ALL:
  - type: `ffa-new-answer-submission`
  - body:
    - recentlyDiscoveredWord (String)
    - scoreData (array of dict):
      - playerId (long)
      - score (int)
      - name (String)
    - playerIdWhoSubmittedAnswer (long)
    - playerNameWhoSubmittedAnswer (String)
    - wordsFound (list of string)



### vote-restart (in-game)

Ketika pemain memberikan suara (vote) untuk merestart ronde.

Endpoint API ini tidak memerlukan body. 

Apabila vote tersebut valid, service ini akan mengembalikan response dengan type `vote-accepted` dan body apapun (tidak dipedulikan). Apabila tersebut valid, main-service akan menghubungi websocket-service dengan type `vote-restart`, dengan target pemain berupa seluruh pemain pada room tersebut, dan body:

- `numberOfVote` (int)
- `requiredNumberOfVotes` (int)

Apabila vote tersebut valid dan menyebabkan jumlah total vote di atas batas jumlah minimum vote yang diperlukan, main-service akan menghubungi websocket-service dengan type `vote-restart-eligible`, dengan target pemain berupa room host, dan body berupa string kosong.



### restart-round (in-game, host only)

Ketika room host memberikan final decision mengenai apakah akan me-restart ronde tersebut atau tidak. Hanya bisa dilakukan ketika jumlah vote untuk restart ronde sudah mencapai batas minimum.

Endpoint API ini tidak memerlukan body, dan tidak mengembalikan data apapun. 

Setelah pemanggilan API ini, main-service akan menghubungi websocket-service dengan type `round-restart`, dengan target pemain berupa seluruh pemain pada room tersebut, dan body berupa string kosong.



### refresh-round-information (in-game)

Ketika room sedang dalam suatu ronde dan seorang pengguna me-refresh browser, pengguna tetap 
dapat melanjutkan ronde tersebut dengan cara meminta informasi mengenai round tersebut ke 
endpoint ini

Endpoint API ini tidak memerlukan body. Body yang dikembalikan oleh endpoint ini antara lain berupa


- latestScoreUpdate
  - scoreData (array of dict):
    - playerId (long)
    - score (int)
    - name (String)
  - wordsFound (list of string)
- roundInformation
  - timeLimit (int)
  - randomLetters (String)
  - wordLengthToNumberOfWordsMapping (Map<Integer, Integer>)
- voteInformation
  - numberOfVote (int)
  - requiredNumberOfVotes (int)

Note: timeLimit dalam satuan **detik**.



### disconnected (in-game)

Ketika salah satu pemain mengalami gangguan koneksi dan sudah tidak terhubung dengan websocket-service

Endpoint API ini tidak memerlukan body dan tidak mengembalikan response body apapun.


### connected (in-game)

Ketika salah satu pemain sudah terhubung dengan websocket service.

Endpoint API ini tidak memerlukan body dan tidak mengembalikan response body apapun.


### finish-the-game (in-game, host only)

Ketika room host memilih untuk mengakhiri permainan dan membubarkan room.

Endpoint API ini tidak memerlukan body, dan tidak mengembalikan data apapun.

Setelah pemanggilan API ini, main-service akan menghubungi websocket-service dengan type `game-ended`, 
dengan target pemain berupa seluruh pemain pada room tersebut, dan body berupa string kosong.

